﻿using log4net;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Quartz;
using SdLa_Int.LaModels;
using SdLa_Int.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace SdLa_Int.Tasks
{
    public class RestTask : IJob
    {
        HttpClient client = new HttpClient();
        DBESUBELIVEContext db = new DBESUBELIVEContext();
        LawepContext ldb = new LawepContext();

        //Shipments shipments = null;
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public async Task Execute(IJobExecutionContext context)
        {
            DateTime now = DateTime.Now;
            client.BaseAddress = new Uri("https://apps.simplydeliver.com/ords/sd/data/export");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));


            //Sadece son yılın kayıtlarına bak..
            //LA tarafı-- 10=seferde,7=depoda
            DateTime currentYear = DateTime.Now.AddMonths(-1);
            this.db.Database.SetCommandTimeout(180);
            var shipments = await (from s in ldb.LtraTransportationorder
                                   join d in ldb.LtraTripdetail on s.TrorId equals d.TrdtTransportationorderid
                                   where
                                   //(s.TrorTransportorderstatusid == 10  || s.TrorTransportorderstatusid == 7 || s.TrorTransportorderstatusid == 12) && 
                                   s.TrorOrderdate >= currentYear &&
                                   (
                                     s.TrorId == 2489210
                                   //s.TrorId == 2489209 || s.TrorId == 2489210
                                   )
                                   //(s.SpintegrationStatus == 100 || d.Sdintegration == 100)
                                   //(s.TrorId == 2139399 || s.TrorId == 2139402)
                                   //(s.TrorDepositorid == 1065 ||
                                   // s.TrorDepositorid == 981 ||
                                   // s.TrorDepositorid == 857)
                                   orderby s.TrorOrderdate ascending
                                   select s
                              ).Take(1000).ToListAsync();

            //Tüm siprişleri LA dan döndürüyoruz...
            foreach (var sh in shipments)
            {
                //Burada Transportation id yi buluyorum. La tarafı
                var TransportIds = await (from td in ldb.LtraTripdetail
                                          join t in ldb.LtraTrip on td.TrdtTripid equals t.TripId
                                          where td.TrdtTransportationorderid == sh.TrorId
                                          select t).ToListAsync();

                //Bir shipment birden fazla Transportation da olabilir.
                foreach (var TransportId in TransportIds)
                {
                    HttpResponseMessage responseMessage = null;
                    if (TransportId != null)
                    {
                        responseMessage = await client.PostAsync(client.BaseAddress.ToString(),
                        //TrorId = Shipment , TripId == Transportation 
                        new StringContent(await Tools.ShipChangePostContent.Js(sh.TrorId.ToString(), TransportId.TripId.ToString())));
                    }


                    if (responseMessage != null && responseMessage.IsSuccessStatusCode)
                    {
                        string transportResult = responseMessage.Content.ReadAsStringAsync().Result;
                        var SDtransports = JsonConvert.DeserializeObject<RootObject>(transportResult);

                        if (SDtransports.transports.Count != 0 || SDtransports != null)
                        {
                            // SD den Taşımaları döndürüyoruz
                            foreach (var t in SDtransports.transports)
                            {

                                // servisden teslim edilmiş olanlar her şekilde POD_Transportations ve POD_Shipments a insert edilmeli.
                                //Normalse ; olan kısmın solunu alacağız.... --
                                //string Tror_Id = sh.TrorId.ToString().Contains(";") != true ? sh.TrorId.ToString() : sh.TrorId.ToString().Split(';')[0];
                                var a = t.shipments.Where(x => (x.statuscode == "ARRIVED" && x.statuscode == "NORMAL") &&
                                        (x.externalsystemid == sh.TrorId.ToString() || x.externalsystemid.Contains(sh.TrorId.ToString()))).FirstOrDefault();
                                
                              
                                //önceden eklenmişse eklememeliyiz.mükerrerlik kontrolü                              
                                var TransRepeatCheckPod = await db.PodTransportations.Where(x => x.Externalsystemid == t.externalsystemid).ToListAsync();
                                if (a != null && TransRepeatCheckPod.Count == 0)
                                {
                                    #region add podtransportation
                                    await new Tools.InsertPOD().AddTransport(t);
                                    #endregion
                                }

                                string tid = TransportId.TripId.ToString();
                                int findPodTransId = Convert.ToInt32(await db.PodTransportations.Where(x => x.Externalsystemid == tid).Select(x=>x.Id).FirstOrDefaultAsync());

                                var ShipRepeatCheck = await db.PodShipments.Where(x => (x.Externalsystemid == sh.TrorId.ToString() || x.Externalsystemid.Contains(sh.TrorId.ToString())) && x.TransportationId == findPodTransId).ToListAsync();
                                if (a != null && ShipRepeatCheck.Count == 0)
                                {

                                    #region podaddshipment
                                    await new Tools.InsertPOD().AddShipment(a, t.code);
                                    #endregion

                                }

                                #region arrived customer
                                var s = t.shipments.Where(x => x.statuscode == "ARRIVED" && x.shipmenttype == "CUSTOMER" &&
                                (x.externalsystemid == sh.TrorId.ToString() || x.externalsystemid.Contains(sh.TrorId.ToString()))).FirstOrDefault();

                                int ShipExternalSysyemId = Convert.ToInt32(s != null ? s.externalsystemid : null);

                                if (ShipExternalSysyemId != 0)
                                {

                                    #region Teslim evrakı kaydetme
                                    try
                                    {
                                        if (s.deliveryphoto1 != null)
                                        {
                                            string path = @"\\192.168.2.42\LawepApp\Lawep\CustomerBase\Documents\RecordImages\LTRA\CVLTRA_QUICKDELIVERYINF\" + sh.TrorId;
                                            new Tools.FileFolderProcess().NewFolder(path);


                                            var bytes = Convert.FromBase64String(s.deliveryphoto1);
                                            using (var imageFile = new FileStream(path + "\\" + s.deliveryfilename1, FileMode.Create))
                                            {
                                                imageFile.Write(bytes, 0, bytes.Length);
                                                imageFile.Flush();
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {

                                        await new Tools.MailTool().SendMail(ShipExternalSysyemId + " Dosya klasör oluşturma sorunu " + ex.ToString());
                                    }
                                    #endregion


                                    var TransRepeatCheck = await db.PodTransportations.Where(x => x.Externalsystemid == t.externalsystemid).ToListAsync();
                                    var ShipRepeatCheckForCustomer = await db.PodShipments.Where(x => x.Externalsystemid == sh.TrorId.ToString() || x.Externalsystemid.Contains(sh.TrorId.ToString())).ToListAsync();
                                    if (TransRepeatCheck.Count == 0)
                                    {
                                        #region add podtransportation
                                        await new Tools.InsertPOD().AddTransport(t);
                                        #endregion

                                        if (s != null && ShipRepeatCheckForCustomer.Count == 0)
                                        {
                                            #region podaddshipment
                                            await new Tools.InsertPOD().AddShipment(s, t.code);
                                            #endregion

                                        }
                                    }
                                    else if(TransRepeatCheck.Count > 0)
                                    {
                                        var GetTransId = await db.PodTransportations.Where(x => x.Externalsystemid == t.externalsystemid).FirstOrDefaultAsync();
                                        Tools.InsertPOD.AutoIdentityKey = (int)GetTransId.Id;
                                        if (s != null && ShipRepeatCheckForCustomer.Count == 0)
                                        {
                                            #region podaddshipment
                                            await new Tools.InsertPOD().AddShipment(s, t.code);
                                            #endregion

                                        }
                                    }
                                    
                     

                                    int NewTodiId = Convert.ToInt32((from r in ldb.LtraTransportorddeliveryinf orderby r.TodiId select r.TodiId).Last()) + 1;

                                    var TransRepeatCheckDeliveryInf = await ldb.LtraTransportorddeliveryinf.Where(x => x.TodiTransportationorderid == ShipExternalSysyemId).ToListAsync();
                                    if (TransRepeatCheckDeliveryInf.Count == 0)
                                    {
                                        #region addtransportorderdelivery
                                        DateTime ata = Convert.ToDateTime(s.atadate.ToShortDateString() + " " + s.atatime);
                                        DateTime atd = Convert.ToDateTime(s.atddate.ToShortDateString() + " " + s.atdtime);
                                        ldb.LtraTransportorddeliveryinf.Add(new LtraTransportorddeliveryinf
                                        {
                                            TodiId = NewTodiId,
                                            TodiArrivaldatetime = ata,
                                            TodiDeliverydatetime = ata,
                                            TodiRecievedby = s.deliverysignatureby,
                                            TodiIdentitytypeid = null,
                                            TodiIdentityno = null,
                                            TodiDeliveryplace = null,
                                            TodiDeparturedatetime = null,
                                            TodiTransportreturnreasonid = null,
                                            TodiNotes = null,
                                            TodiTransportdelayreasonid = null,
                                            TodiTripid = null,
                                            TodiTransportationorderid = Convert.ToInt32(s.externalsystemid),
                                            TodiEnteredby = s.driverexternalsystemid == null ? t.drivermobilenumber : s.drivermobilenumber,
                                            TodiEntrydatetime = ata,
                                            TodiUpdatedby = null,
                                            TodiUpdateddatetime = null,
                                            TodiTransordadrchangreasonid = null,
                                            TodiChangedpartyaddressid = null,
                                            TodiReceiverphoto = null,
                                            TodiTransportorderdetid = null,
                                            TodiPoddocumentno = null,
                                            TodiKm = null,
                                            TodiAsnquantity = null,
                                            TodiProductdescription = null,
                                            TodiActionstatusid = null,
                                            TodiShipmentbarcode = null,
                                            TodiCode = null,
                                            TodiDescription = "SD",
                                            TodiIsapproved = null,
                                            TodiImage = null,
                                            CreatedDate = now,
                                            LastModifiedDate = now,
                                            
                                        });
                                        #endregion
                                    }
                                    #region update transportation order
                                    LtraTransportationorder Order = await ldb.LtraTransportationorder.Where(x => x.TrorId == ShipExternalSysyemId).FirstOrDefaultAsync();
                                    Order.TrorTransportorderstatusid = 12;
                                    #endregion

                                    #region Trip Detail
                                    LtraTripdetail TripDetail = await ldb.LtraTripdetail.Where(x => x.TrdtTransportationorderid == ShipExternalSysyemId).FirstOrDefaultAsync();
                                    TripDetail.TrdtTripdetailstatusid = 3;
                                    #endregion


                                    #region Trip

                                    //Transportation içindeki tüm siparişler yükler teslim olmadan Transportation Trip statusu 6 olmamalı.
                                    int LaTransInShips = await (from ts in ldb.LtraTripdetail where (ts.TrdtTripid == TransportId.TripId) select ts).CountAsync();
                                    int SdTransInShips = await (from lat in db.PodTransportations
                                                                join sds in db.PodShipments on lat.Id equals sds.TransportationId
                                                                where (lat.Externalsystemid == TransportId.TripId.ToString())
                                                                select lat).CountAsync();
                                    if (LaTransInShips == SdTransInShips)
                                    {
                                        LtraTrip Trip = await ldb.LtraTrip.Where(x => x.TripId == TransportId.TripId).FirstOrDefaultAsync();
                                        Trip.TripTripstatusid = 6;
                                    }

                                    #endregion


                                    await ldb.SaveChangesAsync();

                                }
                                #endregion

                            }

                        }

                    }
                }

                Console.WriteLine(sh.TrorId);
            }
            Console.WriteLine("Tamamlandı...");
        }
    }
}

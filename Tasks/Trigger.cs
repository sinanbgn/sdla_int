﻿using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SdLa_Int.Tasks
{
    public class Trigger
    {

        public async Task StartTask()
        {
            IScheduler scheduler = await Start();
            IJobDetail Task = JobBuilder.Create<RestTask>().WithIdentity("RestTask", null).Build();
            ITrigger simpleTrigger = (ITrigger)TriggerBuilder.Create()
                .WithIdentity("RestTask")
                .StartAt(DateTime.UtcNow)
                //.WithDailyTimeIntervalSchedule(x => x.WithIntervalInHours(24)
                //.OnEveryDay().StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(23, 50)))
                .WithSimpleSchedule(x => x.WithIntervalInMinutes(5)
                .RepeatForever())
                .Build();
            await scheduler.ScheduleJob(Task, simpleTrigger);

            // .WithIdentity("RestTask")
            // .StartAt(DateTime.UtcNow)
            // .WithSimpleSchedule(x=>x.WithIntervalInSeconds(30)
            // .RepeatForever())
            // .Build();
        }

        private async Task<IScheduler> Start()
        {
            ISchedulerFactory schedulerFactory = new StdSchedulerFactory();
            IScheduler scheduler = await schedulerFactory.GetScheduler();
            if (!scheduler.IsStarted)
                await scheduler.Start();
            return scheduler;

        }
    }
}

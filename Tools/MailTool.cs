﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Net;
using log4net;
using System.Reflection;

namespace SdLa_Int.Tools
{
    public class MailTool
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public async Task SendMail(string Message)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", false);
            var configuration = builder.Build();

            var aSection = new MailSection();
            var section = configuration.GetSection("EmailConfiguration");
            aSection = section.Get<MailSection>();

            try
            {
                SmtpClient client = new SmtpClient(aSection.SmtpServer);
                client.Port = aSection.SmtpPort;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(aSection.Username, aSection.Password);
                client.EnableSsl = true;

                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(aSection.From);
                foreach (var item in aSection.To.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    mailMessage.Bcc.Add(item);
                }
                mailMessage.IsBodyHtml = false;
                mailMessage.Body = Message;
                mailMessage.Subject = "SD & LS Integration Info";
                await client.SendMailAsync(mailMessage);
                
            }
            catch (System.Exception ex)
            {
                log.Error(ex.ToString());
            }
        }
    }

    public class MailSection
    {
        public string SmtpServer { get; set; }
        public int SmtpPort { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string From { get; set; }
        public string To { get; set; }
    }

}

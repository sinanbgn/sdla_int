﻿using log4net;
//using SdLa_Int.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SdLa_Int.Tools
{
    public static class ShipChangePostContent
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public static async Task<string> Js(string TrorShipId, string TransportId)
        {

            try
            {
                //string pattern = "[0-1]?[0-9]/[0-9]{2}/[0-9]{4}";
                //string today = DateTime.Today.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                string path = Path.Combine(Directory.GetCurrentDirectory(), "ttAttr.json");
                //File.WriteAllText(path, Regex.Replace(File.ReadAllText(path), pattern, today));
                dynamic jsonObj = Newtonsoft.Json.JsonConvert.DeserializeObject(File.ReadAllText(path));
                jsonObj["request"]["shipmentexternalsystemid"][0] = TrorShipId;
                jsonObj["request"]["transportexternalsystemid"][0] = TransportId;
                string output = Newtonsoft.Json.JsonConvert.SerializeObject(jsonObj, Newtonsoft.Json.Formatting.Indented);
                File.WriteAllText(path, output);

                return await System.IO.File.ReadAllTextAsync(path);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return  ex.Message.ToString();
            }

           
        }
    }
}

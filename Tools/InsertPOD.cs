﻿using SdLa_Int.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SdLa_Int.Tools
{
    public class InsertPOD
    {
        DBESUBELIVEContext db = new DBESUBELIVEContext();
        DateTime now = DateTime.Now;
        public static int AutoIdentityKey = 0;
        public async Task<PodTransportations> AddTransport(Transport t)
        {
            try
            {
                var TransEntity = new PodTransportations
                {
                    CreatedDate = now,
                    Code = t.code,
                    Createdatetime = t.createdatetime,
                    Updatedatetime = t.updatedatetime,
                    Updateuser = t.updateuser == null ? "-" : t.updateuser,
                    Departureaddress = t.departureaddress,
                    Etddate = t.etddate.ToString(),
                    Etdtime = t.etdtime,
                    Ptddate = t.ptddate.ToString(),
                    Ptdtime = t.ptdtime,
                    Atddate = t.atddate.ToString(),
                    Atdtime = t.atdtime,
                    Arrivaladdress = t.arrivaladdress,
                    Etadate = t.etadate.ToString(),
                    Etatime = t.etatime,
                    Ptadate = t.ptadate.ToString(),
                    Ptatime = t.ptatime,
                    Atadate = t.atadate.ToString(),
                    Atatime = t.atatime,
                    Transporttypecode = t.transporttypecode,
                    Cargotypecode = t.cargotypecode,
                    Statuscode = t.statuscode,
                    Calculateddistance = t.calculateddistance,
                    Calculatedduration = t.calculatedduration,
                    Driverseq = t.driverseq,
                    Driverexternalsystemid = t.driverexternalsystemid,
                    Drivername = t.drivername,
                    Drivermobilenumber = t.drivermobilenumber,
                    Vehicleseq = t.vehicleseq == null ? 0 : t.vehicleseq,
                    Departureaddressfree = t.departureaddressfree,
                    Arrivaladdressfree = t.arrivaladdressfree,
                    Carriercode = t.carriercode,
                    Externalsystemid = t.externalsystemid,
                    Archivedyn = t.archivedyn == "Y" ? true : false,
                    Lastlocationlatitude = t.lastlocationlatitude == null ? "-" : t.lastlocationlatitude,
                    Lastlocationlongitude = t.lastlocationlongitude == null ? "-" : t.lastlocationlongitude
                };
                db.PodTransportations.Add(TransEntity);
                await db.SaveChangesAsync();
                AutoIdentityKey = Convert.ToInt32(TransEntity.Id);
                return TransEntity;
            }
            catch (Exception ex)
            {
                
                await new Tools.MailTool().SendMail(t.externalsystemid+" nolu "+"PodTransport ekleme sorunu " + ex.ToString());
            }
            return null;
        }
    
        public async Task<PodShipments> AddShipment(Shipment a, string code)
        {
            try
            {
                var ShipmentEntity = new PodShipments
                {
                    CreatedDate = Convert.ToDateTime(now),
                    TransportationCode = code,
                    TransportationId = AutoIdentityKey,
                    Seq = a.seq,
                    Code = a.code,
                    Externalsystemid = a.externalsystemid,
                    Createdatetime = a.createdatetime,
                    Updatedatetime = a.updatedatetime,
                    Updateuser = a.updateuser,
                    Dataorigin = a.dataorigin,
                    Requireddeliverydate = a.requireddeliverydate.ToString(),
                    Pickupaddress = a.pickupaddress,
                    Deliveryaddress = a.deliveryaddress,
                    Requiredpickupdate = a.requiredpickupdate.ToString(),
                    Receiver = a.receiver,
                    Debtor = a.debtor,
                    Shipmenttype = a.shipmenttype,
                    Yourreference = a.yourreference == null ? "-" : a.yourreference,
                    Ourreference = a.ourreference == null ? "-" : a.ourreference,
                    Movementtype = a.movementtype,
                    Cargotype = a.cargotype,
                    Statuscode = a.statuscode,
                    Activeyn = a.activeyn == "Y" ? true : false,
                    Pickuplatitude = a.pickuplatitude,
                    Pickuplongitude = a.pickuplongitude,
                    Deliverylatitude = a.deliverylatitude,
                    Deliverylongitude = a.deliverylongitude,
                    Pickupaddressfree = a.pickupaddressfree,
                    Deliveryaddressfree = a.deliveryaddressfree,
                    Confirmationdate = a.confirmationdate.ToString(),
                    Ptddate = a.ptddate.ToString(),
                    Ptdtime = a.ptdtime,
                    Atddate = a.atddate.ToString(),
                    Atdtime = a.atdtime,
                    Ptadate = a.ptadate.ToString(),
                    Ptatime = a.ptatime,
                    Atadate = a.atadate.ToString(),
                    Atatime = a.atatime,
                    Notifyonpickup = a.notifyonpickup == "E" ? true : false,
                    Notifyondelivery = a.notifyondelivery == "E" ? true : false,
                    Dynamicstatus = a.dynamicstatus,
                    Archivedyn = a.archivedyn == "Y" ? true : false,
                    Deliveryphoto1 = a.deliveryphoto1 == null ? "-" : a.deliveryphoto1,
                    Deliveryfilename1 = a.deliveryfilename1 == null ? "-" : a.deliveryfilename1,
                    Deliverymimetype1 = a.deliverymimetype1 == null ? "-" : a.deliverymimetype1,
                    Deliverysignatureby = a.deliverysignatureby == null ? "-" : a.deliverysignatureby,
                    Driverseq = a.driverseq == null ? 0 : a.driverseq,
                    Driverexternalsystemid = a.deliverysignatureby == null ? "-" : a.driverexternalsystemid,
                    Drivername = a.drivername == null ? "-" : a.drivername,
                    Drivermobilenumber = a.drivermobilenumber == null ? "-" : a.drivermobilenumber,
                    Lastlocationlatitude = a.lastlocationlatitude == null ? "-" : a.lastlocationlatitude,
                    Lastlocationlongitude = a.lastlocationlongitude == null ? "-" : a.lastlocationlongitude

                };
                db.PodShipments.Add(ShipmentEntity);

                await db.SaveChangesAsync();
                return ShipmentEntity;
            }
            catch (Exception ex)
            {
                await new Tools.MailTool().SendMail(a.externalsystemid + " nolu "+"PodShipment ekleme sorunu " + ex.ToString());

            }
            return null;
        }
    }
}

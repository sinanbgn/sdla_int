﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SdLa_Int.Tools
{
    public class FileFolderProcess
    {
        public string NewFolder(string path)
        {
            try
            {

                if (Directory.Exists(path))
                {
                   
                    return "That path exists already.";
                }

                DirectoryInfo di = Directory.CreateDirectory(path);
                //Console.WriteLine("The directory was created successfully at {0}.", Directory.GetCreationTime(path));
                return "Success";

         
            }
            catch (Exception e)
            {
                //Console.WriteLine("The process failed: {0}", e.ToString());
                return e.ToString();
            }
            finally { }
        }
    }
}

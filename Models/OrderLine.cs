﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class OrderLine
    {
        public long Id { get; set; }
        public string IntegrationCode { get; set; }
        public string LineCode { get; set; }
        public double? Quantity { get; set; }
        public string LotCode { get; set; }
        public int RecordType { get; set; }
        public int ContainerType { get; set; }
        public string ProductionDate { get; set; }
        public string ExpireDate { get; set; }
        public string PackageBarcode { get; set; }
        public string PaletteBarcode { get; set; }
        public int? CreateId { get; set; }
        public int? UpdateId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string FreeText { get; set; }
        public int SyncStatusCode { get; set; }
        public string SyncStatus { get; set; }
        public string SyncDesc { get; set; }
        public long? ItemId { get; set; }
        public long? OrderId { get; set; }

        public virtual Item Item { get; set; }
        public virtual Order Order { get; set; }
    }
}

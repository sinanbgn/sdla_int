﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class GetirItemPackType
    {
        public long Id { get; set; }
        public bool ConsumerUnit { get; set; }
        public string Description { get; set; }
        public string Height { get; set; }
        public int IncludesCuquantity { get; set; }
        public string Length { get; set; }
        public string NetWeight { get; set; }
        public string Unit { get; set; }
        public string Volume { get; set; }
        public string Weight { get; set; }
        public string Widht { get; set; }
        public string IntegrationCode { get; set; }
        public int? CreateId { get; set; }
        public int? UpdateId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string FreeText { get; set; }
        public int SyncStatusCode { get; set; }
        public string SyncStatus { get; set; }
        public string SyncDesc { get; set; }
        public long? ItemId { get; set; }

        public virtual GetirItem Item { get; set; }
    }
}

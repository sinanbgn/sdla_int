﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class ClientKey
    {
        public int ClientKeyId { get; set; }
        public long CompanyId { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public DateTime CreateOn { get; set; }
        public long UserId { get; set; }
    }
}

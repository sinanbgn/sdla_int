﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class UetdsYukKaydi
    {
        public long Id { get; set; }
        public string OrderCode { get; set; }
        public string SonucKodu { get; set; }
        public string SonucMesaji { get; set; }
        public string EsyaSonucKodu { get; set; }
        public string EsyaSonucMesaji { get; set; }
        public string UetdsBildirimRefNo { get; set; }
        public int? CreateId { get; set; }
        public int? UpdateId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
    }
}

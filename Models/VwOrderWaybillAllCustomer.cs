﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class VwOrderWaybillAllCustomer
    {
        public string SirketKodu { get; set; }
        public string Hkod { get; set; }
        public string DocumentId { get; set; }
        public string WaybillDate { get; set; }
        public string ItemId { get; set; }
        public int? Quantity { get; set; }
        public string WarehouseCode { get; set; }
        public string IntegrationCode { get; set; }
        public DateTime? IntegrationDate { get; set; }
        public int? IntegrationStatus { get; set; }
        public string IntegrationStatusDesc { get; set; }
        public string OrderCode { get; set; }
        public decimal? OrderStatus { get; set; }
        public string Domaintype { get; set; }
    }
}

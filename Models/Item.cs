﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class Item
    {
        public Item()
        {
            ItemBarcode = new HashSet<ItemBarcode>();
            ItemEachCustomer = new HashSet<ItemEachCustomer>();
            ItemPackType = new HashSet<ItemPackType>();
            OrderLine = new HashSet<OrderLine>();
        }

        public long Id { get; set; }
        public string MainCompanyCode { get; set; }
        public string ItemCode { get; set; }
        public string ItemDescription { get; set; }
        public double? Ldm { get; set; }
        public int UnitTopType { get; set; }
        public int UnitSubType { get; set; }
        public string Gtin { get; set; }
        public string ItemMainCategory { get; set; }
        public string ItemSubCategory { get; set; }
        public string ItemDetailCategory { get; set; }
        public bool ExpireDateRequired { get; set; }
        public bool LotRequired { get; set; }
        public int ShelfLife { get; set; }
        public int? ExpireDateLeftDayCount { get; set; }
        public string AdrTypeLabel { get; set; }
        public string AdrUnNumber { get; set; }
        public string AdrItemName { get; set; }
        public string AdrMainRiskClass { get; set; }
        public string AdrAdditionalRisk { get; set; }
        public string AdrPackageGroup { get; set; }
        public string AdrTunnelRestrictionCode { get; set; }
        public bool IsNotified { get; set; }
        public double? Volume { get; set; }
        public double? GrossWeight { get; set; }
        public double? NetWeight { get; set; }
        public double QuantityInPackage { get; set; }
        public int UnitType { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public double Length { get; set; }
        public int? CreateId { get; set; }
        public int? UpdateId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string FreeText { get; set; }
        public int SyncStatusCode { get; set; }
        public string SyncStatus { get; set; }
        public string SyncDesc { get; set; }
        public int? WarningForExpireDateMin { get; set; }
        public int? WarningForGoodsInExpireDate { get; set; }
        public int RecordType { get; set; }

        public virtual ICollection<ItemBarcode> ItemBarcode { get; set; }
        public virtual ICollection<ItemEachCustomer> ItemEachCustomer { get; set; }
        public virtual ICollection<ItemPackType> ItemPackType { get; set; }
        public virtual ICollection<OrderLine> OrderLine { get; set; }
    }
}

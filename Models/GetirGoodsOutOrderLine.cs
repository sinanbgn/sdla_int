﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class GetirGoodsOutOrderLine
    {
        public long Id { get; set; }
        public string WaorInventoryItemCode { get; set; }
        public string WaorInventoryItemDetailNo { get; set; }
        public string WaorInventoryItemNotes { get; set; }
        public int WaorInventoryItemPlanCuqty { get; set; }
        public string WaorInventoryItemUnitId { get; set; }
        public string IntegrationCode { get; set; }
        public int? CreateId { get; set; }
        public int? UpdateId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string FreeText { get; set; }
        public int SyncStatusCode { get; set; }
        public string SyncStatus { get; set; }
        public string SyncDesc { get; set; }
        public long? OrderId { get; set; }

        public virtual GetirGoodsOutOrder Order { get; set; }
    }
}

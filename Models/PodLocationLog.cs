﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class PodLocationLog
    {
        public long Id { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? TransportationId { get; set; }
        public string Createdatetime { get; set; }
        public string Createusers { get; set; }
        public string Movementtime { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }

        public virtual PodTransportations Transportation { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class Tender
    {
        public Tender()
        {
            TenderAnswer = new HashSet<TenderAnswer>();
            TenderStop = new HashSet<TenderStop>();
        }

        public long Id { get; set; }
        public string Code { get; set; }
        public string ExternalSystemId { get; set; }
        public string TypeCode { get; set; }
        public string PricingTypeCode { get; set; }
        public string Price { get; set; }
        public string Currency { get; set; }
        public string DeadLineDate { get; set; }
        public string MessageText { get; set; }
        public string StatusCode { get; set; }
        public string ShipmentType { get; set; }
        public string ShipmentCode { get; set; }
        public string TransporterCode { get; set; }
        public string CargoType { get; set; }
        public string TruckType { get; set; }
        public string CompanyName { get; set; }
        public string CompanyExternalSystemId { get; set; }
        public string CarrierName1 { get; set; }
        public string CarriereExternalSystemId1 { get; set; }
        public string CarrierContact1 { get; set; }
        public string CarrierEmail1 { get; set; }
        public string CarrierName2 { get; set; }
        public string CarriereExternalSystemId2 { get; set; }
        public string CarrierContact2 { get; set; }
        public string CarrierEmail2 { get; set; }
        public string CarrierName3 { get; set; }
        public string CarriereExternalSystemId3 { get; set; }
        public string CarrierContact3 { get; set; }
        public string CarrierEmail3 { get; set; }
        public string CarrierName4 { get; set; }
        public string CarriereExternalSystemId4 { get; set; }
        public string CarrierContact4 { get; set; }
        public string CarrierEmail4 { get; set; }
        public string CarrierName5 { get; set; }
        public string CarriereExternalSystemId5 { get; set; }
        public string CarrierContact5 { get; set; }
        public string CarrierEmail5 { get; set; }
        public string FromAddress { get; set; }
        public string FromAddressCode { get; set; }
        public string FromAddressName { get; set; }
        public string FromAddressCity { get; set; }
        public string FromAddressStreet { get; set; }
        public string FromAddressPostCode { get; set; }
        public string FromAddressHouseNumber { get; set; }
        public string FromAddressCountryCode { get; set; }
        public string FromAddressExternalSystemId { get; set; }
        public string FromAddressLatitude { get; set; }
        public string FromAddressLongitude { get; set; }
        public string ToAddress { get; set; }
        public string ToAddressCode { get; set; }
        public string ToAddressName { get; set; }
        public string ToAddressCity { get; set; }
        public string ToAddressStreet { get; set; }
        public string ToAddressPostCode { get; set; }
        public string ToAddressHouseNumber { get; set; }
        public string ToAddressCountryCode { get; set; }
        public string ToAddressExternalSystemId { get; set; }
        public string ToAddressLatitude { get; set; }
        public string ToAddressLongitude { get; set; }
        public string TotalWeight { get; set; }
        public string CalculatedKm { get; set; }
        public string ConfirmedKm { get; set; }
        public string RequestSentYn { get; set; }
        public int? CreateId { get; set; }
        public int? UpdateId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string FreeText { get; set; }
        public int SyncStatusCode { get; set; }
        public string SyncStatus { get; set; }
        public string SyncDesc { get; set; }
        public int StopCountUnLoading { get; set; }
        public int StopCountLoading { get; set; }
        public string CompanyCode { get; set; }
        public string FromAddressTown { get; set; }
        public string ToAddressTown { get; set; }
        public string ProductDescription { get; set; }
        public string TotalWeightBase { get; set; }
        public string TotalVolume { get; set; }
        public string CreateUser { get; set; }
        public string UpdateUser { get; set; }
        public string LocationCode { get; set; }
        public string Seq { get; set; }

        public virtual ICollection<TenderAnswer> TenderAnswer { get; set; }
        public virtual ICollection<TenderStop> TenderStop { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class TenderAnswer
    {
        public long Id { get; set; }
        public string TenderCode { get; set; }
        public string AnsweredBy { get; set; }
        public string AnsweredDate { get; set; }
        public string StatusCode { get; set; }
        public string CurrencyCode { get; set; }
        public string Comments { get; set; }
        public string Notes { get; set; }
        public string TenderPriceType1 { get; set; }
        public string TenderPriceType2 { get; set; }
        public string TenderPriceType3 { get; set; }
        public string TenderPriceType4 { get; set; }
        public string TenderPriceType5 { get; set; }
        public string TenderPriceType6 { get; set; }
        public string TenderPriceType7 { get; set; }
        public string TenderPriceType8 { get; set; }
        public string TenderPrice1 { get; set; }
        public string TenderPrice2 { get; set; }
        public string TenderPrice3 { get; set; }
        public string TenderPrice4 { get; set; }
        public string TenderPrice5 { get; set; }
        public string TenderPrice6 { get; set; }
        public string TenderPrice7 { get; set; }
        public string TenderPrice8 { get; set; }
        public string Commission { get; set; }
        public string TotalPrice { get; set; }
        public string TotalAmount { get; set; }
        public string ManualAmount { get; set; }
        public string DriverSeq { get; set; }
        public string DriverName { get; set; }
        public string DriverExternalSystemId { get; set; }
        public string DriverCharachterName { get; set; }
        public string DriverMobileNr { get; set; }
        public string CarrierName { get; set; }
        public string CarrierExternalSystemId { get; set; }
        public int? CreateId { get; set; }
        public int? UpdateId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string FreeText { get; set; }
        public int SyncStatusCode { get; set; }
        public string SyncStatus { get; set; }
        public string SyncDesc { get; set; }
        public long? TenderId { get; set; }
        public string LicensePlate { get; set; }
        public string PaymentMethod { get; set; }
        public string PaymentInfo { get; set; }
        public string TrailerNr { get; set; }

        public virtual Tender Tender { get; set; }
    }
}

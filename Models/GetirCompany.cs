﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class GetirCompany
    {
        public long Id { get; set; }
        public string PartyAddress { get; set; }
        public string PartyAddressDistrict { get; set; }
        public string PartyAddressEmail { get; set; }
        public string PartyAddressLangtitude { get; set; }
        public string PartyAddressLongtitude { get; set; }
        public string PartyAddressPhone { get; set; }
        public string PartyAddressTimeClosed { get; set; }
        public string PartyAddressTimeopened { get; set; }
        public string PartyCity { get; set; }
        public string PartyCode { get; set; }
        public string PartyDepositor { get; set; }
        public string PartyDescription { get; set; }
        public string PartyGeoArea { get; set; }
        public bool PartyIsChanged { get; set; }
        public bool PartyIsCustomer { get; set; }
        public bool PartyIsSupplier { get; set; }
        public string PartyPostcode { get; set; }
        public string PartyTaxCode { get; set; }
        public string PartyTaxoffice { get; set; }
        public string PartyTown { get; set; }
        public string IntegrationCode { get; set; }
        public int? CreateId { get; set; }
        public int? UpdateId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string FreeText { get; set; }
        public int SyncStatusCode { get; set; }
        public string SyncStatus { get; set; }
        public string SyncDesc { get; set; }
    }
}

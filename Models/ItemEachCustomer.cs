﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class ItemEachCustomer
    {
        public long Id { get; set; }
        public string MainCompanyCode { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerTitle { get; set; }
        public string ItemCode { get; set; }
        public string CustItemCode { get; set; }
        public int? CreateId { get; set; }
        public int? UpdateId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string FreeText { get; set; }
        public int SyncStatusCode { get; set; }
        public string SyncStatus { get; set; }
        public string SyncDesc { get; set; }
        public long? ItemId { get; set; }

        public virtual Item Item { get; set; }
    }
}

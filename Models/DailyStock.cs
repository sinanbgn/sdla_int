﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class DailyStock
    {
        public long Id { get; set; }
        public string MainCompanyCode { get; set; }
        public string WarehouseCode { get; set; }
        public string StockDate { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public long Quantity { get; set; }
        public long PackageCapacity { get; set; }
        public long PalleteCapacity { get; set; }
        public string Location { get; set; }
        public string ExpireDate { get; set; }
        public string LotCode { get; set; }
        public int StockSourceCode { get; set; }
        public string StockSource { get; set; }
        public int? CreateId { get; set; }
        public int? UpdateId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string FreeText { get; set; }
        public int SyncStatusCode { get; set; }
        public string SyncStatus { get; set; }
        public string SyncDesc { get; set; }
        public DateTime StockDateSql { get; set; }
        public string ItemSmallUnit { get; set; }
    }
}

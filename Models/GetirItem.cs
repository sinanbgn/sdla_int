﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class GetirItem
    {
        public GetirItem()
        {
            GetirItemBarcode = new HashSet<GetirItemBarcode>();
            GetirItemPackType = new HashSet<GetirItemPackType>();
        }

        public long Id { get; set; }
        public string AbcCode { get; set; }
        public bool AllocatesAgainstQcpolicy { get; set; }
        public int? CountingCycle { get; set; }
        public string Description { get; set; }
        public string InventItemGroup { get; set; }
        public int? InventoryItemAllowedExpDate { get; set; }
        public string InventoryItemCode { get; set; }
        public bool? InventoryItemExpDateTracking { get; set; }
        public bool? InventoryItemLotTracking { get; set; }
        public int? InventoryItemShelfLife { get; set; }
        public int? InventoryItemWarningForExpDate { get; set; }
        public int? InventoryItemWarningForExpDateMin { get; set; }
        public bool IsFragile { get; set; }
        public bool IsKitItem { get; set; }
        public string ItemDetailCategory { get; set; }
        public string ItemMainCategory { get; set; }
        public string ItemSubCategory { get; set; }
        public string ItemType { get; set; }
        public int? MinimumOrderQty { get; set; }
        public string Notes { get; set; }
        public string QualityPolicy { get; set; }
        public string SafetyStockCu { get; set; }
        public string SecondaryCode { get; set; }
        public string Volume { get; set; }
        public string Weight { get; set; }
        public string IntegrationCode { get; set; }
        public int? CreateId { get; set; }
        public int? UpdateId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string FreeText { get; set; }
        public int SyncStatusCode { get; set; }
        public string SyncStatus { get; set; }
        public string SyncDesc { get; set; }

        public virtual ICollection<GetirItemBarcode> GetirItemBarcode { get; set; }
        public virtual ICollection<GetirItemPackType> GetirItemPackType { get; set; }
    }
}

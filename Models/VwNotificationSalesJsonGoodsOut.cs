﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class VwNotificationSalesJsonGoodsOut
    {
        public string Sender { get; set; }
        public string Receiver { get; set; }
        public string ActionType { get; set; }
        public string DocumentNumber { get; set; }
        public string DocumentDate { get; set; }
        public string Note { get; set; }
        public int? DeactivationNote { get; set; }
        public string ExportReceiverNote { get; set; }
        public int? ExportCountryCode { get; set; }
        public string ImportSenderNote { get; set; }
        public string ImportCountryCode { get; set; }
        public string ReturnNote { get; set; }
        public string DestructionNote { get; set; }
        public string IdTaxNo { get; set; }
        public string SerialNumber { get; set; }
        public string LotNumber { get; set; }
        public string GtinNumber { get; set; }
        public string ParentCarrierNo { get; set; }
        public string CarrierNo { get; set; }
        public string ProductNote { get; set; }
        public string ProductionDate { get; set; }
        public string ExpirationDate { get; set; }
        public string MinistryPortalKey { get; set; }
        public bool IsSendToMinistry { get; set; }
        public string MainCompanyBranch { get; set; }
        public string StatusCode { get; set; }
        public int? SendNotification { get; set; }
        public string GoodsOutCode { get; set; }
        public string AdetStatus { get; set; }
    }
}

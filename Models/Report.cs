﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class Report
    {
        public long Id { get; set; }
        public string MainCompanyCode { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public int? CreateId { get; set; }
        public int? UpdateId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
    }
}

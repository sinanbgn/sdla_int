﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class WvGoodsInOutLineCountPowerBi
    {
        public string ReceReceiptdatetime { get; set; }
        public int? LineCount { get; set; }
        public string ReceDepositorid { get; set; }
        public string DepoInvoicereportscode { get; set; }
        public string DepoDescription { get; set; }
        public string Type { get; set; }
    }
}

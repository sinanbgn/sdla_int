﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class VwGetirOrderStatusAxata
    {
        public long Id { get; set; }
        public string DocumentId { get; set; }
        public string Status { get; set; }
        public string StatusDate { get; set; }
        public int OrderType { get; set; }
        public string ApiToken { get; set; }
        public string SubType { get; set; }
        public string IntegrationResponseCode { get; set; }
        public string IntegrationResponseMessage { get; set; }
        public DateTime? IntegrationDate { get; set; }
    }
}

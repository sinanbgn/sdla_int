﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class ItemPackType
    {
        public long Id { get; set; }
        public string ItemCode { get; set; }
        public string IsDefault { get; set; }
        public string TypeCode { get; set; }
        public string TypeDescription { get; set; }
        public int IncludeQuantity { get; set; }
        public int UnitType { get; set; }
        public double? ItemGrossWeight { get; set; }
        public double? ItemNetWeight { get; set; }
        public double? ItemVolume { get; set; }
        public double? ItemWidht { get; set; }
        public double? ItemLength { get; set; }
        public double? ItemHeight { get; set; }
        public int? CreateId { get; set; }
        public int? UpdateId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string FreeText { get; set; }
        public int SyncStatusCode { get; set; }
        public string SyncStatus { get; set; }
        public string SyncDesc { get; set; }
        public long? ItemId { get; set; }

        public virtual Item Item { get; set; }
    }
}

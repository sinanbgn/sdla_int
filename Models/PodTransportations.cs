﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class PodTransportations
    {
        public PodTransportations()
        {
            PodLocationLog = new HashSet<PodLocationLog>();
            PodShipments = new HashSet<PodShipments>();
        }

        public long Id { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string Code { get; set; }
        public string Createdatetime { get; set; }
        public string Updatedatetime { get; set; }
        public string Updateuser { get; set; }
        public string Departureaddress { get; set; }
        public string Etddate { get; set; }
        public string Etdtime { get; set; }
        public string Ptddate { get; set; }
        public string Ptdtime { get; set; }
        public string Atddate { get; set; }
        public string Atdtime { get; set; }
        public string Arrivaladdress { get; set; }
        public string Etadate { get; set; }
        public string Etatime { get; set; }
        public string Ptadate { get; set; }
        public string Ptatime { get; set; }
        public string Atadate { get; set; }
        public string Atatime { get; set; }
        public string Transporttypecode { get; set; }
        public string Cargotypecode { get; set; }
        public string Statuscode { get; set; }
        public int? Calculateddistance { get; set; }
        public int? Calculatedduration { get; set; }
        public long? Driverseq { get; set; }
        public string Driverexternalsystemid { get; set; }
        public string Drivername { get; set; }
        public string Drivermobilenumber { get; set; }
        public long? Vehicleseq { get; set; }
        public string Departureaddressfree { get; set; }
        public string Arrivaladdressfree { get; set; }
        public string Carriercode { get; set; }
        public string Externalsystemid { get; set; }
        public bool? Archivedyn { get; set; }
        public string Lastlocationlatitude { get; set; }
        public string Lastlocationlongitude { get; set; }

        public virtual ICollection<PodLocationLog> PodLocationLog { get; set; }
        public virtual ICollection<PodShipments> PodShipments { get; set; }
    }
}

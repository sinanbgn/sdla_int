﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class Log
    {
        public long Id { get; set; }
        public string Source { get; set; }
        public string MainCompanyCode { get; set; }
        public string RequestUrl { get; set; }
        public string RequestMethod { get; set; }
        public string RequestBody { get; set; }
        public string ResponseBody { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ResponseDate { get; set; }
        public string HostAddress { get; set; }
        public string UserAgent { get; set; }
        public string Headers { get; set; }
        public int? StatusCode { get; set; }
        public string Status { get; set; }
        public string StatusMessage { get; set; }
        public string StoredProcedureName { get; set; }
        public int? ErrorLine { get; set; }
        public string ErrorMessage { get; set; }
        public int? SourceType { get; set; }
    }
}

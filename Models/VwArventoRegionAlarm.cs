﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class VwArventoRegionAlarm
    {
        public string UserName { get; set; }
        public string Pin1 { get; set; }
        public string Pin2 { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string GroupName { get; set; }
        public int? GroupId { get; set; }
    }
}

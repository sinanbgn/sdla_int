﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class WhOnTimeGoodsInPowerBi
    {
        public string Whdescription { get; set; }
        public string Company { get; set; }
        public string Warehouse { get; set; }
        public string WarehouseDescription { get; set; }
        public string ActTypeOrderNo { get; set; }
        public DateTime? Date { get; set; }
        public int? EnteredPaletteCount { get; set; }
        public int? ReferenceTime { get; set; }
        public int? GoodsInMinute { get; set; }
        public int? Miktar { get; set; }
        public string Type { get; set; }
        public decimal? GoodsInWeight { get; set; }
        public string OderNo { get; set; }
        public string ActType { get; set; }
    }
}

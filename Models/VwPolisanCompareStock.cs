﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class VwPolisanCompareStock
    {
        public string WarehouseCode { get; set; }
        public string WarehouseName { get; set; }
        public string StockDate { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string LotCode { get; set; }
        public long StockQuantity { get; set; }
        public long CustomerStockQuantity { get; set; }
        public long? StockDiff { get; set; }
    }
}

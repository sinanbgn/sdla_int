﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SdLa_Int.Models
{
    public class Transport
    {
        public string code { get; set; }
        public string createdatetime { get; set; }
        public string updatedatetime { get; set; }
        public string updateuser { get; set; }
        public string departureaddress { get; set; }
        public DateTime etddate { get; set; }
        public string etdtime { get; set; }
        public DateTime ptddate { get; set; }
        public string ptdtime { get; set; }
        public DateTime atddate { get; set; }
        public string atdtime { get; set; }
        public string arrivaladdress { get; set; }
        public string arrivalnotes { get; set; }
        public DateTime etadate { get; set; }
        public string etatime { get; set; }
        public DateTime ptadate { get; set; }
        public string ptatime { get; set; }
        public DateTime atadate { get; set; }
        public string atatime { get; set; }
        public string transporttypecode { get; set; }
        public string cargotypecode { get; set; }
        public string statuscode { get; set; }
        public int calculateddistance { get; set; }
        public int calculatedduration { get; set; }
        public int driverseq { get; set; }
        public string driverexternalsystemid { get; set; }
        public string drivername { get; set; }
        public string drivermobilenumber { get; set; }
        public int vehicleseq { get; set; }
        public string departureaddressfree { get; set; }
        public string arrivaladdressfree { get; set; }
        public string carriercode { get; set; }
        public string externalsystemid { get; set; }
        public string archivedyn { get; set; }
        public string  lastlocationlatitude { get; set; }
        public string lastlocationlongitude { get; set; }
        public IList<object> locationupdates { get; set; }
        public IList<Shipment> shipments { get; set; }
    }
}

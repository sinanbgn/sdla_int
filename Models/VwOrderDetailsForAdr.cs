﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class VwOrderDetailsForAdr
    {
        public long? Id { get; set; }
        public string OrderCode { get; set; }
        public string SoldToCode { get; set; }
        public string SoldToDescription { get; set; }
        public string SoldToAddress { get; set; }
        public string SoldToTown { get; set; }
        public string SoldToCity { get; set; }
        public string ShipToCode { get; set; }
        public string ShipToDescription { get; set; }
        public string ShipToAddress { get; set; }
        public string ShipFromDescription { get; set; }
        public string ShipFromAddress { get; set; }
        public string ShipFromDist { get; set; }
        public string ShipFromTaxDepNo { get; set; }
        public string ShipFromDocNo { get; set; }
        public string ShipToCity { get; set; }
        public string ShipToTown { get; set; }
        public DateTime? ShipmentDate { get; set; }
        public string ItemCode { get; set; }
        public string ItemDescription { get; set; }
        public decimal? Volume { get; set; }
        public decimal? Weight { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? PackageQuantity { get; set; }
        public string TaxOffice { get; set; }
        public string TaxCode { get; set; }
        public string ShipmentCode { get; set; }
        public string AdrunNumber { get; set; }
        public string AdritemDescription { get; set; }
        public string AdrmainRiskClass { get; set; }
        public string AdrpackageGroup { get; set; }
        public string AdrtunnelRestrictionCode { get; set; }
    }
}

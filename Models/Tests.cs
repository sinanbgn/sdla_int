﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class Tests
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime CreateDate { get; set; }
    }
}

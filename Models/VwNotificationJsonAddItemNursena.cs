﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class VwNotificationJsonAddItemNursena
    {
        public string Sender { get; set; }
        public string HeaderId { get; set; }
        public string SerialNumber { get; set; }
        public string LotNumber { get; set; }
        public string GtinNumber { get; set; }
        public string ParentCarrierNo { get; set; }
        public string CarrierNo { get; set; }
        public string ProductNote { get; set; }
        public string ProductionDate { get; set; }
        public string ExpirationDate { get; set; }
        public string MinistryPortalKey { get; set; }
        public bool IsSendToMinistry { get; set; }
        public string MainCompanyBranch { get; set; }
        public string QrCode { get; set; }
        public string ItemCode { get; set; }
        public string LotCode { get; set; }
        public string DocumentNumber { get; set; }
        public int NotificationActionType { get; set; }
        public string GoodsOutCode { get; set; }
        public string StatusCode { get; set; }
    }
}

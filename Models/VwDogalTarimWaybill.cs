﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class VwDogalTarimWaybill
    {
        public string CompanyName { get; set; }
        public string DeliveryNumber { get; set; }
        public string DeliveryDate { get; set; }
        public DateTime RegulatedDate { get; set; }
        public string OrderNumber { get; set; }
        public string CustomerCode { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public int? Quantity { get; set; }
        public string UnitCode { get; set; }
        public string CreateDate { get; set; }
    }
}

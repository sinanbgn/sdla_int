﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class VwOrderStatusHemel
    {
        public string OrderCode { get; set; }
        public int StatusCode { get; set; }
        public string StatusDate { get; set; }
    }
}

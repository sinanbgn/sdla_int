﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class PodShipments
    {
        public PodShipments()
        {
            PodSurveyAnswers = new HashSet<PodSurveyAnswers>();
        }

        public long Id { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string TransportationCode { get; set; }
        public long? TransportationId { get; set; }
        public long? Seq { get; set; }
        public string Code { get; set; }
        public string Externalsystemid { get; set; }
        public string Createdatetime { get; set; }
        public string Updatedatetime { get; set; }
        public string Updateuser { get; set; }
        public string Dataorigin { get; set; }
        public string Requireddeliverydate { get; set; }
        public string Pickupaddress { get; set; }
        public string Deliveryaddress { get; set; }
        public string Requiredpickupdate { get; set; }
        public string Receiver { get; set; }
        public string Debtor { get; set; }
        public string Shipmenttype { get; set; }
        public string Yourreference { get; set; }
        public string Ourreference { get; set; }
        public string Movementtype { get; set; }
        public string Cargotype { get; set; }
        public string Statuscode { get; set; }
        public bool? Activeyn { get; set; }
        public string Pickuplatitude { get; set; }
        public string Pickuplongitude { get; set; }
        public string Deliverylatitude { get; set; }
        public string Deliverylongitude { get; set; }
        public string Pickupaddressfree { get; set; }
        public string Deliveryaddressfree { get; set; }
        public string Confirmationdate { get; set; }
        public string Ptddate { get; set; }
        public string Ptdtime { get; set; }
        public string Atddate { get; set; }
        public string Atdtime { get; set; }
        public string Ptadate { get; set; }
        public string Ptatime { get; set; }
        public string Atadate { get; set; }
        public string Atatime { get; set; }
        public bool? Notifyonpickup { get; set; }
        public bool? Notifyondelivery { get; set; }
        public string Dynamicstatus { get; set; }
        public string Receivercontactinfo { get; set; }
        public bool? Archivedyn { get; set; }
        public string Deliveryphoto1 { get; set; }
        public string Deliveryfilename1 { get; set; }
        public string Deliverymimetype1 { get; set; }
        public string Deliverysignatureby { get; set; }
        public int? Driverseq { get; set; }
        public string Driverexternalsystemid { get; set; }
        public string Drivername { get; set; }
        public string Drivermobilenumber { get; set; }
        public string Lastlocationlatitude { get; set; }
        public string Lastlocationlongitude { get; set; }

        public virtual PodTransportations Transportation { get; set; }
        public virtual ICollection<PodSurveyAnswers> PodSurveyAnswers { get; set; }
    }
}

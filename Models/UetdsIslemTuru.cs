﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class UetdsIslemTuru
    {
        public long Id { get; set; }
        public string IslemTuru { get; set; }
        public int IslemId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class Warehouses
    {
        public long Id { get; set; }
        public string WarehouseCode { get; set; }
        public string WarehouseName { get; set; }
        public string CountryCode { get; set; }
        public string CityName { get; set; }
        public string CityCode { get; set; }
        public string TownName { get; set; }
        public string TownCode { get; set; }
        public decimal? StorageSpace { get; set; }
        public decimal? StorageVolume { get; set; }
        public decimal? StoreVolume { get; set; }
        public int? StorablePalletsPiece { get; set; }
        public int? CreateId { get; set; }
        public int? UpdateId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
    }
}

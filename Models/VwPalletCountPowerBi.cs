﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class VwPalletCountPowerBi
    {
        public string StockDate { get; set; }
        public string Invoicereportscode { get; set; }
        public string DsGroupCode { get; set; }
        public decimal? PalletCount { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class Documents
    {
        public long Id { get; set; }
        public int MdocId { get; set; }
        public string MdocName { get; set; }
        public string MdocMessagetype { get; set; }
        public string MdocIdocnumber { get; set; }
        public DateTime MdocEntrydatetime { get; set; }
        public DateTime MdocFromsystem { get; set; }
        public string MdocTosystem { get; set; }
        public string MdocErrortext { get; set; }
        public string MdocReferance { get; set; }
        public int MdocType { get; set; }
        public int? CreateId { get; set; }
        public int? UpdateId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class MainCompany
    {
        public MainCompany()
        {
            QuestionAnswer = new HashSet<QuestionAnswer>();
        }

        public string Code { get; set; }
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string PersonInCharge { get; set; }
        public string EmailAddress { get; set; }
        public bool Status { get; set; }

        public virtual ICollection<QuestionAnswer> QuestionAnswer { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class VwNotificationSalesJsonEx
    {
        public string Sender { get; set; }
        public string Receiver { get; set; }
        public string ActionType { get; set; }
        public string DocumentNumber { get; set; }
        public string DocumentDate { get; set; }
        public string Note { get; set; }
        public int DeactivationNote { get; set; }
        public string ExportReceiverNote { get; set; }
        public string ExportCountryCode { get; set; }
        public string ImportSenderNote { get; set; }
        public string ImportCountryCode { get; set; }
        public string ReturnNote { get; set; }
        public string DestructionNote { get; set; }
        public string IdTaxNo { get; set; }
        public string SerialNumber { get; set; }
        public string LotNumber { get; set; }
        public string GtinNumber { get; set; }
        public string ParentCarrierNo { get; set; }
        public string CarrierNo { get; set; }
        public string ProductNote { get; set; }
        public string ProductionDate { get; set; }
        public string ExpirationDate { get; set; }
        public string MinistryPortalKey { get; set; }
        public bool IsSendToMinistry { get; set; }
        public string MainCompanyBranch { get; set; }
        public string QrCode { get; set; }
        public string ItemCode { get; set; }
        public string LotCode { get; set; }
    }
}

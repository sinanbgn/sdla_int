﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class GetirGoodsOutOrder
    {
        public GetirGoodsOutOrder()
        {
            GetirGoodsOutOrderLine = new HashSet<GetirGoodsOutOrderLine>();
        }

        public long Id { get; set; }
        public string WaorCode { get; set; }
        public string WaorCustomerAddressCode { get; set; }
        public string WaorDepositorOrderNo { get; set; }
        public string WaorInventorySiteCode { get; set; }
        public string WaorNotes { get; set; }
        public string WaorWarehouseOrderTypeId { get; set; }
        public string OrderReferenceCode { get; set; }
        public string IntegrationCode { get; set; }
        public int? CreateId { get; set; }
        public int? UpdateId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string FreeText { get; set; }
        public int SyncStatusCode { get; set; }
        public string SyncStatus { get; set; }
        public string SyncDesc { get; set; }

        public virtual ICollection<GetirGoodsOutOrderLine> GetirGoodsOutOrderLine { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class VwDailyStock
    {
        public long? RowNumber { get; set; }
        public string MainCompanyCode { get; set; }
        public string StockDate { get; set; }
        public string ExpireDate { get; set; }
        public string ItemCode { get; set; }
        public string WarehouseCode { get; set; }
        public long? Quantity { get; set; }
        public string LotCode { get; set; }
        public string LocationCode { get; set; }
    }
}

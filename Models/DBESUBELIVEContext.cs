﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SdLa_Int.Models
{
    public partial class DBESUBELIVEContext : DbContext
    {
        public DBESUBELIVEContext()
        {
        }

        public DBESUBELIVEContext(DbContextOptions<DBESUBELIVEContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ClientKey> ClientKey { get; set; }
        public virtual DbSet<Company> Company { get; set; }
        public virtual DbSet<DailyStock> DailyStock { get; set; }
        public virtual DbSet<Documents> Documents { get; set; }
        public virtual DbSet<ExVwDailyStockSnapShotHemel> ExVwDailyStockSnapShotHemel { get; set; }
        public virtual DbSet<GetirCompany> GetirCompany { get; set; }
        public virtual DbSet<GetirGoodsInOrder> GetirGoodsInOrder { get; set; }
        public virtual DbSet<GetirGoodsInOrderLine> GetirGoodsInOrderLine { get; set; }
        public virtual DbSet<GetirGoodsOutOrder> GetirGoodsOutOrder { get; set; }
        public virtual DbSet<GetirGoodsOutOrderLine> GetirGoodsOutOrderLine { get; set; }
        public virtual DbSet<GetirItem> GetirItem { get; set; }
        public virtual DbSet<GetirItemBarcode> GetirItemBarcode { get; set; }
        public virtual DbSet<GetirItemPackType> GetirItemPackType { get; set; }
        public virtual DbSet<IntegrationEnumType> IntegrationEnumType { get; set; }
        public virtual DbSet<Item> Item { get; set; }
        public virtual DbSet<ItemBarcode> ItemBarcode { get; set; }
        public virtual DbSet<ItemEachCustomer> ItemEachCustomer { get; set; }
        public virtual DbSet<ItemPackType> ItemPackType { get; set; }
        public virtual DbSet<Log> Log { get; set; }
        public virtual DbSet<MainCompany> MainCompany { get; set; }
        public virtual DbSet<MigrationHistory> MigrationHistory { get; set; }
        public virtual DbSet<Order> Order { get; set; }
        public virtual DbSet<OrderLine> OrderLine { get; set; }
        public virtual DbSet<PodLocationLog> PodLocationLog { get; set; }
        public virtual DbSet<PodShipments> PodShipments { get; set; }
        public virtual DbSet<PodSurveyAnswers> PodSurveyAnswers { get; set; }
        public virtual DbSet<PodTransportations> PodTransportations { get; set; }
        public virtual DbSet<Question> Question { get; set; }
        public virtual DbSet<QuestionAnswer> QuestionAnswer { get; set; }
        public virtual DbSet<QuestionHeader> QuestionHeader { get; set; }
        public virtual DbSet<Report> Report { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<Tender> Tender { get; set; }
        public virtual DbSet<TenderAnswer> TenderAnswer { get; set; }
        public virtual DbSet<TenderStop> TenderStop { get; set; }
        public virtual DbSet<Tests> Tests { get; set; }
        public virtual DbSet<Token> Token { get; set; }
        public virtual DbSet<UetdsIslemTuru> UetdsIslemTuru { get; set; }
        public virtual DbSet<UetdsLog> UetdsLog { get; set; }
        public virtual DbSet<UetdsYukKaydi> UetdsYukKaydi { get; set; }
        public virtual DbSet<UetdsYukKaydiDetayi> UetdsYukKaydiDetayi { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserRole> UserRole { get; set; }
        public virtual DbSet<VwArventoRegionAlarm> VwArventoRegionAlarm { get; set; }
        public virtual DbSet<VwDailyStock> VwDailyStock { get; set; }
        public virtual DbSet<VwDailyStockAxata> VwDailyStockAxata { get; set; }
        public virtual DbSet<VwDogalTarimWaybill> VwDogalTarimWaybill { get; set; }
        public virtual DbSet<VwDogalWaybillDelete> VwDogalWaybillDelete { get; set; }
        public virtual DbSet<VwGetirIntSendGoodsInAxata> VwGetirIntSendGoodsInAxata { get; set; }
        public virtual DbSet<VwGetirIntSendGoodsOutAxata> VwGetirIntSendGoodsOutAxata { get; set; }
        public virtual DbSet<VwGetirIntSendGoodsOutAxataNew> VwGetirIntSendGoodsOutAxataNew { get; set; }
        public virtual DbSet<VwGetirIntSendGoodsOutcloseAxata> VwGetirIntSendGoodsOutcloseAxata { get; set; }
        public virtual DbSet<VwGetirIntSendSpeedyGoodsOutAxata> VwGetirIntSendSpeedyGoodsOutAxata { get; set; }
        public virtual DbSet<VwGetirOrderStatusAxata> VwGetirOrderStatusAxata { get; set; }
        public virtual DbSet<VwNotificationJsonAddItem> VwNotificationJsonAddItem { get; set; }
        public virtual DbSet<VwNotificationJsonAddItemNursena> VwNotificationJsonAddItemNursena { get; set; }
        public virtual DbSet<VwNotificationJsonAddItemSinan> VwNotificationJsonAddItemSinan { get; set; }
        public virtual DbSet<VwNotificationJsonForBox> VwNotificationJsonForBox { get; set; }
        public virtual DbSet<VwNotificationJsonForPallet> VwNotificationJsonForPallet { get; set; }
        public virtual DbSet<VwNotificationSalesJson20200304> VwNotificationSalesJson20200304 { get; set; }
        public virtual DbSet<VwNotificationSalesJsonEx> VwNotificationSalesJsonEx { get; set; }
        public virtual DbSet<VwNotificationSalesJsonGoodsOut> VwNotificationSalesJsonGoodsOut { get; set; }
        public virtual DbSet<VwNotificationSalesJsonGoodsOutAddItem> VwNotificationSalesJsonGoodsOutAddItem { get; set; }
        public virtual DbSet<VwNotificationSalesJsonGoodsOutExample> VwNotificationSalesJsonGoodsOutExample { get; set; }
        public virtual DbSet<VwOrderDetailsForAdr> VwOrderDetailsForAdr { get; set; }
        public virtual DbSet<VwOrderGoodsInHemel> VwOrderGoodsInHemel { get; set; }
        public virtual DbSet<VwOrderGoodsOutFmc> VwOrderGoodsOutFmc { get; set; }
        public virtual DbSet<VwOrderGoodsOutHemel> VwOrderGoodsOutHemel { get; set; }
        public virtual DbSet<VwOrderStatusAllCustomer> VwOrderStatusAllCustomer { get; set; }
        public virtual DbSet<VwOrderStatusGetir> VwOrderStatusGetir { get; set; }
        public virtual DbSet<VwOrderStatusHemel> VwOrderStatusHemel { get; set; }
        public virtual DbSet<VwOrderWaybillAllCustomer> VwOrderWaybillAllCustomer { get; set; }
        public virtual DbSet<VwOrderWaybillGetir> VwOrderWaybillGetir { get; set; }
        public virtual DbSet<VwPalletCountPowerBi> VwPalletCountPowerBi { get; set; }
        public virtual DbSet<VwPolisanCompareStock> VwPolisanCompareStock { get; set; }
        public virtual DbSet<VwShippedOrderLines> VwShippedOrderLines { get; set; }
        public virtual DbSet<Warehouses> Warehouses { get; set; }
        public virtual DbSet<WhOnTimeGoodsInPowerBi> WhOnTimeGoodsInPowerBi { get; set; }
        public virtual DbSet<WvGoodsInOutLineCountPowerBi> WvGoodsInOutLineCountPowerBi { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Data Source=10.61.1.10;Initial Catalog=DBESUBELIVE;Integrated Security=False;Persist Security Info=False;User ID=onsel.aydin;Password=Oa123456+");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ClientKey>(entity =>
            {
                entity.Property(e => e.ClientKeyId).HasColumnName("ClientKeyID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

                entity.Property(e => e.CreateOn).HasColumnType("datetime");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<Company>(entity =>
            {
                entity.HasIndex(e => e.OrderNewId)
                    .HasName("IX_OrderNew_Id");

                entity.Property(e => e.ContactName).HasMaxLength(12);

                entity.Property(e => e.Gln).HasColumnName("GLN");

                entity.Property(e => e.OrderNewId).HasColumnName("OrderNew_Id");

                entity.HasOne(d => d.OrderNew)
                    .WithMany(p => p.Company)
                    .HasForeignKey(d => d.OrderNewId)
                    .HasConstraintName("FK_dbo.Company_dbo.Order_OrderNew_Id");
            });

            modelBuilder.Entity<DailyStock>(entity =>
            {
                entity.HasIndex(e => e.StockDate)
                    .HasName("DailyStock__StockCode_index");

                entity.HasIndex(e => new { e.MainCompanyCode, e.StockDate })
                    .HasName("idx_MainCompany");

                entity.Property(e => e.ExpireDate).HasColumnName("expireDate");

                entity.Property(e => e.ItemCode).HasColumnName("itemCode");

                entity.Property(e => e.ItemName).HasColumnName("itemName");

                entity.Property(e => e.ItemSmallUnit).HasColumnName("itemSmallUnit");

                entity.Property(e => e.Location).HasColumnName("location");

                entity.Property(e => e.LotCode).HasColumnName("lotCode");

                entity.Property(e => e.MainCompanyCode)
                    .HasColumnName("mainCompanyCode")
                    .HasMaxLength(10);

                entity.Property(e => e.PackageCapacity).HasColumnName("packageCapacity");

                entity.Property(e => e.PalleteCapacity).HasColumnName("palleteCapacity");

                entity.Property(e => e.Quantity).HasColumnName("quantity");

                entity.Property(e => e.StockDate)
                    .IsRequired()
                    .HasColumnName("stockDate")
                    .HasMaxLength(10);

                entity.Property(e => e.StockDateSql)
                    .HasColumnName("stockDateSQL")
                    .HasColumnType("datetime");

                entity.Property(e => e.WarehouseCode)
                    .IsRequired()
                    .HasColumnName("warehouseCode");
            });

            modelBuilder.Entity<Documents>(entity =>
            {
                entity.Property(e => e.MdocEntrydatetime)
                    .HasColumnName("MDOC_ENTRYDATETIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.MdocErrortext).HasColumnName("MDOC_ERRORTEXT");

                entity.Property(e => e.MdocFromsystem)
                    .HasColumnName("MDOC_FROMSYSTEM")
                    .HasColumnType("datetime");

                entity.Property(e => e.MdocId).HasColumnName("MDOC_ID");

                entity.Property(e => e.MdocIdocnumber).HasColumnName("MDOC_IDOCNUMBER");

                entity.Property(e => e.MdocMessagetype).HasColumnName("MDOC_MESSAGETYPE");

                entity.Property(e => e.MdocName).HasColumnName("MDOC_NAME");

                entity.Property(e => e.MdocReferance).HasColumnName("MDOC_REFERANCE");

                entity.Property(e => e.MdocTosystem).HasColumnName("MDOC_TOSYSTEM");

                entity.Property(e => e.MdocType).HasColumnName("MDOC_TYPE");
            });

            modelBuilder.Entity<ExVwDailyStockSnapShotHemel>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("ex_vw_dailyStockSnapShotHemel");

                entity.Property(e => e.Depo)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.LotNumarasi).HasMaxLength(60);

                entity.Property(e => e.PaketTipi).HasMaxLength(100);

                entity.Property(e => e.StokTarihi)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.UrunAciklamasi).HasMaxLength(4000);

                entity.Property(e => e.UrunKodu)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<GetirCompany>(entity =>
            {
                entity.Property(e => e.PartyAddress)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.PartyAddressDistrict).HasMaxLength(500);

                entity.Property(e => e.PartyAddressEmail).HasMaxLength(500);

                entity.Property(e => e.PartyAddressLangtitude).HasMaxLength(500);

                entity.Property(e => e.PartyAddressLongtitude).HasMaxLength(500);

                entity.Property(e => e.PartyAddressPhone).HasMaxLength(50);

                entity.Property(e => e.PartyAddressTimeClosed).HasMaxLength(50);

                entity.Property(e => e.PartyAddressTimeopened).HasMaxLength(50);

                entity.Property(e => e.PartyCity).HasMaxLength(50);

                entity.Property(e => e.PartyCode)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.PartyDepositor).HasMaxLength(100);

                entity.Property(e => e.PartyDescription)
                    .IsRequired()
                    .HasMaxLength(400);

                entity.Property(e => e.PartyGeoArea).HasMaxLength(400);

                entity.Property(e => e.PartyPostcode).HasMaxLength(10);

                entity.Property(e => e.PartyTaxCode).HasMaxLength(20);

                entity.Property(e => e.PartyTaxoffice).HasMaxLength(100);

                entity.Property(e => e.PartyTown).HasMaxLength(100);
            });

            modelBuilder.Entity<GetirGoodsInOrder>(entity =>
            {
                entity.Property(e => e.OrderReferenceCode).HasMaxLength(50);

                entity.Property(e => e.WareCode).HasMaxLength(100);

                entity.Property(e => e.WareDepositorReceiptNo).HasMaxLength(100);

                entity.Property(e => e.WareInventorySiteCode).HasMaxLength(10);

                entity.Property(e => e.WareNotes).HasMaxLength(250);

                entity.Property(e => e.WarePlannedReceiptDate).HasMaxLength(100);

                entity.Property(e => e.WareSupplierAddressCode).HasMaxLength(250);

                entity.Property(e => e.WareWarehouseReceiptTypeId).HasColumnName("WareWarehouseReceiptTypeID");
            });

            modelBuilder.Entity<GetirGoodsInOrderLine>(entity =>
            {
                entity.HasIndex(e => e.OrderId)
                    .HasName("IX_order_Id");

                entity.Property(e => e.OrderId).HasColumnName("order_Id");

                entity.Property(e => e.WareInventoryItemDetailNotes).HasMaxLength(400);

                entity.Property(e => e.WareInventoryItemId)
                    .HasColumnName("WareInventoryItemID")
                    .HasMaxLength(100);

                entity.Property(e => e.WareInventoryItemPlanCuqty).HasColumnName("WareInventoryItemPlanCUQty");

                entity.Property(e => e.WareInventoryItemUnitId)
                    .HasColumnName("WareInventoryItemUnitID")
                    .HasMaxLength(40);

                entity.Property(e => e.WareInventoryItemWrdetailCode)
                    .HasColumnName("WareInventoryItemWRDetailCode")
                    .HasMaxLength(10);

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.GetirGoodsInOrderLine)
                    .HasForeignKey(d => d.OrderId)
                    .HasConstraintName("FK_dbo.GetirGoodsInOrderLine_dbo.GetirGoodsInOrder_order_Id");
            });

            modelBuilder.Entity<GetirGoodsOutOrder>(entity =>
            {
                entity.Property(e => e.OrderReferenceCode).HasMaxLength(50);

                entity.Property(e => e.WaorCode).HasMaxLength(100);

                entity.Property(e => e.WaorCustomerAddressCode).HasMaxLength(250);

                entity.Property(e => e.WaorDepositorOrderNo).HasMaxLength(100);

                entity.Property(e => e.WaorInventorySiteCode).HasMaxLength(10);

                entity.Property(e => e.WaorNotes).HasMaxLength(250);

                entity.Property(e => e.WaorWarehouseOrderTypeId).HasColumnName("WaorWarehouseOrderTypeID");
            });

            modelBuilder.Entity<GetirGoodsOutOrderLine>(entity =>
            {
                entity.HasIndex(e => e.OrderId)
                    .HasName("IX_order_Id");

                entity.Property(e => e.OrderId).HasColumnName("order_Id");

                entity.Property(e => e.WaorInventoryItemCode).HasMaxLength(100);

                entity.Property(e => e.WaorInventoryItemDetailNo).HasMaxLength(3);

                entity.Property(e => e.WaorInventoryItemNotes).HasMaxLength(400);

                entity.Property(e => e.WaorInventoryItemPlanCuqty).HasColumnName("WaorInventoryItemPlanCUQty");

                entity.Property(e => e.WaorInventoryItemUnitId)
                    .HasColumnName("WaorInventoryItemUnitID")
                    .HasMaxLength(40);

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.GetirGoodsOutOrderLine)
                    .HasForeignKey(d => d.OrderId)
                    .HasConstraintName("FK_dbo.GetirGoodsOutOrderLine_dbo.GetirGoodsOutOrder_order_Id");
            });

            modelBuilder.Entity<GetirItem>(entity =>
            {
                entity.Property(e => e.AbcCode).HasMaxLength(5);

                entity.Property(e => e.AllocatesAgainstQcpolicy).HasColumnName("AllocatesAgainstQCPolicy");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.InventItemGroup).HasMaxLength(50);

                entity.Property(e => e.InventoryItemCode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ItemDetailCategory).HasMaxLength(50);

                entity.Property(e => e.ItemMainCategory).HasMaxLength(50);

                entity.Property(e => e.ItemSubCategory).HasMaxLength(50);

                entity.Property(e => e.ItemType)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.MinimumOrderQty).HasColumnName("MinimumOrderQTY");

                entity.Property(e => e.SafetyStockCu).HasColumnName("SafetyStockCU");
            });

            modelBuilder.Entity<GetirItemBarcode>(entity =>
            {
                entity.HasIndex(e => e.ItemId)
                    .HasName("IX_Item_Id");

                entity.Property(e => e.Barcode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.InventoryItemPackType).HasMaxLength(50);

                entity.Property(e => e.ItemId).HasColumnName("Item_Id");

                entity.HasOne(d => d.Item)
                    .WithMany(p => p.GetirItemBarcode)
                    .HasForeignKey(d => d.ItemId)
                    .HasConstraintName("FK_dbo.GetirItemBarcode_dbo.GetirItem_Item_Id");
            });

            modelBuilder.Entity<GetirItemPackType>(entity =>
            {
                entity.HasIndex(e => e.ItemId)
                    .HasName("IX_Item_Id");

                entity.Property(e => e.Description).HasMaxLength(400);

                entity.Property(e => e.Height).HasMaxLength(10);

                entity.Property(e => e.IncludesCuquantity).HasColumnName("IncludesCUQuantity");

                entity.Property(e => e.ItemId).HasColumnName("Item_Id");

                entity.Property(e => e.Length).HasMaxLength(10);

                entity.Property(e => e.NetWeight).HasMaxLength(10);

                entity.Property(e => e.Volume).HasMaxLength(10);

                entity.Property(e => e.Weight).HasMaxLength(10);

                entity.Property(e => e.Widht).HasMaxLength(10);

                entity.HasOne(d => d.Item)
                    .WithMany(p => p.GetirItemPackType)
                    .HasForeignKey(d => d.ItemId)
                    .HasConstraintName("FK_dbo.GetirItemPackType_dbo.GetirItem_Item_Id");
            });

            modelBuilder.Entity<IntegrationEnumType>(entity =>
            {
                entity.Property(e => e.Description).HasMaxLength(300);

                entity.Property(e => e.IntegrationValue).HasMaxLength(300);

                entity.Property(e => e.MapType).HasMaxLength(300);

                entity.Property(e => e.MapValue).HasMaxLength(300);
            });

            modelBuilder.Entity<Item>(entity =>
            {
                entity.Property(e => e.AdrAdditionalRisk).HasMaxLength(300);

                entity.Property(e => e.AdrItemName).HasMaxLength(300);

                entity.Property(e => e.AdrMainRiskClass).HasMaxLength(300);

                entity.Property(e => e.AdrPackageGroup).HasMaxLength(300);

                entity.Property(e => e.AdrTunnelRestrictionCode).HasMaxLength(300);

                entity.Property(e => e.AdrTypeLabel).HasMaxLength(300);

                entity.Property(e => e.AdrUnNumber).HasMaxLength(300);

                entity.Property(e => e.Gtin).HasMaxLength(60);

                entity.Property(e => e.ItemCode).HasMaxLength(200);

                entity.Property(e => e.ItemDescription).HasMaxLength(400);

                entity.Property(e => e.ItemDetailCategory).HasMaxLength(50);

                entity.Property(e => e.ItemMainCategory).HasMaxLength(50);

                entity.Property(e => e.ItemSubCategory).HasMaxLength(50);
            });

            modelBuilder.Entity<ItemBarcode>(entity =>
            {
                entity.HasIndex(e => e.ItemId)
                    .HasName("IX_Item_Id");

                entity.Property(e => e.Barcode).HasMaxLength(50);

                entity.Property(e => e.ItemId).HasColumnName("Item_Id");

                entity.HasOne(d => d.Item)
                    .WithMany(p => p.ItemBarcode)
                    .HasForeignKey(d => d.ItemId)
                    .HasConstraintName("FK_dbo.ItemBarcode_dbo.Item_Item_Id");
            });

            modelBuilder.Entity<ItemEachCustomer>(entity =>
            {
                entity.HasIndex(e => e.ItemId)
                    .HasName("IX_Item_Id");

                entity.Property(e => e.ItemId).HasColumnName("Item_Id");

                entity.HasOne(d => d.Item)
                    .WithMany(p => p.ItemEachCustomer)
                    .HasForeignKey(d => d.ItemId)
                    .HasConstraintName("FK_dbo.ItemEachCustomer_dbo.Item_Item_Id");
            });

            modelBuilder.Entity<ItemPackType>(entity =>
            {
                entity.HasIndex(e => e.ItemId)
                    .HasName("IX_Item_Id");

                entity.Property(e => e.IncludeQuantity).HasColumnName("includeQuantity");

                entity.Property(e => e.IsDefault)
                    .HasColumnName("isDefault")
                    .HasMaxLength(1);

                entity.Property(e => e.ItemGrossWeight).HasColumnName("itemGrossWeight");

                entity.Property(e => e.ItemHeight).HasColumnName("itemHeight");

                entity.Property(e => e.ItemId).HasColumnName("Item_Id");

                entity.Property(e => e.ItemLength).HasColumnName("itemLength");

                entity.Property(e => e.ItemNetWeight).HasColumnName("itemNetWeight");

                entity.Property(e => e.ItemVolume).HasColumnName("itemVolume");

                entity.Property(e => e.ItemWidht).HasColumnName("itemWidht");

                entity.Property(e => e.TypeCode)
                    .HasColumnName("typeCode")
                    .HasMaxLength(40);

                entity.Property(e => e.TypeDescription)
                    .HasColumnName("typeDescription")
                    .HasMaxLength(400);

                entity.HasOne(d => d.Item)
                    .WithMany(p => p.ItemPackType)
                    .HasForeignKey(d => d.ItemId)
                    .HasConstraintName("FK_dbo.ItemPackType_dbo.Item_Item_Id");
            });

            modelBuilder.Entity<Log>(entity =>
            {
                entity.Property(e => e.SourceType).HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<MainCompany>(entity =>
            {
                entity.HasKey(e => e.Code)
                    .HasName("PK_dbo.MainCompany");

                entity.Property(e => e.Code).HasMaxLength(7);
            });

            modelBuilder.Entity<MigrationHistory>(entity =>
            {
                entity.HasKey(e => new { e.MigrationId, e.ContextKey })
                    .HasName("PK_dbo.__MigrationHistory");

                entity.ToTable("__MigrationHistory");

                entity.Property(e => e.MigrationId).HasMaxLength(150);

                entity.Property(e => e.ContextKey).HasMaxLength(300);

                entity.Property(e => e.Model).IsRequired();

                entity.Property(e => e.ProductVersion)
                    .IsRequired()
                    .HasMaxLength(32);
            });

            modelBuilder.Entity<Order>(entity =>
            {
                entity.Property(e => e.CustomerOrderCode).HasMaxLength(50);

                entity.Property(e => e.OrderCode).HasMaxLength(50);

                entity.Property(e => e.OrderDate).HasMaxLength(20);

                entity.Property(e => e.OrderId).HasMaxLength(50);

                entity.Property(e => e.OrderPriority).HasMaxLength(2);

                entity.Property(e => e.PlannedDeliveryDate).HasMaxLength(20);

                entity.Property(e => e.RelatedOrderCode).HasMaxLength(50);

                entity.Property(e => e.ShipmentCode).HasMaxLength(400);

                entity.Property(e => e.TempAddress).HasMaxLength(70);

                entity.Property(e => e.WarehouseCode).HasMaxLength(25);

                entity.Property(e => e.WaybillCode).HasMaxLength(50);
            });

            modelBuilder.Entity<OrderLine>(entity =>
            {
                entity.HasIndex(e => e.ItemId)
                    .HasName("IX_Item_Id");

                entity.HasIndex(e => e.OrderId)
                    .HasName("IX_Order_Id");

                entity.Property(e => e.ItemId).HasColumnName("Item_Id");

                entity.Property(e => e.LineCode).HasMaxLength(400);

                entity.Property(e => e.OrderId).HasColumnName("Order_Id");

                entity.HasOne(d => d.Item)
                    .WithMany(p => p.OrderLine)
                    .HasForeignKey(d => d.ItemId)
                    .HasConstraintName("FK_dbo.OrderLine_dbo.Item_Item_Id");

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.OrderLine)
                    .HasForeignKey(d => d.OrderId)
                    .HasConstraintName("FK_dbo.OrderLine_dbo.Order_Order_Id");
            });

            modelBuilder.Entity<PodLocationLog>(entity =>
            {
                entity.ToTable("POD_LocationLog");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Createdatetime)
                    .HasColumnName("createdatetime")
                    .HasMaxLength(30);

                entity.Property(e => e.Createusers)
                    .HasColumnName("createusers")
                    .HasMaxLength(50);

                entity.Property(e => e.Latitude)
                    .HasColumnName("latitude")
                    .HasMaxLength(30);

                entity.Property(e => e.Longitude)
                    .HasColumnName("longitude")
                    .HasMaxLength(30);

                entity.Property(e => e.Movementtime)
                    .HasColumnName("movementtime")
                    .HasMaxLength(30);

                entity.HasOne(d => d.Transportation)
                    .WithMany(p => p.PodLocationLog)
                    .HasForeignKey(d => d.TransportationId)
                    .HasConstraintName("FK_dbo.POD_LocationLog_POD_Transportations_Id");
            });

            modelBuilder.Entity<PodShipments>(entity =>
            {
                entity.ToTable("POD_Shipments");

                entity.Property(e => e.Activeyn).HasColumnName("activeyn");

                entity.Property(e => e.Archivedyn).HasColumnName("archivedyn");

                entity.Property(e => e.Atadate)
                    .HasColumnName("atadate")
                    .HasMaxLength(30);

                entity.Property(e => e.Atatime)
                    .HasColumnName("atatime")
                    .HasMaxLength(30);

                entity.Property(e => e.Atddate)
                    .HasColumnName("atddate")
                    .HasMaxLength(30);

                entity.Property(e => e.Atdtime)
                    .HasColumnName("atdtime")
                    .HasMaxLength(30);

                entity.Property(e => e.Cargotype)
                    .HasColumnName("cargotype")
                    .HasMaxLength(30);

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasMaxLength(26);

                entity.Property(e => e.Confirmationdate)
                    .HasColumnName("confirmationdate")
                    .HasMaxLength(30);

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Createdatetime)
                    .HasColumnName("createdatetime")
                    .HasMaxLength(30);

                entity.Property(e => e.Dataorigin)
                    .HasColumnName("dataorigin")
                    .HasMaxLength(30);

                entity.Property(e => e.Debtor)
                    .HasColumnName("debtor")
                    .HasMaxLength(100);

                entity.Property(e => e.Deliveryaddress)
                    .HasColumnName("deliveryaddress")
                    .HasMaxLength(500);

                entity.Property(e => e.Deliveryaddressfree)
                    .HasColumnName("deliveryaddressfree")
                    .HasMaxLength(500);

                entity.Property(e => e.Deliveryfilename1).HasColumnName("deliveryfilename1");

                entity.Property(e => e.Deliverylatitude)
                    .HasColumnName("deliverylatitude")
                    .HasMaxLength(30);

                entity.Property(e => e.Deliverylongitude)
                    .HasColumnName("deliverylongitude")
                    .HasMaxLength(30);

                entity.Property(e => e.Deliverymimetype1)
                    .HasColumnName("deliverymimetype1")
                    .HasMaxLength(50);

                entity.Property(e => e.Deliveryphoto1)
                    .HasColumnName("deliveryphoto1")
                    .HasColumnType("text");

                entity.Property(e => e.Deliverysignatureby)
                    .HasColumnName("deliverysignatureby")
                    .HasMaxLength(50);

                entity.Property(e => e.Driverexternalsystemid)
                    .HasColumnName("driverexternalsystemid")
                    .HasMaxLength(50);

                entity.Property(e => e.Drivermobilenumber)
                    .HasColumnName("drivermobilenumber")
                    .HasMaxLength(50);

                entity.Property(e => e.Drivername)
                    .HasColumnName("drivername")
                    .HasMaxLength(50);

                entity.Property(e => e.Driverseq).HasColumnName("driverseq");

                entity.Property(e => e.Dynamicstatus)
                    .HasColumnName("dynamicstatus")
                    .HasMaxLength(30);

                entity.Property(e => e.Externalsystemid)
                    .HasColumnName("externalsystemid")
                    .HasMaxLength(30);

                entity.Property(e => e.Lastlocationlatitude)
                    .HasColumnName("lastlocationlatitude")
                    .HasMaxLength(50);

                entity.Property(e => e.Lastlocationlongitude)
                    .HasColumnName("lastlocationlongitude")
                    .HasMaxLength(50);

                entity.Property(e => e.Movementtype)
                    .HasColumnName("movementtype")
                    .HasMaxLength(30);

                entity.Property(e => e.Notifyondelivery).HasColumnName("notifyondelivery");

                entity.Property(e => e.Notifyonpickup).HasColumnName("notifyonpickup");

                entity.Property(e => e.Ourreference).HasColumnName("ourreference");

                entity.Property(e => e.Pickupaddress)
                    .HasColumnName("pickupaddress")
                    .HasMaxLength(500);

                entity.Property(e => e.Pickupaddressfree)
                    .HasColumnName("pickupaddressfree")
                    .HasMaxLength(500);

                entity.Property(e => e.Pickuplatitude)
                    .HasColumnName("pickuplatitude")
                    .HasMaxLength(30);

                entity.Property(e => e.Pickuplongitude)
                    .HasColumnName("pickuplongitude")
                    .HasMaxLength(30);

                entity.Property(e => e.Ptadate)
                    .HasColumnName("ptadate")
                    .HasMaxLength(30);

                entity.Property(e => e.Ptatime)
                    .HasColumnName("ptatime")
                    .HasMaxLength(30);

                entity.Property(e => e.Ptddate)
                    .HasColumnName("ptddate")
                    .HasMaxLength(30);

                entity.Property(e => e.Ptdtime)
                    .HasColumnName("ptdtime")
                    .HasMaxLength(30);

                entity.Property(e => e.Receiver)
                    .HasColumnName("receiver")
                    .HasMaxLength(100);

                entity.Property(e => e.Receivercontactinfo)
                    .HasColumnName("receivercontactinfo")
                    .HasMaxLength(30);

                entity.Property(e => e.Requireddeliverydate)
                    .HasColumnName("requireddeliverydate")
                    .HasMaxLength(30);

                entity.Property(e => e.Requiredpickupdate)
                    .HasColumnName("requiredpickupdate")
                    .HasMaxLength(30);

                entity.Property(e => e.Seq).HasColumnName("seq");

                entity.Property(e => e.Shipmenttype)
                    .HasColumnName("shipmenttype")
                    .HasMaxLength(30);

                entity.Property(e => e.Statuscode)
                    .HasColumnName("statuscode")
                    .HasMaxLength(30);

                entity.Property(e => e.TransportationCode).HasMaxLength(50);

                entity.Property(e => e.Updatedatetime)
                    .HasColumnName("updatedatetime")
                    .HasMaxLength(30);

                entity.Property(e => e.Updateuser).HasColumnName("updateuser");

                entity.Property(e => e.Yourreference).HasColumnName("yourreference");

                entity.HasOne(d => d.Transportation)
                    .WithMany(p => p.PodShipments)
                    .HasForeignKey(d => d.TransportationId)
                    .HasConstraintName("FK_dbo.POD_Shipments_POD_Transportations_Id");
            });

            modelBuilder.Entity<PodSurveyAnswers>(entity =>
            {
                entity.ToTable("POD_SurveyAnswers");

                entity.Property(e => e.Answeredyn)
                    .HasColumnName("ANSWEREDYN")
                    .HasMaxLength(500);

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Driverfullname)
                    .HasColumnName("DRIVERFULLNAME")
                    .HasMaxLength(500);

                entity.Property(e => e.Forwho)
                    .HasColumnName("FORWHO")
                    .HasMaxLength(500);

                entity.Property(e => e.Forwhodetails)
                    .HasColumnName("FORWHODETAILS")
                    .HasMaxLength(500);

                entity.Property(e => e.Messagesentdatetime)
                    .HasColumnName("MESSAGESENTDATETIME")
                    .HasMaxLength(30);

                entity.Property(e => e.Questionarydescription)
                    .HasColumnName("QUESTIONARYDESCRIPTION")
                    .HasMaxLength(250);

                entity.Property(e => e.Questionaryintroductiontext)
                    .HasColumnName("QUESTIONARYINTRODUCTIONTEXT")
                    .HasMaxLength(500);

                entity.Property(e => e.Questiontext)
                    .HasColumnName("QUESTIONTEXT")
                    .HasMaxLength(500);

                entity.Property(e => e.Replydatetime)
                    .HasColumnName("REPLYDATETIME")
                    .HasMaxLength(30);

                entity.Property(e => e.Satisfactionrate)
                    .HasColumnName("SATISFACTIONRATE")
                    .HasMaxLength(500);

                entity.Property(e => e.Shipmentreceiver)
                    .HasColumnName("SHIPMENTRECEIVER")
                    .HasMaxLength(500);

                entity.HasOne(d => d.Shipment)
                    .WithMany(p => p.PodSurveyAnswers)
                    .HasForeignKey(d => d.ShipmentId)
                    .HasConstraintName("FK_dbo.POD_SurveyAnswers_POD_Shipments_Id");
            });

            modelBuilder.Entity<PodTransportations>(entity =>
            {
                entity.ToTable("POD_Transportations");

                entity.Property(e => e.Archivedyn).HasColumnName("archivedyn");

                entity.Property(e => e.Arrivaladdress)
                    .HasColumnName("arrivaladdress")
                    .HasMaxLength(30);

                entity.Property(e => e.Arrivaladdressfree)
                    .HasColumnName("arrivaladdressfree")
                    .HasMaxLength(500);

                entity.Property(e => e.Atadate)
                    .HasColumnName("atadate")
                    .HasMaxLength(30);

                entity.Property(e => e.Atatime)
                    .HasColumnName("atatime")
                    .HasMaxLength(15);

                entity.Property(e => e.Atddate)
                    .HasColumnName("atddate")
                    .HasMaxLength(30);

                entity.Property(e => e.Atdtime)
                    .HasColumnName("atdtime")
                    .HasMaxLength(15);

                entity.Property(e => e.Calculateddistance).HasColumnName("calculateddistance");

                entity.Property(e => e.Calculatedduration).HasColumnName("calculatedduration");

                entity.Property(e => e.Cargotypecode)
                    .HasColumnName("cargotypecode")
                    .HasMaxLength(30);

                entity.Property(e => e.Carriercode)
                    .HasColumnName("carriercode")
                    .HasMaxLength(50);

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasMaxLength(26);

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Createdatetime)
                    .HasColumnName("createdatetime")
                    .HasMaxLength(30);

                entity.Property(e => e.Departureaddress)
                    .HasColumnName("departureaddress")
                    .HasMaxLength(15);

                entity.Property(e => e.Departureaddressfree)
                    .HasColumnName("departureaddressfree")
                    .HasMaxLength(500);

                entity.Property(e => e.Driverexternalsystemid)
                    .HasColumnName("driverexternalsystemid")
                    .HasMaxLength(30);

                entity.Property(e => e.Drivermobilenumber)
                    .HasColumnName("drivermobilenumber")
                    .HasMaxLength(30);

                entity.Property(e => e.Drivername)
                    .HasColumnName("drivername")
                    .HasMaxLength(50);

                entity.Property(e => e.Driverseq).HasColumnName("driverseq");

                entity.Property(e => e.Etadate)
                    .HasColumnName("etadate")
                    .HasMaxLength(30);

                entity.Property(e => e.Etatime)
                    .HasColumnName("etatime")
                    .HasMaxLength(15);

                entity.Property(e => e.Etddate)
                    .HasColumnName("etddate")
                    .HasMaxLength(30);

                entity.Property(e => e.Etdtime)
                    .HasColumnName("etdtime")
                    .HasMaxLength(15);

                entity.Property(e => e.Externalsystemid)
                    .HasColumnName("externalsystemid")
                    .HasMaxLength(30);

                entity.Property(e => e.Lastlocationlatitude)
                    .HasColumnName("lastlocationlatitude")
                    .HasMaxLength(30);

                entity.Property(e => e.Lastlocationlongitude)
                    .HasColumnName("lastlocationlongitude")
                    .HasMaxLength(30);

                entity.Property(e => e.Ptadate)
                    .HasColumnName("ptadate")
                    .HasMaxLength(30);

                entity.Property(e => e.Ptatime)
                    .HasColumnName("ptatime")
                    .HasMaxLength(15);

                entity.Property(e => e.Ptddate)
                    .HasColumnName("ptddate")
                    .HasMaxLength(30);

                entity.Property(e => e.Ptdtime)
                    .HasColumnName("ptdtime")
                    .HasMaxLength(15);

                entity.Property(e => e.Statuscode)
                    .HasColumnName("statuscode")
                    .HasMaxLength(30);

                entity.Property(e => e.Transporttypecode)
                    .HasColumnName("transporttypecode")
                    .HasMaxLength(30);

                entity.Property(e => e.Updatedatetime)
                    .HasColumnName("updatedatetime")
                    .HasMaxLength(30);

                entity.Property(e => e.Updateuser)
                    .HasColumnName("updateuser")
                    .HasMaxLength(50);

                entity.Property(e => e.Vehicleseq).HasColumnName("vehicleseq");
            });

            modelBuilder.Entity<Question>(entity =>
            {
                entity.HasIndex(e => e.QuestionHeaderId)
                    .HasName("IX__QuestionHeader_Id");

                entity.Property(e => e.QuestionHeaderId).HasColumnName("_QuestionHeader_Id");

                entity.HasOne(d => d.QuestionHeader)
                    .WithMany(p => p.Question)
                    .HasForeignKey(d => d.QuestionHeaderId)
                    .HasConstraintName("FK_dbo.Question_dbo.QuestionHeader__QuestionHeader_Id");
            });

            modelBuilder.Entity<QuestionAnswer>(entity =>
            {
                entity.HasIndex(e => e.MainCompanyCode)
                    .HasName("IX__MainCompany_Code");

                entity.HasIndex(e => e.QuestionId)
                    .HasName("IX__Question_Id");

                entity.Property(e => e.MainCompanyCode)
                    .HasColumnName("_MainCompany_Code")
                    .HasMaxLength(7);

                entity.Property(e => e.QuestionId).HasColumnName("_Question_Id");

                entity.HasOne(d => d.MainCompanyCodeNavigation)
                    .WithMany(p => p.QuestionAnswer)
                    .HasForeignKey(d => d.MainCompanyCode)
                    .HasConstraintName("FK_dbo.QuestionAnswer_dbo.MainCompany__MainCompany_Code");

                entity.HasOne(d => d.Question)
                    .WithMany(p => p.QuestionAnswer)
                    .HasForeignKey(d => d.QuestionId)
                    .HasConstraintName("FK_dbo.QuestionAnswer_dbo.Question__Question_Id");
            });

            modelBuilder.Entity<Report>(entity =>
            {
                entity.Property(e => e.MainCompanyCode).IsRequired();

                entity.Property(e => e.Name).IsRequired();

                entity.Property(e => e.Url)
                    .IsRequired()
                    .HasColumnName("URL");
            });

            modelBuilder.Entity<Tender>(entity =>
            {
                entity.Property(e => e.CalculatedKm).HasColumnName("CalculatedKM");

                entity.Property(e => e.CarrierContact1).HasColumnName("CarrierContact_1");

                entity.Property(e => e.CarrierContact2).HasColumnName("CarrierContact_2");

                entity.Property(e => e.CarrierContact3).HasColumnName("CarrierContact_3");

                entity.Property(e => e.CarrierContact4).HasColumnName("CarrierContact_4");

                entity.Property(e => e.CarrierContact5).HasColumnName("CarrierContact_5");

                entity.Property(e => e.CarrierEmail1).HasColumnName("CarrierEmail_1");

                entity.Property(e => e.CarrierEmail2).HasColumnName("CarrierEmail_2");

                entity.Property(e => e.CarrierEmail3).HasColumnName("CarrierEmail_3");

                entity.Property(e => e.CarrierEmail4).HasColumnName("CarrierEmail_4");

                entity.Property(e => e.CarrierEmail5).HasColumnName("CarrierEmail_5");

                entity.Property(e => e.CarrierName1).HasColumnName("CarrierName_1");

                entity.Property(e => e.CarrierName2).HasColumnName("CarrierName_2");

                entity.Property(e => e.CarrierName3).HasColumnName("CarrierName_3");

                entity.Property(e => e.CarrierName4).HasColumnName("CarrierName_4");

                entity.Property(e => e.CarrierName5).HasColumnName("CarrierName_5");

                entity.Property(e => e.CarriereExternalSystemId1).HasColumnName("CarriereExternalSystemId_1");

                entity.Property(e => e.CarriereExternalSystemId2).HasColumnName("CarriereExternalSystemId_2");

                entity.Property(e => e.CarriereExternalSystemId3).HasColumnName("CarriereExternalSystemId_3");

                entity.Property(e => e.CarriereExternalSystemId4).HasColumnName("CarriereExternalSystemId_4");

                entity.Property(e => e.CarriereExternalSystemId5).HasColumnName("CarriereExternalSystemId_5");

                entity.Property(e => e.ConfirmedKm).HasColumnName("ConfirmedKM");

                entity.Property(e => e.RequestSentYn).HasColumnName("RequestSentYN");

                entity.Property(e => e.Seq).HasColumnName("SEQ");
            });

            modelBuilder.Entity<TenderAnswer>(entity =>
            {
                entity.HasIndex(e => e.TenderId)
                    .HasName("IX_tender_Id");

                entity.Property(e => e.DriverMobileNr).HasColumnName("DriverMobileNR");

                entity.Property(e => e.TenderId).HasColumnName("Tender_Id");

                entity.HasOne(d => d.Tender)
                    .WithMany(p => p.TenderAnswer)
                    .HasForeignKey(d => d.TenderId)
                    .HasConstraintName("FK_dbo.TenderAnswer_dbo.Tender_Tender_Id");
            });

            modelBuilder.Entity<TenderStop>(entity =>
            {
                entity.HasIndex(e => e.TenderId)
                    .HasName("IX_tender_Id");

                entity.Property(e => e.TenderId).HasColumnName("Tender_Id");

                entity.HasOne(d => d.Tender)
                    .WithMany(p => p.TenderStop)
                    .HasForeignKey(d => d.TenderId)
                    .HasConstraintName("FK_dbo.TenderStop_dbo.Tender_Tender_Id");
            });

            modelBuilder.Entity<Token>(entity =>
            {
                entity.Property(e => e.TokenId).HasColumnName("TokenID");
            });

            modelBuilder.Entity<UetdsIslemTuru>(entity =>
            {
                entity.ToTable("Uetds_Islem_Turu");

                entity.Property(e => e.IslemId).HasColumnName("Islem_Id");

                entity.Property(e => e.IslemTuru).HasColumnName("Islem_Turu");
            });

            modelBuilder.Entity<UetdsLog>(entity =>
            {
                entity.ToTable("Uetds_Log");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.IslemId).HasColumnName("Islem_Id");
            });

            modelBuilder.Entity<UetdsYukKaydi>(entity =>
            {
                entity.ToTable("Uetds_Yuk_Kaydi");
            });

            modelBuilder.Entity<UetdsYukKaydiDetayi>(entity =>
            {
                entity.ToTable("Uetds_Yuk_Kaydi_Detayi");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<UserRole>(entity =>
            {
                entity.HasIndex(e => e.RoleId)
                    .HasName("IX_RoleId");

                entity.HasIndex(e => e.UserId)
                    .HasName("IX_UserId");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.UserRole)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK_dbo.UserRole_dbo.Role_RoleId");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserRole)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_dbo.UserRole_dbo.User_UserId");
            });

            modelBuilder.Entity<VwArventoRegionAlarm>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_arventoRegionAlarm");

                entity.Property(e => e.EndDate).HasMaxLength(4000);

                entity.Property(e => e.Pin1)
                    .IsRequired()
                    .HasColumnName("PIN1")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Pin2)
                    .IsRequired()
                    .HasColumnName("PIN2")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.StartDate).HasMaxLength(4000);

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasColumnName("userName")
                    .HasMaxLength(16)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwDailyStock>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_dailyStock");

                entity.Property(e => e.ExpireDate).HasColumnName("expireDate");

                entity.Property(e => e.ItemCode).HasColumnName("itemCode");

                entity.Property(e => e.LotCode).HasColumnName("lotCode");

                entity.Property(e => e.MainCompanyCode)
                    .HasColumnName("mainCompanyCode")
                    .HasMaxLength(50);

                entity.Property(e => e.Quantity).HasColumnName("quantity");

                entity.Property(e => e.RowNumber).HasColumnName("rowNumber");

                entity.Property(e => e.StockDate)
                    .HasColumnName("stockDate")
                    .HasMaxLength(10);

                entity.Property(e => e.WarehouseCode).HasColumnName("warehouseCode");
            });

            modelBuilder.Entity<VwDailyStockAxata>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_dailyStock_axata");

                entity.Property(e => e.ExpireDate).HasColumnName("expireDate");

                entity.Property(e => e.ItemCode).HasColumnName("itemCode");

                entity.Property(e => e.LotCode).HasColumnName("lotCode");

                entity.Property(e => e.Quantity).HasColumnName("quantity");

                entity.Property(e => e.RowNumber).HasColumnName("rowNumber");

                entity.Property(e => e.StockDate)
                    .IsRequired()
                    .HasColumnName("stockDate");

                entity.Property(e => e.WarehouseCode)
                    .IsRequired()
                    .HasColumnName("warehouseCode");
            });

            modelBuilder.Entity<VwDogalTarimWaybill>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_dogalTarimWaybill");

                entity.Property(e => e.CompanyName)
                    .IsRequired()
                    .HasColumnName("companyName")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DeliveryDate)
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.DeliveryNumber).HasMaxLength(100);

                entity.Property(e => e.ItemCode)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.ItemName)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.OrderNumber)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.RegulatedDate).HasColumnType("datetime");

                entity.Property(e => e.UnitCode)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<VwDogalWaybillDelete>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_dogalWaybill_delete");

                entity.Property(e => e.CompanyName)
                    .IsRequired()
                    .HasColumnName("companyName")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerCode)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.DeliveryDate).HasColumnType("datetime");

                entity.Property(e => e.ItemCode)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.ItemName)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.OrderNumber).HasMaxLength(100);

                entity.Property(e => e.RegulatedDate).HasColumnType("datetime");

                entity.Property(e => e.UnitCode)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<VwGetirIntSendGoodsInAxata>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_getirIntSendGoodsIN_axata");

                entity.Property(e => e.ApiToken)
                    .IsRequired()
                    .HasColumnName("apiToken")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Count).HasColumnName("count");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DocumentId)
                    .HasColumnName("documentId")
                    .HasMaxLength(50);

                entity.Property(e => e.DocumentType).HasColumnName("documentType");

                entity.Property(e => e.DomainType)
                    .HasColumnName("domainType")
                    .HasMaxLength(100);

                entity.Property(e => e.ExpiryDate)
                    .HasColumnName("expiryDate")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Image)
                    .HasColumnName("image")
                    .HasMaxLength(136);

                entity.Property(e => e.IntegrationCode)
                    .HasColumnName("integrationCode")
                    .HasMaxLength(300);

                entity.Property(e => e.IntegrationData).HasColumnName("integrationData");

                entity.Property(e => e.IntegrationDate)
                    .HasColumnName("integrationDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.IntegrationId)
                    .HasColumnName("integrationId")
                    .HasMaxLength(300);

                entity.Property(e => e.IntegrationResponseCode)
                    .HasColumnName("integrationResponseCode")
                    .HasMaxLength(50);

                entity.Property(e => e.IntegrationResponseMessage)
                    .HasColumnName("integrationResponseMessage")
                    .HasMaxLength(300);

                entity.Property(e => e.IsDamaged)
                    .IsRequired()
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.ItemId).HasMaxLength(50);

                entity.Property(e => e.Reports).HasColumnName("reports");

                entity.Property(e => e.S16gtIntegrationStatus).HasColumnName("S16GT_IntegrationStatus");

                entity.Property(e => e.S16sipn)
                    .HasColumnName("S16SIPN")
                    .HasMaxLength(50);

                entity.Property(e => e.Ss)
                    .HasColumnName("ss")
                    .HasMaxLength(310);

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasMaxLength(300);
            });

            modelBuilder.Entity<VwGetirIntSendGoodsOutAxata>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_getirIntSendGoodsOUT_axata");

                entity.Property(e => e.ApiToken)
                    .IsRequired()
                    .HasColumnName("apiToken")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Barcode)
                    .HasColumnName("barcode")
                    .HasMaxLength(50);

                entity.Property(e => e.Count).HasColumnName("count");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasMaxLength(19)
                    .IsUnicode(false);

                entity.Property(e => e.DocumentId)
                    .HasColumnName("documentId")
                    .HasMaxLength(50);

                entity.Property(e => e.DocumentType).HasColumnName("documentType");

                entity.Property(e => e.DomainType).HasColumnName("domainType");

                entity.Property(e => e.ExpiryDate)
                    .HasColumnName("expiryDate")
                    .HasMaxLength(10);

                entity.Property(e => e.Image)
                    .HasColumnName("image")
                    .HasMaxLength(504)
                    .IsUnicode(false);

                entity.Property(e => e.IntegrationCode)
                    .HasColumnName("integrationCode")
                    .HasMaxLength(300);

                entity.Property(e => e.IntegrationData).HasColumnName("integrationData");

                entity.Property(e => e.IntegrationDate)
                    .HasColumnName("integrationDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.IntegrationId)
                    .HasColumnName("integrationId")
                    .HasMaxLength(300);

                entity.Property(e => e.IntegrationResponseCode)
                    .HasColumnName("integrationResponseCode")
                    .HasMaxLength(50);

                entity.Property(e => e.IntegrationResponseMessage)
                    .HasColumnName("integrationResponseMessage")
                    .HasMaxLength(200);

                entity.Property(e => e.IsDamaged)
                    .IsRequired()
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.ItemId).HasMaxLength(50);

                entity.Property(e => e.OrderCode)
                    .HasColumnName("orderCode")
                    .HasMaxLength(50);

                entity.Property(e => e.Plate)
                    .HasColumnName("plate")
                    .HasMaxLength(50);

                entity.Property(e => e.Ss)
                    .HasColumnName("ss")
                    .HasMaxLength(26);

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasMaxLength(300);
            });

            modelBuilder.Entity<VwGetirIntSendGoodsOutAxataNew>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_getirIntSendGoodsOUT_axata_New");

                entity.Property(e => e.ApiToken)
                    .IsRequired()
                    .HasColumnName("apiToken")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Barcode)
                    .HasColumnName("barcode")
                    .HasMaxLength(50);

                entity.Property(e => e.Count).HasColumnName("count");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.DocumentId).HasMaxLength(50);

                entity.Property(e => e.DocumentType).HasColumnName("documentType");

                entity.Property(e => e.DomainType)
                    .HasColumnName("domainType")
                    .HasMaxLength(500);

                entity.Property(e => e.ExpiryDate)
                    .HasColumnName("expiryDate")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Image)
                    .HasColumnName("image")
                    .HasMaxLength(504)
                    .IsUnicode(false);

                entity.Property(e => e.IntegrationCode)
                    .IsRequired()
                    .HasColumnName("integrationCode")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.IntegrationData)
                    .HasColumnName("integrationData")
                    .HasColumnType("text");

                entity.Property(e => e.IntegrationDate)
                    .HasColumnName("integrationDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.IntegrationId)
                    .IsRequired()
                    .HasColumnName("integrationId")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.IntegrationResponseCode)
                    .HasColumnName("integrationResponseCode")
                    .HasMaxLength(50);

                entity.Property(e => e.IntegrationResponseMessage)
                    .HasColumnName("integrationResponseMessage")
                    .HasMaxLength(200);

                entity.Property(e => e.IsDamaged)
                    .IsRequired()
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.ItemId).HasMaxLength(50);

                entity.Property(e => e.OrderCode)
                    .HasColumnName("orderCode")
                    .HasMaxLength(50);

                entity.Property(e => e.Plate)
                    .HasColumnName("plate")
                    .HasMaxLength(50);

                entity.Property(e => e.Ss)
                    .HasColumnName("ss")
                    .HasMaxLength(26);

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasMaxLength(3)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwGetirIntSendGoodsOutcloseAxata>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_getirIntSendGoodsOUTClose_axata");

                entity.Property(e => e.ApiToken)
                    .IsRequired()
                    .HasColumnName("apiToken")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.DocumentId)
                    .HasColumnName("documentId")
                    .HasMaxLength(50);

                entity.Property(e => e.Domaintype).HasColumnName("domaintype");

                entity.Property(e => e.IntegrationCode)
                    .HasColumnName("integrationCode")
                    .HasMaxLength(300);

                entity.Property(e => e.OrderCode)
                    .HasColumnName("orderCode")
                    .HasMaxLength(50);

                entity.Property(e => e.S75gtIntegrationStatus).HasColumnName("S75GT_IntegrationStatus");

                entity.Property(e => e.SendLine).HasColumnName("sendLine");

                entity.Property(e => e.TotalLine).HasColumnName("totalLine");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasMaxLength(300);
            });

            modelBuilder.Entity<VwGetirIntSendSpeedyGoodsOutAxata>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_getirIntSendSpeedyGoodsOUT_axata");

                entity.Property(e => e.ApiToken)
                    .IsRequired()
                    .HasColumnName("apiToken")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Barcode)
                    .HasColumnName("barcode")
                    .HasMaxLength(50);

                entity.Property(e => e.Count).HasColumnName("count");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasMaxLength(19)
                    .IsUnicode(false);

                entity.Property(e => e.DocumentId)
                    .HasColumnName("documentId")
                    .HasMaxLength(50);

                entity.Property(e => e.DocumentType).HasColumnName("documentType");

                entity.Property(e => e.DomainType).HasColumnName("domainType");

                entity.Property(e => e.ExpiryDate)
                    .HasColumnName("expiryDate")
                    .HasMaxLength(10);

                entity.Property(e => e.Image)
                    .HasColumnName("image")
                    .HasMaxLength(504)
                    .IsUnicode(false);

                entity.Property(e => e.IntegrationCode)
                    .HasColumnName("integrationCode")
                    .HasMaxLength(300);

                entity.Property(e => e.IntegrationData).HasColumnName("integrationData");

                entity.Property(e => e.IntegrationDate)
                    .HasColumnName("integrationDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.IntegrationId)
                    .HasColumnName("integrationId")
                    .HasMaxLength(300);

                entity.Property(e => e.IntegrationResponseCode)
                    .HasColumnName("integrationResponseCode")
                    .HasMaxLength(50);

                entity.Property(e => e.IntegrationResponseMessage)
                    .HasColumnName("integrationResponseMessage")
                    .HasMaxLength(200);

                entity.Property(e => e.IsDamaged)
                    .IsRequired()
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.ItemId).HasMaxLength(50);

                entity.Property(e => e.OrderCode)
                    .HasColumnName("orderCode")
                    .HasMaxLength(50);

                entity.Property(e => e.Plate)
                    .HasColumnName("plate")
                    .HasMaxLength(50);

                entity.Property(e => e.Ss)
                    .HasColumnName("ss")
                    .HasMaxLength(26);

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasMaxLength(300);
            });

            modelBuilder.Entity<VwGetirOrderStatusAxata>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_getirOrderStatus_axata");

                entity.Property(e => e.ApiToken)
                    .IsRequired()
                    .HasColumnName("apiToken")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.DocumentId).HasMaxLength(50);

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.IntegrationDate)
                    .HasColumnName("integrationDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.IntegrationResponseCode)
                    .HasColumnName("integrationResponseCode")
                    .HasMaxLength(50);

                entity.Property(e => e.IntegrationResponseMessage)
                    .HasColumnName("integrationResponseMessage")
                    .HasMaxLength(300);

                entity.Property(e => e.OrderType).HasColumnName("orderType");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnName("status")
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.StatusDate)
                    .HasColumnName("statusDate")
                    .HasMaxLength(19)
                    .IsUnicode(false);

                entity.Property(e => e.SubType).HasMaxLength(150);
            });

            modelBuilder.Entity<VwNotificationJsonAddItem>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_notification_JSON_AddItem");

                entity.Property(e => e.CarrierNo)
                    .HasColumnName("carrierNo")
                    .HasMaxLength(50);

                entity.Property(e => e.ExpirationDate)
                    .HasColumnName("expirationDate")
                    .HasMaxLength(8000)
                    .IsUnicode(false);

                entity.Property(e => e.GoodsOutCode)
                    .HasColumnName("GoodsOut_Code")
                    .HasMaxLength(50);

                entity.Property(e => e.GtinNumber)
                    .HasColumnName("gtinNumber")
                    .HasMaxLength(20);

                entity.Property(e => e.HeaderId).HasColumnName("headerId");

                entity.Property(e => e.ItemCode).HasMaxLength(50);

                entity.Property(e => e.LotCode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LotNumber)
                    .HasColumnName("lotNumber")
                    .HasMaxLength(20);

                entity.Property(e => e.MainCompanyBranch)
                    .IsRequired()
                    .HasColumnName("mainCompanyBranch")
                    .HasMaxLength(50);

                entity.Property(e => e.ParentCarrierNo)
                    .HasColumnName("parentCarrierNo")
                    .HasMaxLength(50);

                entity.Property(e => e.ProductNote)
                    .IsRequired()
                    .HasColumnName("productNote")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ProductionDate)
                    .HasColumnName("productionDate")
                    .HasMaxLength(8000)
                    .IsUnicode(false);

                entity.Property(e => e.QrCode).HasColumnName("qrCode");

                entity.Property(e => e.Sender)
                    .HasColumnName("sender")
                    .HasMaxLength(200);

                entity.Property(e => e.SerialNumber).HasColumnName("serialNumber");

                entity.Property(e => e.StatusCode)
                    .HasColumnName("Status_Code")
                    .HasMaxLength(128);
            });

            modelBuilder.Entity<VwNotificationJsonAddItemNursena>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_notification_JSON_AddItem_nursena");

                entity.Property(e => e.CarrierNo)
                    .HasColumnName("carrierNo")
                    .HasMaxLength(50);

                entity.Property(e => e.ExpirationDate)
                    .HasColumnName("expirationDate")
                    .HasMaxLength(8000)
                    .IsUnicode(false);

                entity.Property(e => e.GoodsOutCode)
                    .HasColumnName("GoodsOut_Code")
                    .HasMaxLength(50);

                entity.Property(e => e.GtinNumber)
                    .HasColumnName("gtinNumber")
                    .HasMaxLength(20);

                entity.Property(e => e.HeaderId).HasColumnName("headerId");

                entity.Property(e => e.ItemCode).HasMaxLength(50);

                entity.Property(e => e.LotCode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LotNumber)
                    .HasColumnName("lotNumber")
                    .HasMaxLength(20);

                entity.Property(e => e.MainCompanyBranch)
                    .IsRequired()
                    .HasColumnName("mainCompanyBranch")
                    .HasMaxLength(50);

                entity.Property(e => e.ParentCarrierNo)
                    .HasColumnName("parentCarrierNo")
                    .HasMaxLength(50);

                entity.Property(e => e.ProductNote)
                    .IsRequired()
                    .HasColumnName("productNote")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ProductionDate)
                    .HasColumnName("productionDate")
                    .HasMaxLength(8000)
                    .IsUnicode(false);

                entity.Property(e => e.QrCode).HasColumnName("qrCode");

                entity.Property(e => e.Sender)
                    .HasColumnName("sender")
                    .HasMaxLength(200);

                entity.Property(e => e.SerialNumber).HasColumnName("serialNumber");

                entity.Property(e => e.StatusCode)
                    .HasColumnName("Status_Code")
                    .HasMaxLength(128);
            });

            modelBuilder.Entity<VwNotificationJsonAddItemSinan>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_notification_JSON_AddItem_sinan");

                entity.Property(e => e.CarrierNo)
                    .HasColumnName("carrierNo")
                    .HasMaxLength(50);

                entity.Property(e => e.ExpirationDate)
                    .HasColumnName("expirationDate")
                    .HasMaxLength(8000)
                    .IsUnicode(false);

                entity.Property(e => e.GoodsOutCode)
                    .HasColumnName("GoodsOut_Code")
                    .HasMaxLength(50);

                entity.Property(e => e.GtinNumber)
                    .HasColumnName("gtinNumber")
                    .HasMaxLength(20);

                entity.Property(e => e.HeaderId).HasColumnName("headerId");

                entity.Property(e => e.ItemCode).HasMaxLength(50);

                entity.Property(e => e.LotCode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LotNumber)
                    .HasColumnName("lotNumber")
                    .HasMaxLength(20);

                entity.Property(e => e.MainCompanyBranch)
                    .IsRequired()
                    .HasColumnName("mainCompanyBranch")
                    .HasMaxLength(50);

                entity.Property(e => e.ParentCarrierNo)
                    .HasColumnName("parentCarrierNo")
                    .HasMaxLength(50);

                entity.Property(e => e.ProductNote)
                    .IsRequired()
                    .HasColumnName("productNote")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ProductionDate)
                    .HasColumnName("productionDate")
                    .HasMaxLength(8000)
                    .IsUnicode(false);

                entity.Property(e => e.QrCode).HasColumnName("qrCode");

                entity.Property(e => e.Sender)
                    .HasColumnName("sender")
                    .HasMaxLength(200);

                entity.Property(e => e.SerialNumber).HasColumnName("serialNumber");

                entity.Property(e => e.StatusCode)
                    .HasColumnName("Status_Code")
                    .HasMaxLength(128);
            });

            modelBuilder.Entity<VwNotificationJsonForBox>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_notification_JSON_For_Box");

                entity.Property(e => e.ActionType)
                    .HasColumnName("actionType")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.CarrierNo)
                    .HasColumnName("carrierNo")
                    .HasMaxLength(50);

                entity.Property(e => e.DocumentDate)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ExpirationDate)
                    .HasColumnName("expirationDate")
                    .HasMaxLength(8000)
                    .IsUnicode(false);

                entity.Property(e => e.ExportCountryCode)
                    .HasColumnName("ExportCountry_Code")
                    .HasMaxLength(50);

                entity.Property(e => e.GoodsOutCode)
                    .HasColumnName("GoodsOut_Code")
                    .HasMaxLength(50);

                entity.Property(e => e.GtinNumber)
                    .HasColumnName("gtinNumber")
                    .HasMaxLength(20);

                entity.Property(e => e.IdTaxNo)
                    .IsRequired()
                    .HasColumnName("idTaxNo")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ImportCountryCode).HasColumnName("ImportCountry_Code");

                entity.Property(e => e.ItemCode).HasMaxLength(50);

                entity.Property(e => e.LotCode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LotNumber)
                    .HasColumnName("lotNumber")
                    .HasMaxLength(20);

                entity.Property(e => e.MainCompanyBranch)
                    .IsRequired()
                    .HasColumnName("mainCompanyBranch")
                    .HasMaxLength(50);

                entity.Property(e => e.Note).HasColumnName("note");

                entity.Property(e => e.ParentCarrierNo)
                    .HasColumnName("parentCarrierNo")
                    .HasMaxLength(50);

                entity.Property(e => e.ProductNote)
                    .IsRequired()
                    .HasColumnName("productNote")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ProductionDate)
                    .HasColumnName("productionDate")
                    .HasMaxLength(8000)
                    .IsUnicode(false);

                entity.Property(e => e.QrCode).HasColumnName("qrCode");

                entity.Property(e => e.Receiver).HasColumnName("receiver");

                entity.Property(e => e.ReturnNote)
                    .IsRequired()
                    .HasColumnName("returnNote")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Sender).HasColumnName("sender");

                entity.Property(e => e.SerialNumber).HasColumnName("serialNumber");

                entity.Property(e => e.StatusCode)
                    .HasColumnName("Status_Code")
                    .HasMaxLength(128);
            });

            modelBuilder.Entity<VwNotificationJsonForPallet>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_notification_JSON_For_Pallet");

                entity.Property(e => e.ActionType)
                    .HasColumnName("actionType")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.CarrierNo)
                    .HasColumnName("carrierNo")
                    .HasMaxLength(50);

                entity.Property(e => e.DocumentDate)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ExpirationDate)
                    .HasColumnName("expirationDate")
                    .HasMaxLength(8000)
                    .IsUnicode(false);

                entity.Property(e => e.ExportCountryCode)
                    .HasColumnName("ExportCountry_Code")
                    .HasMaxLength(50);

                entity.Property(e => e.GoodsOutCode)
                    .HasColumnName("GoodsOut_Code")
                    .HasMaxLength(50);

                entity.Property(e => e.GtinNumber)
                    .HasColumnName("gtinNumber")
                    .HasMaxLength(20);

                entity.Property(e => e.IdTaxNo)
                    .IsRequired()
                    .HasColumnName("idTaxNo")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ImportCountryCode).HasColumnName("ImportCountry_Code");

                entity.Property(e => e.ItemCode).HasMaxLength(50);

                entity.Property(e => e.LotCode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LotNumber)
                    .HasColumnName("lotNumber")
                    .HasMaxLength(20);

                entity.Property(e => e.MainCompanyBranch)
                    .IsRequired()
                    .HasColumnName("mainCompanyBranch")
                    .HasMaxLength(50);

                entity.Property(e => e.Note).HasColumnName("note");

                entity.Property(e => e.ParentCarrierNo)
                    .IsRequired()
                    .HasColumnName("parentCarrierNo")
                    .HasMaxLength(50);

                entity.Property(e => e.ProductNote)
                    .IsRequired()
                    .HasColumnName("productNote")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ProductionDate)
                    .HasColumnName("productionDate")
                    .HasMaxLength(8000)
                    .IsUnicode(false);

                entity.Property(e => e.QrCode).HasColumnName("qrCode");

                entity.Property(e => e.Receiver).HasColumnName("receiver");

                entity.Property(e => e.ReturnNote)
                    .IsRequired()
                    .HasColumnName("returnNote")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Sender).HasColumnName("sender");

                entity.Property(e => e.SerialNumber).HasColumnName("serialNumber");

                entity.Property(e => e.StatusCode)
                    .HasColumnName("Status_Code")
                    .HasMaxLength(128);
            });

            modelBuilder.Entity<VwNotificationSalesJson20200304>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_notification_sales_JSON20200304");

                entity.Property(e => e.ActionType)
                    .HasColumnName("actionType")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.CarrierNo)
                    .HasColumnName("carrierNo")
                    .HasMaxLength(50);

                entity.Property(e => e.DocumentDate)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ExpirationDate)
                    .HasColumnName("expirationDate")
                    .HasMaxLength(8000)
                    .IsUnicode(false);

                entity.Property(e => e.ExportCountryCode)
                    .HasColumnName("ExportCountry_Code")
                    .HasMaxLength(50);

                entity.Property(e => e.GoodsOutCode)
                    .HasColumnName("GoodsOut_Code")
                    .HasMaxLength(50);

                entity.Property(e => e.GtinNumber)
                    .HasColumnName("gtinNumber")
                    .HasMaxLength(20);

                entity.Property(e => e.IdTaxNo)
                    .IsRequired()
                    .HasColumnName("idTaxNo")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ImportCountryCode).HasColumnName("ImportCountry_Code");

                entity.Property(e => e.ItemCode).HasMaxLength(50);

                entity.Property(e => e.LotCode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LotNumber)
                    .HasColumnName("lotNumber")
                    .HasMaxLength(20);

                entity.Property(e => e.MainCompanyBranch)
                    .IsRequired()
                    .HasColumnName("mainCompanyBranch")
                    .HasMaxLength(50);

                entity.Property(e => e.Note).HasColumnName("note");

                entity.Property(e => e.ParentCarrierNo)
                    .HasColumnName("parentCarrierNo")
                    .HasMaxLength(50);

                entity.Property(e => e.ProductNote)
                    .IsRequired()
                    .HasColumnName("productNote")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ProductionDate)
                    .HasColumnName("productionDate")
                    .HasMaxLength(8000)
                    .IsUnicode(false);

                entity.Property(e => e.QrCode).HasColumnName("qrCode");

                entity.Property(e => e.Receiver).HasColumnName("receiver");

                entity.Property(e => e.ReturnNote)
                    .IsRequired()
                    .HasColumnName("returnNote")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Sender).HasColumnName("sender");

                entity.Property(e => e.SerialNumber).HasColumnName("serialNumber");

                entity.Property(e => e.StatusCode)
                    .HasColumnName("Status_Code")
                    .HasMaxLength(128);
            });

            modelBuilder.Entity<VwNotificationSalesJsonEx>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_notification_sales_JSON_ex");

                entity.Property(e => e.ActionType)
                    .HasColumnName("actionType")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.CarrierNo)
                    .HasColumnName("carrierNo")
                    .HasMaxLength(50);

                entity.Property(e => e.DocumentDate)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ExpirationDate)
                    .HasColumnName("expirationDate")
                    .HasMaxLength(8000)
                    .IsUnicode(false);

                entity.Property(e => e.ExportCountryCode)
                    .HasColumnName("ExportCountry_Code")
                    .HasMaxLength(50);

                entity.Property(e => e.GtinNumber)
                    .HasColumnName("gtinNumber")
                    .HasMaxLength(20);

                entity.Property(e => e.IdTaxNo)
                    .IsRequired()
                    .HasColumnName("idTaxNo")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ImportCountryCode)
                    .HasColumnName("ImportCountry_Code")
                    .HasMaxLength(50);

                entity.Property(e => e.ItemCode).HasMaxLength(50);

                entity.Property(e => e.LotCode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LotNumber)
                    .HasColumnName("lotNumber")
                    .HasMaxLength(20);

                entity.Property(e => e.MainCompanyBranch)
                    .IsRequired()
                    .HasColumnName("mainCompanyBranch")
                    .HasMaxLength(50);

                entity.Property(e => e.Note).HasColumnName("note");

                entity.Property(e => e.ParentCarrierNo)
                    .HasColumnName("parentCarrierNo")
                    .HasMaxLength(50);

                entity.Property(e => e.ProductNote)
                    .IsRequired()
                    .HasColumnName("productNote")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ProductionDate)
                    .HasColumnName("productionDate")
                    .HasMaxLength(8000)
                    .IsUnicode(false);

                entity.Property(e => e.QrCode).HasColumnName("qrCode");

                entity.Property(e => e.Receiver).HasColumnName("receiver");

                entity.Property(e => e.ReturnNote)
                    .IsRequired()
                    .HasColumnName("returnNote")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Sender).HasColumnName("sender");

                entity.Property(e => e.SerialNumber).HasColumnName("serialNumber");
            });

            modelBuilder.Entity<VwNotificationSalesJsonGoodsOut>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_notification_sales_JSON_GoodsOut");

                entity.Property(e => e.ActionType)
                    .IsRequired()
                    .HasColumnName("actionType")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.AdetStatus).HasMaxLength(128);

                entity.Property(e => e.CarrierNo)
                    .HasColumnName("carrierNo")
                    .HasMaxLength(50);

                entity.Property(e => e.DestructionNote)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.DocumentDate)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ExpirationDate)
                    .HasColumnName("expirationDate")
                    .HasMaxLength(8000)
                    .IsUnicode(false);

                entity.Property(e => e.ExportCountryCode).HasColumnName("ExportCountry_Code");

                entity.Property(e => e.ExportReceiverNote)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.GoodsOutCode)
                    .HasColumnName("GoodsOut_Code")
                    .HasMaxLength(50);

                entity.Property(e => e.GtinNumber)
                    .HasColumnName("gtinNumber")
                    .HasMaxLength(20);

                entity.Property(e => e.IdTaxNo)
                    .IsRequired()
                    .HasColumnName("idTaxNo")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ImportCountryCode).HasColumnName("ImportCountry_Code");

                entity.Property(e => e.ImportSenderNote)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.LotNumber)
                    .HasColumnName("lotNumber")
                    .HasMaxLength(20);

                entity.Property(e => e.MainCompanyBranch)
                    .IsRequired()
                    .HasColumnName("mainCompanyBranch")
                    .HasMaxLength(50);

                entity.Property(e => e.Note)
                    .IsRequired()
                    .HasColumnName("note");

                entity.Property(e => e.ParentCarrierNo)
                    .HasColumnName("parentCarrierNo")
                    .HasMaxLength(50);

                entity.Property(e => e.ProductNote)
                    .IsRequired()
                    .HasColumnName("productNote")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ProductionDate)
                    .HasColumnName("productionDate")
                    .HasMaxLength(8000)
                    .IsUnicode(false);

                entity.Property(e => e.Receiver).HasColumnName("receiver");

                entity.Property(e => e.ReturnNote)
                    .IsRequired()
                    .HasColumnName("returnNote")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Sender)
                    .HasColumnName("sender")
                    .HasMaxLength(200);

                entity.Property(e => e.SerialNumber).HasColumnName("serialNumber");

                entity.Property(e => e.StatusCode)
                    .HasColumnName("Status_Code")
                    .HasMaxLength(128);
            });

            modelBuilder.Entity<VwNotificationSalesJsonGoodsOutAddItem>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_notification_sales_JSON_GoodsOut_AddItem");

                entity.Property(e => e.CarrierNo)
                    .HasColumnName("carrierNo")
                    .HasMaxLength(50);

                entity.Property(e => e.ExpirationDate)
                    .HasColumnName("expirationDate")
                    .HasMaxLength(8000)
                    .IsUnicode(false);

                entity.Property(e => e.GoodsOutCode)
                    .HasColumnName("GoodsOut_Code")
                    .HasMaxLength(50);

                entity.Property(e => e.GtinNumber)
                    .HasColumnName("gtinNumber")
                    .HasMaxLength(20);

                entity.Property(e => e.HeaderId).HasColumnName("headerId");

                entity.Property(e => e.ItemCode).HasMaxLength(50);

                entity.Property(e => e.LotCode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LotNumber)
                    .HasColumnName("lotNumber")
                    .HasMaxLength(20);

                entity.Property(e => e.MainCompanyBranch)
                    .IsRequired()
                    .HasColumnName("mainCompanyBranch")
                    .HasMaxLength(50);

                entity.Property(e => e.ParentCarrierNo)
                    .HasColumnName("parentCarrierNo")
                    .HasMaxLength(50);

                entity.Property(e => e.ProductNote)
                    .IsRequired()
                    .HasColumnName("productNote")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ProductionDate)
                    .HasColumnName("productionDate")
                    .HasMaxLength(8000)
                    .IsUnicode(false);

                entity.Property(e => e.QrCode).HasColumnName("qrCode");

                entity.Property(e => e.Sender)
                    .HasColumnName("sender")
                    .HasMaxLength(200);

                entity.Property(e => e.SerialNumber).HasColumnName("serialNumber");

                entity.Property(e => e.StatusCode)
                    .HasColumnName("Status_Code")
                    .HasMaxLength(128);
            });

            modelBuilder.Entity<VwNotificationSalesJsonGoodsOutExample>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_notification_sales_JSON_GoodsOut_Example");

                entity.Property(e => e.ActionType)
                    .IsRequired()
                    .HasColumnName("actionType")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.AdetStatus).HasMaxLength(128);

                entity.Property(e => e.CarrierNo)
                    .HasColumnName("carrierNo")
                    .HasMaxLength(50);

                entity.Property(e => e.DeactivationNote)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.DestructionNote)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.DocumentDate)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ExpirationDate)
                    .HasColumnName("expirationDate")
                    .HasMaxLength(8000)
                    .IsUnicode(false);

                entity.Property(e => e.ExportCountryCode)
                    .IsRequired()
                    .HasColumnName("ExportCountry_Code")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ExportReceiverNote)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.GoodsOutCode)
                    .HasColumnName("GoodsOut_Code")
                    .HasMaxLength(50);

                entity.Property(e => e.GtinNumber)
                    .HasColumnName("gtinNumber")
                    .HasMaxLength(20);

                entity.Property(e => e.IdTaxNo)
                    .IsRequired()
                    .HasColumnName("idTaxNo")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ImportCountryCode).HasColumnName("ImportCountry_Code");

                entity.Property(e => e.ImportSenderNote)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.LotNumber)
                    .HasColumnName("lotNumber")
                    .HasMaxLength(20);

                entity.Property(e => e.MainCompanyBranch)
                    .IsRequired()
                    .HasColumnName("mainCompanyBranch")
                    .HasMaxLength(50);

                entity.Property(e => e.Note)
                    .IsRequired()
                    .HasColumnName("note");

                entity.Property(e => e.ParentCarrierNo)
                    .HasColumnName("parentCarrierNo")
                    .HasMaxLength(50);

                entity.Property(e => e.ProductNote)
                    .IsRequired()
                    .HasColumnName("productNote")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ProductionDate)
                    .HasColumnName("productionDate")
                    .HasMaxLength(8000)
                    .IsUnicode(false);

                entity.Property(e => e.Receiver).HasColumnName("receiver");

                entity.Property(e => e.ReturnNote)
                    .IsRequired()
                    .HasColumnName("returnNote")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Sender)
                    .HasColumnName("sender")
                    .HasMaxLength(200);

                entity.Property(e => e.SerialNumber).HasColumnName("serialNumber");

                entity.Property(e => e.StatusCode)
                    .HasColumnName("Status_Code")
                    .HasMaxLength(128);
            });

            modelBuilder.Entity<VwOrderDetailsForAdr>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_OrderDetailsForADR");

                entity.Property(e => e.AdritemDescription).HasColumnName("ADRItemDescription");

                entity.Property(e => e.AdrmainRiskClass).HasColumnName("ADRMainRiskClass");

                entity.Property(e => e.AdrpackageGroup).HasColumnName("ADRPackageGroup");

                entity.Property(e => e.AdrtunnelRestrictionCode).HasColumnName("ADRTunnelRestrictionCode");

                entity.Property(e => e.AdrunNumber).HasColumnName("ADRUnNumber");

                entity.Property(e => e.ItemCode)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ItemDescription).HasMaxLength(200);

                entity.Property(e => e.OrderCode)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.PackageQuantity).HasColumnType("decimal(21, 2)");

                entity.Property(e => e.Quantity).HasColumnType("decimal(21, 2)");

                entity.Property(e => e.ShipFromAddress).HasMaxLength(100);

                entity.Property(e => e.ShipFromDescription).HasMaxLength(300);

                entity.Property(e => e.ShipFromDist)
                    .IsRequired()
                    .HasMaxLength(18)
                    .IsUnicode(false);

                entity.Property(e => e.ShipFromDocNo)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ShipFromTaxDepNo).HasMaxLength(300);

                entity.Property(e => e.ShipToAddress).HasMaxLength(4000);

                entity.Property(e => e.ShipToCity).HasMaxLength(200);

                entity.Property(e => e.ShipToCode).HasMaxLength(100);

                entity.Property(e => e.ShipToDescription).HasMaxLength(400);

                entity.Property(e => e.ShipToTown).HasMaxLength(200);

                entity.Property(e => e.ShipmentCode).HasMaxLength(100);

                entity.Property(e => e.ShipmentDate)
                    .HasColumnName("Shipment_Date")
                    .HasColumnType("date");

                entity.Property(e => e.SoldToAddress).HasMaxLength(4000);

                entity.Property(e => e.SoldToCity).HasMaxLength(50);

                entity.Property(e => e.SoldToCode)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.SoldToDescription).HasMaxLength(400);

                entity.Property(e => e.SoldToTown).HasMaxLength(50);

                entity.Property(e => e.TaxCode).HasMaxLength(400);

                entity.Property(e => e.TaxOffice).HasMaxLength(50);

                entity.Property(e => e.Volume).HasColumnType("numeric(38, 3)");

                entity.Property(e => e.Weight).HasColumnType("numeric(38, 3)");
            });

            modelBuilder.Entity<VwOrderGoodsInHemel>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_OrderGoodsIn_Hemel");

                entity.Property(e => e.Company)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.DocumentCode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.DocumentDate)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DocumentTypeDesc)
                    .IsRequired()
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.ExpiryDate)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ItemCode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ItemName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.LotNumber)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.MailBody)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.MailSubject)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.MainCompanyCode)
                    .IsRequired()
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.OrderCode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.OrderDate)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.OrderStatus)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.PalletBarcode)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ReceiptDate)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ReceiverMailAddresses)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ShipmentCode)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Supplier)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.VehiclePlate).HasMaxLength(50);
            });

            modelBuilder.Entity<VwOrderGoodsOutFmc>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_OrderGoodsOut_FMC");

                entity.Property(e => e.Company)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.DocumentCode)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.DocumentDate)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.DocumentTypeDesc)
                    .IsRequired()
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.ExpiryDate)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ItemCode)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.ItemName)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.LotNumber)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.MailBody)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.MailSubject)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.MainCompanyCode)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.OrderCode)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.OrderDate)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.OrderStatus)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.PalletBarcode)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ReceiptDate)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ReceiverMailAddresses)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ShipmentCode)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Supplier)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.VehiclePlate)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwOrderGoodsOutHemel>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_OrderGoodsOut_Hemel");

                entity.Property(e => e.Company)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.DocumentCode).HasMaxLength(61);

                entity.Property(e => e.DocumentDate)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.DocumentTypeDesc)
                    .IsRequired()
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.ExpiryDate)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ItemCode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ItemName).HasMaxLength(100);

                entity.Property(e => e.LotNumber)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.MailBody)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.MailSubject)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.MainCompanyCode)
                    .IsRequired()
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.OrderCode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.OrderDate)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.OrderStatus)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.PalletBarcode)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ReceiptDate)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ReceiverMailAddresses)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ShipmentCode)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Supplier)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.VehiclePlate)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwOrderStatusAllCustomer>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_OrderStatus_AllCustomer");

                entity.Property(e => e.OrderCode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.StatusDate)
                    .HasMaxLength(19)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwOrderStatusGetir>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_OrderStatus_Getir");

                entity.Property(e => e.OrderCode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.StatusDate)
                    .HasMaxLength(19)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwOrderStatusHemel>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_OrderStatus_Hemel");

                entity.Property(e => e.OrderCode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.StatusDate)
                    .HasMaxLength(19)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwOrderWaybillAllCustomer>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_OrderWaybill_AllCustomer");

                entity.Property(e => e.DocumentId).HasMaxLength(50);

                entity.Property(e => e.Domaintype)
                    .HasColumnName("domaintype")
                    .HasMaxLength(500);

                entity.Property(e => e.Hkod)
                    .HasColumnName("HKod")
                    .HasMaxLength(3);

                entity.Property(e => e.IntegrationCode)
                    .HasColumnName("integrationCode")
                    .HasMaxLength(50);

                entity.Property(e => e.IntegrationDate)
                    .HasColumnName("integrationDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.IntegrationStatus).HasColumnName("integrationStatus");

                entity.Property(e => e.IntegrationStatusDesc)
                    .HasColumnName("integrationStatusDesc")
                    .HasMaxLength(300);

                entity.Property(e => e.ItemId).HasMaxLength(50);

                entity.Property(e => e.OrderCode)
                    .HasColumnName("orderCode")
                    .HasMaxLength(50);

                entity.Property(e => e.OrderStatus).HasColumnType("numeric(3, 0)");

                entity.Property(e => e.SirketKodu)
                    .HasColumnName("sirket_kodu")
                    .HasMaxLength(4);

                entity.Property(e => e.WarehouseCode).HasMaxLength(8);

                entity.Property(e => e.WaybillDate)
                    .HasMaxLength(19)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwOrderWaybillGetir>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_OrderWaybill_Getir");

                entity.Property(e => e.DocumentId).HasMaxLength(50);

                entity.Property(e => e.Domaintype).HasColumnName("domaintype");

                entity.Property(e => e.Hkod)
                    .HasColumnName("HKod")
                    .HasMaxLength(3);

                entity.Property(e => e.IntegrationCode)
                    .HasColumnName("integrationCode")
                    .HasMaxLength(50);

                entity.Property(e => e.IntegrationDate)
                    .HasColumnName("integrationDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.IntegrationStatus).HasColumnName("integrationStatus");

                entity.Property(e => e.IntegrationStatusDesc)
                    .HasColumnName("integrationStatusDesc")
                    .HasMaxLength(300);

                entity.Property(e => e.ItemId).HasMaxLength(50);

                entity.Property(e => e.OrderCode)
                    .HasColumnName("orderCode")
                    .HasMaxLength(50);

                entity.Property(e => e.OrderStatus).HasColumnType("numeric(3, 0)");

                entity.Property(e => e.SirketKodu)
                    .HasColumnName("sirket_kodu")
                    .HasMaxLength(4);

                entity.Property(e => e.WarehouseCode).HasMaxLength(4);

                entity.Property(e => e.WaybillDate)
                    .HasMaxLength(19)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwPalletCountPowerBi>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_PalletCount_PowerBi");

                entity.Property(e => e.DsGroupCode)
                    .HasColumnName("DS_GroupCode")
                    .HasMaxLength(200);

                entity.Property(e => e.Invoicereportscode)
                    .HasColumnName("INVOICEREPORTSCODE")
                    .HasMaxLength(100);

                entity.Property(e => e.PalletCount)
                    .HasColumnName("palletCount")
                    .HasColumnType("numeric(38, 0)");

                entity.Property(e => e.StockDate)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwPolisanCompareStock>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_polisan_compare_stock");

                entity.Property(e => e.CustomerStockQuantity).HasColumnName("customerStockQuantity");

                entity.Property(e => e.ItemCode).HasColumnName("itemCode");

                entity.Property(e => e.ItemName).HasColumnName("itemName");

                entity.Property(e => e.LotCode).HasColumnName("lotCode");

                entity.Property(e => e.StockDate)
                    .IsRequired()
                    .HasColumnName("stockDate")
                    .HasMaxLength(10);

                entity.Property(e => e.StockDiff).HasColumnName("stockDiff");

                entity.Property(e => e.StockQuantity).HasColumnName("stockQuantity");

                entity.Property(e => e.WarehouseCode)
                    .IsRequired()
                    .HasColumnName("warehouseCode");

                entity.Property(e => e.WarehouseName)
                    .HasColumnName("warehouseName")
                    .HasMaxLength(15)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwShippedOrderLines>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_shippedOrderLines");

                entity.Property(e => e.CompanyBranchCode)
                    .IsRequired()
                    .HasColumnName("companyBranchCode")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.DispatchImageName)
                    .IsRequired()
                    .HasColumnName("dispatchImageName")
                    .HasMaxLength(13)
                    .IsUnicode(false);

                entity.Property(e => e.DispatchNumber)
                    .IsRequired()
                    .HasColumnName("dispatchNumber")
                    .HasMaxLength(7)
                    .IsUnicode(false);

                entity.Property(e => e.DispatchType)
                    .IsRequired()
                    .HasColumnName("dispatchType")
                    .HasMaxLength(14)
                    .IsUnicode(false);

                entity.Property(e => e.DriverName)
                    .IsRequired()
                    .HasColumnName("driverName")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DriverNationalityId)
                    .IsRequired()
                    .HasColumnName("driverNationalityID")
                    .HasMaxLength(14)
                    .IsUnicode(false);

                entity.Property(e => e.ExpiryDate)
                    .IsRequired()
                    .HasColumnName("expiryDate")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.GrossWeight)
                    .HasColumnName("grossWeight")
                    .HasColumnType("numeric(4, 2)");

                entity.Property(e => e.IntegrationData).HasColumnName("integrationData");

                entity.Property(e => e.IntegrationDate).HasColumnName("integrationDate");

                entity.Property(e => e.IntegrationResponseCode)
                    .IsRequired()
                    .HasColumnName("integrationResponseCode")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IntegrationResponseMessage)
                    .IsRequired()
                    .HasColumnName("integrationResponseMessage")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IssueDate)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.IssueTime)
                    .IsRequired()
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.ItemCode)
                    .IsRequired()
                    .HasColumnName("itemCode")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.ItemName)
                    .IsRequired()
                    .HasColumnName("itemName")
                    .HasMaxLength(14)
                    .IsUnicode(false);

                entity.Property(e => e.LineCode)
                    .IsRequired()
                    .HasColumnName("lineCode")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.LotCode)
                    .IsRequired()
                    .HasColumnName("lotCode")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.MainCompanyCode)
                    .IsRequired()
                    .HasColumnName("mainCompanyCode")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.OrderDate)
                    .IsRequired()
                    .HasColumnName("orderDate")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.OrderNumber)
                    .IsRequired()
                    .HasColumnName("orderNumber")
                    .HasMaxLength(14)
                    .IsUnicode(false);

                entity.Property(e => e.OrderRefNumber)
                    .IsRequired()
                    .HasColumnName("orderRefNumber")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.OrderType).HasColumnName("orderType");

                entity.Property(e => e.PackageType)
                    .IsRequired()
                    .HasColumnName("packageType")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.PalletBarcode)
                    .IsRequired()
                    .HasColumnName("palletBarcode")
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.Plate)
                    .IsRequired()
                    .HasColumnName("plate")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.Quantity)
                    .HasColumnName("quantity")
                    .HasColumnType("numeric(3, 1)");

                entity.Property(e => e.ShipmentCode)
                    .IsRequired()
                    .HasColumnName("shipmentCode")
                    .HasMaxLength(9)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Warehouses>(entity =>
            {
                entity.Property(e => e.StorageSpace).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.StorageVolume).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.StoreVolume).HasColumnType("decimal(18, 2)");
            });

            modelBuilder.Entity<WhOnTimeGoodsInPowerBi>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("wh_onTimeGoodsIn_PowerBi");

                entity.Property(e => e.ActType)
                    .HasColumnName("actType")
                    .HasMaxLength(3);

                entity.Property(e => e.ActTypeOrderNo)
                    .HasColumnName("actTypeOrderNo")
                    .HasMaxLength(54);

                entity.Property(e => e.Company)
                    .HasColumnName("company")
                    .HasMaxLength(2);

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.EnteredPaletteCount).HasColumnName("enteredPaletteCount");

                entity.Property(e => e.GoodsInWeight)
                    .HasColumnName("goodsInWeight")
                    .HasColumnType("decimal(15, 2)");

                entity.Property(e => e.Miktar).HasColumnName("miktar");

                entity.Property(e => e.OderNo)
                    .HasColumnName("oderNo")
                    .HasMaxLength(50);

                entity.Property(e => e.ReferenceTime).HasColumnName("referenceTime");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.Warehouse)
                    .HasColumnName("warehouse")
                    .HasMaxLength(2);

                entity.Property(e => e.WarehouseDescription)
                    .IsRequired()
                    .HasColumnName("warehouseDescription")
                    .HasMaxLength(27)
                    .IsUnicode(false);

                entity.Property(e => e.Whdescription)
                    .IsRequired()
                    .HasColumnName("WHdescription")
                    .HasMaxLength(9)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<WvGoodsInOutLineCountPowerBi>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("wv_GoodsInOutLineCount_PowerBi");

                entity.Property(e => e.DepoDescription)
                    .HasColumnName("DEPO_DESCRIPTION")
                    .HasMaxLength(200);

                entity.Property(e => e.DepoInvoicereportscode)
                    .HasColumnName("DEPO_INVOICEREPORTSCODE")
                    .HasMaxLength(100);

                entity.Property(e => e.LineCount).HasColumnName("lineCount");

                entity.Property(e => e.ReceDepositorid)
                    .HasColumnName("RECE_DEPOSITORID")
                    .HasMaxLength(50);

                entity.Property(e => e.ReceReceiptdatetime)
                    .HasColumnName("RECE_RECEIPTDATETIME")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasMaxLength(5)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}

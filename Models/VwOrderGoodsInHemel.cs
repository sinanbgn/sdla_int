﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class VwOrderGoodsInHemel
    {
        public long? LineCode { get; set; }
        public string OrderCode { get; set; }
        public int OrderType { get; set; }
        public int OrderSubType { get; set; }
        public string Company { get; set; }
        public string MainCompanyCode { get; set; }
        public string ShipmentCode { get; set; }
        public string ReceiptDate { get; set; }
        public string OrderDate { get; set; }
        public string Supplier { get; set; }
        public string OrderStatus { get; set; }
        public string MailSubject { get; set; }
        public string MailBody { get; set; }
        public string ReceiverMailAddresses { get; set; }
        public int DocumentType { get; set; }
        public string DocumentTypeDesc { get; set; }
        public string DocumentCode { get; set; }
        public string DocumentDate { get; set; }
        public string VehiclePlate { get; set; }
        public string PalletBarcode { get; set; }
        public int PackageType { get; set; }
        public string ExpiryDate { get; set; }
        public string LotNumber { get; set; }
        public double? QuantityTotal { get; set; }
        public double? Quantity { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public double? WeightNetItemUnit { get; set; }
        public double? WeightGrossItemUnit { get; set; }
        public double? QuantityItemInStandart { get; set; }
    }
}

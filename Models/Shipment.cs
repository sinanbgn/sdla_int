﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SdLa_Int.Models
{
    public class Shipment
    {
        public int seq { get; set; }
        public string code { get; set; }
        public string externalsystemid { get; set; }
        public string createdatetime { get; set; }
        public string updatedatetime { get; set; }
        public string updateuser { get; set; }
        public string dataorigin { get; set; }
        public DateTime requireddeliverydate { get; set; }
        public string pickupaddress { get; set; }
        public string deliveryaddress { get; set; }
        public DateTime requiredpickupdate { get; set; }
        public string receiver { get; set; }
        public string debtor { get; set; }
        public string shipmenttype { get; set; }
        public string yourreference { get; set; }
        public string ourreference { get; set; }
        public string movementtype { get; set; }
        public string cargotype { get; set; }
        public string statuscode { get; set; }
        public string activeyn { get; set; }
        public string deliverylatitude { get; set; }
        public string deliverylongitude { get; set; }
        public string pickuplatitude { get; set; }
        public string pickuplongitude { get; set; }
        public string pickupaddressfree { get; set; }
        public string deliveryaddressfree { get; set; }
        public DateTime confirmationdate { get; set; }
        public DateTime ptddate { get; set; }
        public string ptdtime { get; set; }
        public DateTime ptadate { get; set; }
        public string ptatime { get; set; }
        public DateTime atddate { get; set; }
        public string atdtime { get; set; }
        public DateTime atadate { get; set; }
        public string atatime { get; set; }
        public string notifyonpickup { get; set; }
        public string notifyondelivery { get; set; }
        public string dynamicstatus { get; set; }
        public string archivedyn { get; set; }
        public string deliveryphoto1 { get; set; }
        public string deliveryfilename1 { get; set; }
        public string deliverymimetype1 { get; set; }
        public string deliverysignatureby { get; set; }
        public IList<Good> goods { get; set; }
        public int driverseq { get; set; }
        public string driverexternalsystemid { get; set; }
        public string drivername { get; set; }
        public string drivermobilenumber { get; set; }
        public string lastlocationlatitude { get; set; }
        public string lastlocationlongitude { get; set; }
        public IList<object> surveyanswers { get; set; }
        public IList<Statuschanx> statuschanges { get; set; }
    }
}

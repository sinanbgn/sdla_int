﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class Order
    {
        public Order()
        {
            Company = new HashSet<Company>();
            OrderLine = new HashSet<OrderLine>();
        }

        public long Id { get; set; }
        public string MainCompanyCode { get; set; }
        public int OrderType { get; set; }
        public string WarehouseCode { get; set; }
        public string OrderDate { get; set; }
        public string OrderCode { get; set; }
        public string OrderId { get; set; }
        public string CustomerOrderCode { get; set; }
        public string RelatedOrderCode { get; set; }
        public string WaybillCode { get; set; }
        public string PlannedDeliveryDate { get; set; }
        public string OrderPriority { get; set; }
        public int LoadingType { get; set; }
        public string OrderSubType { get; set; }
        public string ShipmentCode { get; set; }
        public string IntegrationDate { get; set; }
        public string TempAddress { get; set; }
        public int RecordType { get; set; }
        public int? CreateId { get; set; }
        public int? UpdateId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string FreeText { get; set; }
        public int SyncStatusCode { get; set; }
        public string SyncStatus { get; set; }
        public string SyncDesc { get; set; }
        public string TransactionCode { get; set; }
        public int GoodsTypeId { get; set; }
        public string LoadingDate { get; set; }
        public int TransportTypeCode { get; set; }

        public virtual ICollection<Company> Company { get; set; }
        public virtual ICollection<OrderLine> OrderLine { get; set; }
    }
}

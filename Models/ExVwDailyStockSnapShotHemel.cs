﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class ExVwDailyStockSnapShotHemel
    {
        public long? SiraNo { get; set; }
        public string StokTarihi { get; set; }
        public string UrunKodu { get; set; }
        public string UrunAciklamasi { get; set; }
        public string PaketTipi { get; set; }
        public long? Miktar { get; set; }
        public string LotNumarasi { get; set; }
        public string Depo { get; set; }
    }
}

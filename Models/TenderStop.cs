﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class TenderStop
    {
        public long Id { get; set; }
        public int ClaimId { get; set; }
        public string TenderCode { get; set; }
        public int StopNr { get; set; }
        public string StopType { get; set; }
        public string Name { get; set; }
        public string Street { get; set; }
        public string HouseNumber { get; set; }
        public string PostCode { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string CountryCode { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public int? CreateId { get; set; }
        public int? UpdateId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string FreeText { get; set; }
        public int SyncStatusCode { get; set; }
        public string SyncStatus { get; set; }
        public string SyncDesc { get; set; }
        public long? TenderId { get; set; }

        public virtual Tender Tender { get; set; }
    }
}

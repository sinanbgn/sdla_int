﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class VwDogalWaybillDelete
    {
        public string CompanyName { get; set; }
        public long DeliveryNumber { get; set; }
        public DateTime DeliveryDate { get; set; }
        public DateTime RegulatedDate { get; set; }
        public string OrderNumber { get; set; }
        public string CustomerCode { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public long Quantity { get; set; }
        public string UnitCode { get; set; }
    }
}

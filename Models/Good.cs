﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SdLa_Int.Models
{
    public class Good
    {
        public int grossweight { get; set; }
        public string grossweightbase { get; set; }
        public int cargoweight { get; set; }
        public int quantity { get; set; }
        public string packagetype { get; set; }
        public string externalsystemid { get; set; }
        public string description { get; set; }
    }
}

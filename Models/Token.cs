﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class Token
    {
        public int TokenId { get; set; }
        public string TokenKey { get; set; }
        public DateTime IssuedOn { get; set; }
        public DateTime ExpireDate { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public long CompanyId { get; set; }
    }
}

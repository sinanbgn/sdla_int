﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class PodSurveyAnswers
    {
        public long Id { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ShipmentId { get; set; }
        public string Questionarydescription { get; set; }
        public string Questionaryintroductiontext { get; set; }
        public string Questiontext { get; set; }
        public string Satisfactionrate { get; set; }
        public string Forwho { get; set; }
        public string Forwhodetails { get; set; }
        public string Answeredyn { get; set; }
        public string Shipmentreceiver { get; set; }
        public string Driverfullname { get; set; }
        public string Messagesentdatetime { get; set; }
        public string Replydatetime { get; set; }

        public virtual PodShipments Shipment { get; set; }
    }
}

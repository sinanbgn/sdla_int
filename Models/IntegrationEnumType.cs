﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class IntegrationEnumType
    {
        public long Id { get; set; }
        public string MapType { get; set; }
        public string IntegrationValue { get; set; }
        public string MapValue { get; set; }
        public string Description { get; set; }
    }
}

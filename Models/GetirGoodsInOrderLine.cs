﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class GetirGoodsInOrderLine
    {
        public long Id { get; set; }
        public string WareInventoryItemDetailNotes { get; set; }
        public string WareInventoryItemId { get; set; }
        public int WareInventoryItemPlanCuqty { get; set; }
        public string WareInventoryItemUnitId { get; set; }
        public string WareInventoryItemWrdetailCode { get; set; }
        public string IntegrationCode { get; set; }
        public string FreeText { get; set; }
        public int? CreateId { get; set; }
        public int? UpdateId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int SyncStatusCode { get; set; }
        public string SyncStatus { get; set; }
        public string SyncDesc { get; set; }
        public long? OrderId { get; set; }

        public virtual GetirGoodsInOrder Order { get; set; }
    }
}

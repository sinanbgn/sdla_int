﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class UetdsLog
    {
        public long Id { get; set; }
        public int? IslemId { get; set; }
        public string SonucKodu { get; set; }
        public string SonucMesaji { get; set; }
        public string UetdsBildirimRefNo { get; set; }
        public string IptalKodu { get; set; }
        public string IptalAciklama { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}

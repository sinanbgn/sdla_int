﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class VwGetirIntSendGoodsInAxata
    {
        public string IntegrationCode { get; set; }
        public string IntegrationId { get; set; }
        public string ApiToken { get; set; }
        public string Ss { get; set; }
        public string Date { get; set; }
        public string Image { get; set; }
        public int DocumentType { get; set; }
        public string ExpiryDate { get; set; }
        public int? Count { get; set; }
        public string ItemId { get; set; }
        public string IsDamaged { get; set; }
        public string Type { get; set; }
        public string DocumentId { get; set; }
        public int? Reports { get; set; }
        public string IntegrationResponseCode { get; set; }
        public string IntegrationResponseMessage { get; set; }
        public DateTime? IntegrationDate { get; set; }
        public int? IntegrationData { get; set; }
        public int? S16gtIntegrationStatus { get; set; }
        public string S16sipn { get; set; }
        public string DomainType { get; set; }
    }
}

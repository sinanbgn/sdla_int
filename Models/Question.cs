﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class Question
    {
        public Question()
        {
            QuestionAnswer = new HashSet<QuestionAnswer>();
        }

        public long Id { get; set; }
        public string QuestionText { get; set; }
        public int? CreateId { get; set; }
        public int? UpdateId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
        public long? QuestionHeaderId { get; set; }

        public virtual QuestionHeader QuestionHeader { get; set; }
        public virtual ICollection<QuestionAnswer> QuestionAnswer { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class VwShippedOrderLines
    {
        public string MainCompanyCode { get; set; }
        public string CompanyBranchCode { get; set; }
        public string ShipmentCode { get; set; }
        public string Plate { get; set; }
        public string DriverName { get; set; }
        public string DriverNationalityId { get; set; }
        public int OrderType { get; set; }
        public string OrderRefNumber { get; set; }
        public string OrderNumber { get; set; }
        public string OrderDate { get; set; }
        public string DispatchNumber { get; set; }
        public string IssueDate { get; set; }
        public string IssueTime { get; set; }
        public string DispatchType { get; set; }
        public string DispatchImageName { get; set; }
        public string PalletBarcode { get; set; }
        public string LineCode { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string LotCode { get; set; }
        public string ExpiryDate { get; set; }
        public decimal Quantity { get; set; }
        public string PackageType { get; set; }
        public decimal GrossWeight { get; set; }
        public int IsDamaged { get; set; }
        public string IntegrationResponseCode { get; set; }
        public string IntegrationResponseMessage { get; set; }
        public int? IntegrationDate { get; set; }
        public int? IntegrationData { get; set; }
    }
}

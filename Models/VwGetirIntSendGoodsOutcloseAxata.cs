﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class VwGetirIntSendGoodsOutcloseAxata
    {
        public int? TotalLine { get; set; }
        public int? SendLine { get; set; }
        public string OrderCode { get; set; }
        public string DocumentId { get; set; }
        public string ApiToken { get; set; }
        public string IntegrationCode { get; set; }
        public string Type { get; set; }
        public int? S75gtIntegrationStatus { get; set; }
        public string Domaintype { get; set; }
    }
}

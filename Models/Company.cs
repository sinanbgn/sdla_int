﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class Company
    {
        public long Id { get; set; }
        public string MainCompanyCode { get; set; }
        public int CompanyType { get; set; }
        public string CompanyCode { get; set; }
        public string ParentCompanyCode { get; set; }
        public string Name { get; set; }
        public string TaxNumber { get; set; }
        public string TaxDepartmentName { get; set; }
        public string CountryCode { get; set; }
        public string CityCode { get; set; }
        public string CityName { get; set; }
        public string TownCode { get; set; }
        public string TownName { get; set; }
        public string AddressText { get; set; }
        public string Gln { get; set; }
        public bool IsDispatcher { get; set; }
        public string Phone { get; set; }
        public string PostCode { get; set; }
        public string ContactCode { get; set; }
        public string ContactName { get; set; }
        public string ContactMail { get; set; }
        public string ContactPhone { get; set; }
        public int OrderRoleType { get; set; }
        public int? CreateId { get; set; }
        public int? UpdateId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string FreeText { get; set; }
        public int SyncStatusCode { get; set; }
        public string SyncStatus { get; set; }
        public string SyncDesc { get; set; }
        public long? OrderNewId { get; set; }

        public virtual Order OrderNew { get; set; }
    }
}

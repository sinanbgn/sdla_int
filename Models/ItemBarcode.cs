﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.Models
{
    public partial class ItemBarcode
    {
        public long Id { get; set; }
        public string ItemCode { get; set; }
        public string Barcode { get; set; }
        public bool IsDefault { get; set; }
        public int? CreateId { get; set; }
        public int? UpdateId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string FreeText { get; set; }
        public int SyncStatusCode { get; set; }
        public string SyncStatus { get; set; }
        public string SyncDesc { get; set; }
        public long? ItemId { get; set; }
        public int UnitType { get; set; }

        public virtual Item Item { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.LaModels
{
    public partial class LtraTripdetail
    {
        public long? TrdtVolumeunitid { get; set; }
        public long? TrdtWeightunitid { get; set; }
        public DateTime? TrdtPlannedsenderarrival { get; set; }
        public DateTime? TrdtPlannedsenderload { get; set; }
        public DateTime? TrdtPlannedsenderdepart { get; set; }
        public DateTime? TrdtPlannedcustomerarrival { get; set; }
        public DateTime? TrdtPlannedcustomerload { get; set; }
        public DateTime? TrdtPlannedcustomerdepart { get; set; }
        public long? TrdtTransporttaskid { get; set; }
        public long TrdtId { get; set; }
        public long TrdtTransportationorderid { get; set; }
        public long TrdtTripid { get; set; }
        public long? TrdtTransportorderdetid { get; set; }
        public decimal? TrdtQuantity { get; set; }
        public decimal? TrdtVolume { get; set; }
        public decimal? TrdtWeight { get; set; }
        public DateTime? TrdtExpecteddeliverydate { get; set; }
        public DateTime? TrdtActualdeliverydate { get; set; }
        public string TrdtNotes { get; set; }
        public long? TrdtCustomerid { get; set; }
        public decimal? TrdtPurchasebaseprice { get; set; }
        public long? TrdtTransportationpricetypeid { get; set; }
        public long? TrdtPurchasecurrencycodeid { get; set; }
        public decimal? TrdtActualweight { get; set; }
        public decimal? TrdtActualquantity { get; set; }
        public decimal? TrdtActualvolume { get; set; }
        public decimal? TrdtDriverpenalty { get; set; }
        public long TrdtTripdetailstatusid { get; set; }
        public bool? TrdtDrivergetspayment { get; set; }
        public long? TrdtTransportproductgroupid { get; set; }
        public string TrdtDescription { get; set; }
        public DateTime? TrdtActualloaddatetime { get; set; }
        public decimal? TrdtCalculatedprice { get; set; }
        public long? TrdtRouteorder { get; set; }
        public decimal? TrdtPalletqty { get; set; }
        public long? TrdtFrompartyaddressid { get; set; }
        public long? TrdtTopartyaddressid { get; set; }
        public string TrdtUpdatedby { get; set; }
        public DateTime? TrdtUpdateddatetime { get; set; }
        public DateTime? TrdtWaybillprintdate { get; set; }
        public long? TrdtTriptypeid { get; set; }
        public long? TrdtPlannedkm { get; set; }
        public string TrdtHawbno { get; set; }
        public decimal? TrdtDeclaredvalforcarriage { get; set; }
        public bool? TrdtOtherprepaid { get; set; }
        public bool? TrdtIsweightvalprepaid { get; set; }
        public decimal? TrdtDeclaredvalforcustoms { get; set; }
        public decimal? TrdtGrossweight { get; set; }
        public string TrdtEnteredby { get; set; }
        public DateTime? TrdtEntrydatetime { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public int? Sdintegration { get; set; }
        public DateTime? Sddatetime { get; set; }
        public byte? Sddelivery { get; set; }

        public virtual LtraTransportationorder TrdtTransportationorder { get; set; }
        public virtual LtraTrip TrdtTrip { get; set; }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SdLa_Int.LaModels
{
    public partial class LawepContext : DbContext
    {
        public LawepContext()
        {
        }

        public LawepContext(DbContextOptions<LawepContext> options)
            : base(options)
        {
        }

        public virtual DbSet<LtraTransportationorder> LtraTransportationorder { get; set; }
        public virtual DbSet<LtraTransportorddeliveryinf> LtraTransportorddeliveryinf { get; set; }
        public virtual DbSet<LtraTrip> LtraTrip { get; set; }
        public virtual DbSet<LtraTripdetail> LtraTripdetail { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Data Source=192.168.2.43;Initial Catalog=Lawep;Integrated Security=False;Persist Security Info=False;User ID=onsel.aydin;Password=Oa123456+");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<LtraTransportationorder>(entity =>
            {
                entity.HasKey(e => e.TrorId)
                    .HasName("LTRA_TRANSPORTATIONORDER_PK");

                entity.ToTable("LTRA_TRANSPORTATIONORDER");

                entity.HasIndex(e => e.RefCode)
                    .HasName("iTROR_REFERENCE_CODE_LTRA_TRANSPORTATIONORDER");

                entity.HasIndex(e => e.RefSeq)
                    .HasName("RefSeq");

                entity.HasIndex(e => e.TrorCode)
                    .HasName("LTRA_TRANSPORTATIONORDER_UI")
                    .IsUnique();

                entity.HasIndex(e => e.TrorCompanyid)
                    .HasName("iTROR_COMPANYID_LTRA_TRANSPORTATIONORDER");

                entity.HasIndex(e => e.TrorDepositorid)
                    .HasName("iTROR_DEPOSITORID_LTRA_TRANSPORTATIONORDER");

                entity.HasIndex(e => e.TrorInventorysiteid)
                    .HasName("iTROR_INVENTORYSITEID_LTRA_TRANSPORTATIONORDER");

                entity.HasIndex(e => e.TrorTocityid)
                    .HasName("iTROR_TOCITYID_LTRA_TRANSPORTATIONORDER");

                entity.HasIndex(e => new { e.TrorTotownid, e.TrorTocityid, e.TrorId })
                    .HasName("_dta_index_LTRA_TRANSPORTATIONORDER_5_748594501__K107_K48_K36");

                entity.HasIndex(e => new { e.TrorWeight, e.TrorTocityid, e.TrorTotownid })
                    .HasName("_dta_index_LTRA_TRANSPORTATIONORDER_5_748594501__K36_48_107");

                entity.HasIndex(e => new { e.TrorDepositorid, e.TrorProjectid, e.TrorTransportorderstatusid, e.TrorOrderdate })
                    .HasName("IX_TROR_DEPOSITORID_TROR_PROJECTID");

                entity.HasIndex(e => new { e.TrorTransportationordertypeid, e.TrorInventorysiteid, e.TrorId, e.TrorDepositorid })
                    .HasName("_dta_index_LTRA_TRANSPORTATIONORDER_5_1777558262__K74_K94_K36_K43");

                entity.HasIndex(e => new { e.TrorId, e.TrorCustomerpartyaddressid, e.TrorTransportationordertypeid, e.TrorWeight, e.TrorVolume, e.TrorQuantity })
                    .HasName("_dta_index_LTRA_TRANSPORTATIONORDER_5_1777558262__K36_K64_K74_K5_K6_K7");

                entity.HasIndex(e => new { e.TrorToadress, e.TrorCustomerordernumber, e.TrorDepositorid, e.TrorId, e.TrorFromtownid, e.TrorCustomerid })
                    .HasName("_dta_index_LTRA_TRANSPORTATIONORDER_5_748594501__K43_K36_K108_K44_54_103");

                entity.HasIndex(e => new { e.TrorCode, e.TrorCustomerid, e.TrorDepositorsdocument, e.TrorTotownid, e.TrorFromtownid, e.TrorFromcityid, e.TrorTransportationordertypeid, e.TrorDepositorid, e.TrorOrderdate })
                    .HasName("IX_TROR_FROMCITYID_TROR_TRANSPORTATIONORDERTYPEID");

                entity.HasIndex(e => new { e.TrorWeight, e.TrorDepositorsdocument, e.TrorOrderdate, e.TrorActualdeliverydatetime, e.TrorActualloaddatetime, e.TrorApprovaldate, e.TrorDepositorid, e.TrorTransportorderstatusid, e.TrorId, e.TrorTotownid, e.TrorCustomerid, e.TrorTransportsenderid, e.TrorTocityid, e.TrorTransportationordertypeid, e.TrorTransportationcontractid })
                    .HasName("_dta_index_LTRA_TRANSPORTATIONORDER_5_748594501__K43_K59_K36_K107_K44_K51_K48_K74_K73_5_57_61_98_99_104");

                entity.HasIndex(e => new { e.TrorWeight, e.TrorVolume, e.TrorQuantity, e.TrorTripinvoicetypeid, e.TrorId, e.TrorCode, e.TrorDepositorid, e.TrorFromcityid, e.TrorTocityid, e.TrorToadress, e.TrorTransportationtypeid, e.TrorOrderdate, e.TrorDeparturecustoms, e.TrorInventorysiteid, e.TrorTotownid, e.TrorTransportationordertypeid, e.TrorTransportorderstatusid })
                    .HasName("IX_TROR_TRANSPORTORDERSTATUSID");

                entity.Property(e => e.TrorId)
                    .HasColumnName("TROR_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Currency).HasMaxLength(5);

                entity.Property(e => e.Price).HasColumnType("decimal(24, 8)");

                entity.Property(e => e.PricingTypeCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RefCode).HasMaxLength(100);

                entity.Property(e => e.SpintegrationDate)
                    .HasColumnName("SPIntegrationDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.SpintegrationStatus).HasColumnName("SPIntegrationStatus");

                entity.Property(e => e.TrorActualdeliverydatetime)
                    .HasColumnName("TROR_ACTUALDELIVERYDATETIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.TrorActualkm).HasColumnName("TROR_ACTUALKM");

                entity.Property(e => e.TrorActualloaddatetime)
                    .HasColumnName("TROR_ACTUALLOADDATETIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.TrorAddressdirections)
                    .HasColumnName("TROR_ADDRESSDIRECTIONS")
                    .HasMaxLength(400);

                entity.Property(e => e.TrorAgencyid).HasColumnName("TROR_AGENCYID");

                entity.Property(e => e.TrorApprovaldate)
                    .HasColumnName("TROR_APPROVALDATE")
                    .HasColumnType("datetime");

                entity.Property(e => e.TrorArrivalcustoms)
                    .HasColumnName("TROR_ARRIVALCUSTOMS")
                    .HasMaxLength(200);

                entity.Property(e => e.TrorCancelreason)
                    .HasColumnName("TROR_CANCELREASON")
                    .HasMaxLength(200);

                entity.Property(e => e.TrorCmr)
                    .HasColumnName("TROR_CMR")
                    .HasMaxLength(200);

                entity.Property(e => e.TrorCode)
                    .IsRequired()
                    .HasColumnName("TROR_CODE")
                    .HasMaxLength(200);

                entity.Property(e => e.TrorCompanyid).HasColumnName("TROR_COMPANYID");

                entity.Property(e => e.TrorCustomerid).HasColumnName("TROR_CUSTOMERID");

                entity.Property(e => e.TrorCustomerordernumber)
                    .HasColumnName("TROR_CUSTOMERORDERNUMBER")
                    .HasMaxLength(900);

                entity.Property(e => e.TrorCustomerpartyaddressid).HasColumnName("TROR_CUSTOMERPARTYADDRESSID");

                entity.Property(e => e.TrorCustomsdeclarationdate)
                    .HasColumnName("TROR_CUSTOMSDECLARATIONDATE")
                    .HasMaxLength(200);

                entity.Property(e => e.TrorCustomsdeclarationnumber)
                    .HasColumnName("TROR_CUSTOMSDECLARATIONNUMBER")
                    .HasMaxLength(200);

                entity.Property(e => e.TrorCustomsid).HasColumnName("TROR_CUSTOMSID");

                entity.Property(e => e.TrorDeclaredvalforcarriage)
                    .HasColumnName("TROR_DECLAREDVALFORCARRIAGE")
                    .HasColumnType("decimal(24, 8)");

                entity.Property(e => e.TrorDeclaredvalforcustoms)
                    .HasColumnName("TROR_DECLAREDVALFORCUSTOMS")
                    .HasColumnType("decimal(24, 8)");

                entity.Property(e => e.TrorDeliveryleadtime).HasColumnName("TROR_DELIVERYLEADTIME");

                entity.Property(e => e.TrorDeliverynote)
                    .HasColumnName("TROR_DELIVERYNOTE")
                    .HasMaxLength(400);

                entity.Property(e => e.TrorDeliverytermsid).HasColumnName("TROR_DELIVERYTERMSID");

                entity.Property(e => e.TrorDensity)
                    .HasColumnName("TROR_DENSITY")
                    .HasColumnType("decimal(24, 8)");

                entity.Property(e => e.TrorDeparturecustoms)
                    .HasColumnName("TROR_DEPARTURECUSTOMS")
                    .HasMaxLength(200);

                entity.Property(e => e.TrorDepositorid).HasColumnName("TROR_DEPOSITORID");

                entity.Property(e => e.TrorDepositorsdocument)
                    .HasColumnName("TROR_DEPOSITORSDOCUMENT")
                    .HasMaxLength(900);

                entity.Property(e => e.TrorEnteredby)
                    .HasColumnName("TROR_ENTEREDBY")
                    .HasMaxLength(400);

                entity.Property(e => e.TrorEntrydatetime)
                    .HasColumnName("TROR_ENTRYDATETIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.TrorExpectedreadydatetime)
                    .HasColumnName("TROR_EXPECTEDREADYDATETIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.TrorFcr)
                    .HasColumnName("TROR_FCR")
                    .HasMaxLength(200);

                entity.Property(e => e.TrorFromadress)
                    .HasColumnName("TROR_FROMADRESS")
                    .HasMaxLength(800);

                entity.Property(e => e.TrorFromcityid).HasColumnName("TROR_FROMCITYID");

                entity.Property(e => e.TrorFromcountryid).HasColumnName("TROR_FROMCOUNTRYID");

                entity.Property(e => e.TrorFromdistictid).HasColumnName("TROR_FROMDISTICTID");

                entity.Property(e => e.TrorFromgeoareaid).HasColumnName("TROR_FROMGEOAREAID");

                entity.Property(e => e.TrorFrompostalcodeid).HasColumnName("TROR_FROMPOSTALCODEID");

                entity.Property(e => e.TrorFromtownid).HasColumnName("TROR_FROMTOWNID");

                entity.Property(e => e.TrorFromzoneid).HasColumnName("TROR_FROMZONEID");

                entity.Property(e => e.TrorInsuaranceamount)
                    .HasColumnName("TROR_INSUARANCEAMOUNT")
                    .HasColumnType("decimal(28, 8)");

                entity.Property(e => e.TrorInventorysiteid).HasColumnName("TROR_INVENTORYSITEID");

                entity.Property(e => e.TrorInvoiceid).HasColumnName("TROR_INVOICEID");

                entity.Property(e => e.TrorInvoicetoaddressid).HasColumnName("TROR_INVOICETOADDRESSID");

                entity.Property(e => e.TrorIselevator).HasColumnName("TROR_ISELEVATOR");

                entity.Property(e => e.TrorIsinternational).HasColumnName("TROR_ISINTERNATIONAL");

                entity.Property(e => e.TrorIsseparateinvoice).HasColumnName("TROR_ISSEPARATEINVOICE");

                entity.Property(e => e.TrorIsweightvalprepaid).HasColumnName("TROR_ISWEIGHTVALPREPAID");

                entity.Property(e => e.TrorKm).HasColumnName("TROR_KM");

                entity.Property(e => e.TrorLetterofcreditnumber)
                    .HasColumnName("TROR_LETTEROFCREDITNUMBER")
                    .HasMaxLength(200);

                entity.Property(e => e.TrorManufacturerid).HasColumnName("TROR_MANUFACTURERID");

                entity.Property(e => e.TrorMilestoneid).HasColumnName("TROR_MILESTONEID");

                entity.Property(e => e.TrorMilestonetemplateid).HasColumnName("TROR_MILESTONETEMPLATEID");

                entity.Property(e => e.TrorNotes)
                    .HasColumnName("TROR_NOTES")
                    .HasMaxLength(1000);

                entity.Property(e => e.TrorNumberoffloor)
                    .HasColumnName("TROR_NUMBEROFFLOOR")
                    .HasColumnType("decimal(3, 0)");

                entity.Property(e => e.TrorOfficialdocumentno)
                    .HasColumnName("TROR_OFFICIALDOCUMENTNO")
                    .HasMaxLength(300);

                entity.Property(e => e.TrorOrderdate)
                    .HasColumnName("TROR_ORDERDATE")
                    .HasColumnType("datetime");

                entity.Property(e => e.TrorOrderedby)
                    .HasColumnName("TROR_ORDEREDBY")
                    .HasMaxLength(200);

                entity.Property(e => e.TrorOrdervalue)
                    .HasColumnName("TROR_ORDERVALUE")
                    .HasColumnType("decimal(28, 8)");

                entity.Property(e => e.TrorOrdervatpercent).HasColumnName("TROR_ORDERVATPERCENT");

                entity.Property(e => e.TrorOtherprepaid).HasColumnName("TROR_OTHERPREPAID");

                entity.Property(e => e.TrorPayincash).HasColumnName("TROR_PAYINCASH");

                entity.Property(e => e.TrorPaymentpartyid).HasColumnName("TROR_PAYMENTPARTYID");

                entity.Property(e => e.TrorPaymenttermid).HasColumnName("TROR_PAYMENTTERMID");

                entity.Property(e => e.TrorPaymentwayid).HasColumnName("TROR_PAYMENTWAYID");

                entity.Property(e => e.TrorPlanneddeliverydatetime)
                    .HasColumnName("TROR_PLANNEDDELIVERYDATETIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.TrorPlannedshipmentdatetime)
                    .HasColumnName("TROR_PLANNEDSHIPMENTDATETIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.TrorProjectid).HasColumnName("TROR_PROJECTID");

                entity.Property(e => e.TrorPurchaseinvoicablekm).HasColumnName("TROR_PURCHASEINVOICABLEKM");

                entity.Property(e => e.TrorPurchbaseamount)
                    .HasColumnName("TROR_PURCHBASEAMOUNT")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TrorPurchcurrencyamount)
                    .HasColumnName("TROR_PURCHCURRENCYAMOUNT")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TrorPurchcurrencycodeid)
                    .HasColumnName("TROR_PURCHCURRENCYCODEID")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.TrorPurchcurrencyrate)
                    .HasColumnName("TROR_PURCHCURRENCYRATE")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TrorQuantity).HasColumnName("TROR_QUANTITY");

                entity.Property(e => e.TrorReferancecode)
                    .HasColumnName("TROR_REFERANCECODE")
                    .HasMaxLength(100);

                entity.Property(e => e.TrorReferanceid).HasColumnName("TROR_REFERANCEID");

                entity.Property(e => e.TrorReferancekey)
                    .HasColumnName("TROR_REFERANCEKEY")
                    .HasMaxLength(40);

                entity.Property(e => e.TrorRegionid).HasColumnName("TROR_REGIONID");

                entity.Property(e => e.TrorReplicationid).HasColumnName("TROR_REPLICATIONID");

                entity.Property(e => e.TrorReportdate1)
                    .HasColumnName("TROR_REPORTDATE1")
                    .HasColumnType("datetime");

                entity.Property(e => e.TrorReportdate2)
                    .HasColumnName("TROR_REPORTDATE2")
                    .HasColumnType("datetime");

                entity.Property(e => e.TrorReportdate3)
                    .HasColumnName("TROR_REPORTDATE3")
                    .HasColumnType("datetime");

                entity.Property(e => e.TrorRequestedassemblydate)
                    .HasColumnName("TROR_REQUESTEDASSEMBLYDATE")
                    .HasColumnType("date");

                entity.Property(e => e.TrorRequesteddeliverydate)
                    .HasColumnName("TROR_REQUESTEDDELIVERYDATE")
                    .HasColumnType("date");

                entity.Property(e => e.TrorReturnoforderid).HasColumnName("TROR_RETURNOFORDERID");

                entity.Property(e => e.TrorRouteid).HasColumnName("TROR_ROUTEID");

                entity.Property(e => e.TrorSalesamountapproved).HasColumnName("TROR_SALESAMOUNTAPPROVED");

                entity.Property(e => e.TrorSalesbaseamount)
                    .HasColumnName("TROR_SALESBASEAMOUNT")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TrorSalescurrencyamount)
                    .HasColumnName("TROR_SALESCURRENCYAMOUNT")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TrorSalescurrencycodeid).HasColumnName("TROR_SALESCURRENCYCODEID");

                entity.Property(e => e.TrorSalescurrencyrate)
                    .HasColumnName("TROR_SALESCURRENCYRATE")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TrorSalesinvoicablekm).HasColumnName("TROR_SALESINVOICABLEKM");

                entity.Property(e => e.TrorSalesinvoiced).HasColumnName("TROR_SALESINVOICED");

                entity.Property(e => e.TrorSenderpartyaddressid).HasColumnName("TROR_SENDERPARTYADDRESSID");

                entity.Property(e => e.TrorShipmentpacknumber).HasColumnName("TROR_SHIPMENTPACKNUMBER");

                entity.Property(e => e.TrorToadress)
                    .HasColumnName("TROR_TOADRESS")
                    .HasMaxLength(800);

                entity.Property(e => e.TrorTocityid).HasColumnName("TROR_TOCITYID");

                entity.Property(e => e.TrorTocountryid).HasColumnName("TROR_TOCOUNTRYID");

                entity.Property(e => e.TrorTodistictid).HasColumnName("TROR_TODISTICTID");

                entity.Property(e => e.TrorTogeoareaid).HasColumnName("TROR_TOGEOAREAID");

                entity.Property(e => e.TrorTopostalcodeid).HasColumnName("TROR_TOPOSTALCODEID");

                entity.Property(e => e.TrorTotownid).HasColumnName("TROR_TOTOWNID");

                entity.Property(e => e.TrorTozoneid).HasColumnName("TROR_TOZONEID");

                entity.Property(e => e.TrorTransitwarehousesid).HasColumnName("TROR_TRANSITWAREHOUSESID");

                entity.Property(e => e.TrorTransordcancelreasonid).HasColumnName("TROR_TRANSORDCANCELREASONID");

                entity.Property(e => e.TrorTransportationcontractid).HasColumnName("TROR_TRANSPORTATIONCONTRACTID");

                entity.Property(e => e.TrorTransportationordertypeid).HasColumnName("TROR_TRANSPORTATIONORDERTYPEID");

                entity.Property(e => e.TrorTransportationpayeeid).HasColumnName("TROR_TRANSPORTATIONPAYEEID");

                entity.Property(e => e.TrorTransportationpricetypeid).HasColumnName("TROR_TRANSPORTATIONPRICETYPEID");

                entity.Property(e => e.TrorTransportationpriorityid).HasColumnName("TROR_TRANSPORTATIONPRIORITYID");

                entity.Property(e => e.TrorTransportationtypeid).HasColumnName("TROR_TRANSPORTATIONTYPEID");

                entity.Property(e => e.TrorTransportationwayid).HasColumnName("TROR_TRANSPORTATIONWAYID");

                entity.Property(e => e.TrorTransportorderentrytypeid).HasColumnName("TROR_TRANSPORTORDERENTRYTYPEID");

                entity.Property(e => e.TrorTransportorderstatusid).HasColumnName("TROR_TRANSPORTORDERSTATUSID");

                entity.Property(e => e.TrorTransportpurchcontractid).HasColumnName("TROR_TRANSPORTPURCHCONTRACTID");

                entity.Property(e => e.TrorTransportscheduleid).HasColumnName("TROR_TRANSPORTSCHEDULEID");

                entity.Property(e => e.TrorTransportsenderid).HasColumnName("TROR_TRANSPORTSENDERID");

                entity.Property(e => e.TrorTransportservicelevelid).HasColumnName("TROR_TRANSPORTSERVICELEVELID");

                entity.Property(e => e.TrorTranspprojectscheduleid).HasColumnName("TROR_TRANSPPROJECTSCHEDULEID");

                entity.Property(e => e.TrorTripinvoicetypeid).HasColumnName("TROR_TRIPINVOICETYPEID");

                entity.Property(e => e.TrorUpdatedby)
                    .HasColumnName("TROR_UPDATEDBY")
                    .HasMaxLength(200);

                entity.Property(e => e.TrorUpdateddatetime)
                    .HasColumnName("TROR_UPDATEDDATETIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.TrorVatrate)
                    .HasColumnName("TROR_VATRATE")
                    .HasColumnType("decimal(24, 8)");

                entity.Property(e => e.TrorVehicleid).HasColumnName("TROR_VEHICLEID");

                entity.Property(e => e.TrorVolume)
                    .HasColumnName("TROR_VOLUME")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TrorWarehouseorderid).HasColumnName("TROR_WAREHOUSEORDERID");

                entity.Property(e => e.TrorWarehousereceiptid).HasColumnName("TROR_WAREHOUSERECEIPTID");

                entity.Property(e => e.TrorWeight)
                    .HasColumnName("TROR_WEIGHT")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TypeCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LtraTransportorddeliveryinf>(entity =>
            {
                entity.HasKey(e => e.TodiId)
                    .HasName("LTRA_TRANSPORTORDDE_PK");

                entity.ToTable("LTRA_TRANSPORTORDDELIVERYINF");

                entity.HasIndex(e => e.TodiTransportationorderid)
                    .HasName("missing_index_54");

                entity.Property(e => e.TodiId)
                    .HasColumnName("TODI_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastModifiedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.TodiActionstatusid).HasColumnName("TODI_ACTIONSTATUSID");

                entity.Property(e => e.TodiArrivaldatetime)
                    .HasColumnName("TODI_ARRIVALDATETIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.TodiAsnquantity).HasColumnName("TODI_ASNQUANTITY");

                entity.Property(e => e.TodiChangedpartyaddressid).HasColumnName("TODI_CHANGEDPARTYADDRESSID");

                entity.Property(e => e.TodiCode)
                    .HasColumnName("TODI_CODE")
                    .HasMaxLength(100);

                entity.Property(e => e.TodiDeliverydatetime)
                    .HasColumnName("TODI_DELIVERYDATETIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.TodiDeliveryplace)
                    .HasColumnName("TODI_DELIVERYPLACE")
                    .HasMaxLength(200);

                entity.Property(e => e.TodiDeparturedatetime)
                    .HasColumnName("TODI_DEPARTUREDATETIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.TodiDescription)
                    .HasColumnName("TODI_DESCRIPTION")
                    .HasMaxLength(400);

                entity.Property(e => e.TodiEnteredby)
                    .HasColumnName("TODI_ENTEREDBY")
                    .HasMaxLength(200);

                entity.Property(e => e.TodiEntrydatetime)
                    .HasColumnName("TODI_ENTRYDATETIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.TodiIdentityno)
                    .HasColumnName("TODI_IDENTITYNO")
                    .HasMaxLength(200);

                entity.Property(e => e.TodiIdentitytypeid).HasColumnName("TODI_IDENTITYTYPEID");

                entity.Property(e => e.TodiImage)
                    .HasColumnName("TODI_IMAGE")
                    .HasColumnType("image");

                entity.Property(e => e.TodiIsapproved).HasColumnName("TODI_ISAPPROVED");

                entity.Property(e => e.TodiKm)
                    .HasColumnName("TODI_KM")
                    .HasColumnType("decimal(24, 8)");

                entity.Property(e => e.TodiNotes)
                    .HasColumnName("TODI_NOTES")
                    .HasMaxLength(1000);

                entity.Property(e => e.TodiPoddocumentno)
                    .HasColumnName("TODI_PODDOCUMENTNO")
                    .HasMaxLength(500);

                entity.Property(e => e.TodiProductdescription)
                    .HasColumnName("TODI_PRODUCTDESCRIPTION")
                    .HasMaxLength(500);

                entity.Property(e => e.TodiReceiverphoto)
                    .HasColumnName("TODI_RECEIVERPHOTO")
                    .HasMaxLength(500);

                entity.Property(e => e.TodiRecievedby)
                    .HasColumnName("TODI_RECIEVEDBY")
                    .HasMaxLength(200);

                entity.Property(e => e.TodiShipmentbarcode)
                    .HasColumnName("TODI_SHIPMENTBARCODE")
                    .HasMaxLength(200);

                entity.Property(e => e.TodiTransordadrchangreasonid).HasColumnName("TODI_TRANSORDADRCHANGREASONID");

                entity.Property(e => e.TodiTransportationorderid).HasColumnName("TODI_TRANSPORTATIONORDERID");

                entity.Property(e => e.TodiTransportdelayreasonid).HasColumnName("TODI_TRANSPORTDELAYREASONID");

                entity.Property(e => e.TodiTransportorderdetid).HasColumnName("TODI_TRANSPORTORDERDETID");

                entity.Property(e => e.TodiTransportreturnreasonid).HasColumnName("TODI_TRANSPORTRETURNREASONID");

                entity.Property(e => e.TodiTripid).HasColumnName("TODI_TRIPID");

                entity.Property(e => e.TodiUpdatedby)
                    .HasColumnName("TODI_UPDATEDBY")
                    .HasMaxLength(200);

                entity.Property(e => e.TodiUpdateddatetime)
                    .HasColumnName("TODI_UPDATEDDATETIME")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.TodiTransportationorder)
                    .WithMany(p => p.LtraTransportorddeliveryinf)
                    .HasForeignKey(d => d.TodiTransportationorderid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("TRANSPORTORDDELIVER_FK04");

                entity.HasOne(d => d.TodiTrip)
                    .WithMany(p => p.LtraTransportorddeliveryinf)
                    .HasForeignKey(d => d.TodiTripid)
                    .HasConstraintName("TRANSPORTORDDELIVER_FK03");
            });

            modelBuilder.Entity<LtraTrip>(entity =>
            {
                entity.HasKey(e => e.TripId)
                    .HasName("LTRA_TRIP_PK");

                entity.ToTable("LTRA_TRIP");

                entity.HasIndex(e => e.TripCode)
                    .HasName("LTRA_TRIP_UI")
                    .IsUnique();

                entity.HasIndex(e => e.TripCompanyid)
                    .HasName("iTRIP_COMPANYID_LTRA_TRIP");

                entity.HasIndex(e => e.TripInventorysiteid)
                    .HasName("iTRIP_INVENTORYSITEID_LTRA_TRIP");

                entity.HasIndex(e => e.TripTransportationcontractid)
                    .HasName("iTRIP_TRANSPORTATIONCONTRACTID_LTRA_TRIP");

                entity.HasIndex(e => e.TripTransportsubcontrorid)
                    .HasName("iTRIP_TRANSPORTSUBCONTRORID_LTRA_TRIP");

                entity.HasIndex(e => e.TripTripstatusid)
                    .HasName("iTRIP_TRIPSTATUSID_LTRA_TRIP");

                entity.HasIndex(e => e.TripVehicleid)
                    .HasName("iTRIP_VEHICLEID_LTRA_TRIP");

                entity.HasIndex(e => new { e.TripId, e.TripCode })
                    .HasName("_dta_index_LTRA_TRIP_5_1404596838__K1_K2");

                entity.HasIndex(e => new { e.TripPlanneddepartdatetime, e.TripInventorysiteid })
                    .HasName("IX_TRIP_PLANNEDDEPARTDATETIME_TRIP_INVENTORYSITEID");

                entity.HasIndex(e => new { e.TripParenttripid, e.TripId, e.TripCode })
                    .HasName("_dta_index_LTRA_TRIP_5_1404596838__K29_K1_K2");

                entity.HasIndex(e => new { e.TripCode, e.TripPlanneddepartdatetime, e.TripId, e.TripVehicleid, e.TripVehicledriverid })
                    .HasName("_dta_index_LTRA_TRIP_5_1404596838__K1_K5_K6_2_7");

                entity.HasIndex(e => new { e.TripPlanneddepartdatetime, e.TripPurchcurrencyamount, e.TripId, e.TripVehicleid, e.TripInventorysiteid, e.TripCode })
                    .HasName("_dta_index_LTRA_TRIP_5_1404596838__K1_K5_K45_K2_7_63");

                entity.HasIndex(e => new { e.TripPlanneddepartdatetime, e.TripId, e.TripTripinvoicetypeid, e.TripVehicleid, e.TripTriptypeid, e.TripTripstatusid, e.TripCode })
                    .HasName("_dta_index_LTRA_TRIP_5_1404596838__K1_K10_K5_K33_K9_K2_7");

                entity.HasIndex(e => new { e.TripTripdistancekm, e.TripVehicleid, e.TripPlanneddepartdatetime, e.TripTripstatusid, e.TripVehicledriverid, e.TripInventorysiteid, e.TripId, e.TripTransportsubcontrorid, e.TripCode })
                    .HasName("_dta_index_LTRA_TRIP_5_1404596838__K5_K7_K9_K6_K45_K1_K50_K2_24");

                entity.Property(e => e.TripId)
                    .HasColumnName("TRIP_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsUpdate).HasDefaultValueSql("((0))");

                entity.Property(e => e.LastModifiedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.TripActualarrivaldatetime)
                    .HasColumnName("TRIP_ACTUALARRIVALDATETIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.TripActualdepartdatetime)
                    .HasColumnName("TRIP_ACTUALDEPARTDATETIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.TripAppointedbyid).HasColumnName("TRIP_APPOINTEDBYID");

                entity.Property(e => e.TripArrivalcustoms)
                    .HasColumnName("TRIP_ARRIVALCUSTOMS")
                    .HasMaxLength(2000);

                entity.Property(e => e.TripAubkno)
                    .HasColumnName("TRIP_AUBKNO")
                    .HasMaxLength(200);

                entity.Property(e => e.TripBeginninglitre)
                    .HasColumnName("TRIP_BEGINNINGLITRE")
                    .HasColumnType("decimal(24, 8)");

                entity.Property(e => e.TripBookingnumber)
                    .HasColumnName("TRIP_BOOKINGNUMBER")
                    .HasMaxLength(80);

                entity.Property(e => e.TripCode)
                    .IsRequired()
                    .HasColumnName("TRIP_CODE")
                    .HasMaxLength(200);

                entity.Property(e => e.TripCompanyid).HasColumnName("TRIP_COMPANYID");

                entity.Property(e => e.TripContainerno)
                    .HasColumnName("TRIP_CONTAINERNO")
                    .HasMaxLength(200);

                entity.Property(e => e.TripContainertypeid).HasColumnName("TRIP_CONTAINERTYPEID");

                entity.Property(e => e.TripDeparturecustoms)
                    .HasColumnName("TRIP_DEPARTURECUSTOMS")
                    .HasMaxLength(2000);

                entity.Property(e => e.TripDepositorsdocumentno)
                    .HasColumnName("TRIP_DEPOSITORSDOCUMENTNO")
                    .HasMaxLength(200);

                entity.Property(e => e.TripDescription)
                    .HasColumnName("TRIP_DESCRIPTION")
                    .HasMaxLength(400);

                entity.Property(e => e.TripDocumentarrivaldate)
                    .HasColumnName("TRIP_DOCUMENTARRIVALDATE")
                    .HasColumnType("datetime");

                entity.Property(e => e.TripDocumentarrived).HasColumnName("TRIP_DOCUMENTARRIVED");

                entity.Property(e => e.TripDriverinvoicedatetime)
                    .HasColumnName("TRIP_DRIVERINVOICEDATETIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.TripDriverinvoicenumber)
                    .HasColumnName("TRIP_DRIVERINVOICENUMBER")
                    .HasMaxLength(100);

                entity.Property(e => e.TripEmptycontreturndate)
                    .HasColumnName("TRIP_EMPTYCONTRETURNDATE")
                    .HasColumnType("datetime");

                entity.Property(e => e.TripEndinglitre)
                    .HasColumnName("TRIP_ENDINGLITRE")
                    .HasColumnType("decimal(24, 8)");

                entity.Property(e => e.TripEnteredby)
                    .HasColumnName("TRIP_ENTEREDBY")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TripEntrydatetime)
                    .HasColumnName("TRIP_ENTRYDATETIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.TripExtravehicleid).HasColumnName("TRIP_EXTRAVEHICLEID");

                entity.Property(e => e.TripFromaddress)
                    .HasColumnName("TRIP_FROMADDRESS")
                    .HasMaxLength(500);

                entity.Property(e => e.TripFromaddressid).HasColumnName("TRIP_FROMADDRESSID");

                entity.Property(e => e.TripFromcityid).HasColumnName("TRIP_FROMCITYID");

                entity.Property(e => e.TripFromgeoareaid).HasColumnName("TRIP_FROMGEOAREAID");

                entity.Property(e => e.TripFrompostalcodeid).HasColumnName("TRIP_FROMPOSTALCODEID");

                entity.Property(e => e.TripIncludeintransportreport).HasColumnName("TRIP_INCLUDEINTRANSPORTREPORT");

                entity.Property(e => e.TripInsuaranceamount)
                    .HasColumnName("TRIP_INSUARANCEAMOUNT")
                    .HasColumnType("decimal(28, 8)");

                entity.Property(e => e.TripInventorysiteid).HasColumnName("TRIP_INVENTORYSITEID");

                entity.Property(e => e.TripInvoiceabledistancekm)
                    .HasColumnName("TRIP_INVOICEABLEDISTANCEKM")
                    .HasColumnType("decimal(18, 8)");

                entity.Property(e => e.TripInvoiceabletripdistancekm)
                    .HasColumnName("TRIP_INVOICEABLETRIPDISTANCEKM")
                    .HasColumnType("decimal(18, 8)");

                entity.Property(e => e.TripInvoiceabletripquantity)
                    .HasColumnName("TRIP_INVOICEABLETRIPQUANTITY")
                    .HasColumnType("decimal(18, 8)");

                entity.Property(e => e.TripInvoiceablevalue)
                    .HasColumnName("TRIP_INVOICEABLEVALUE")
                    .HasColumnType("decimal(18, 8)");

                entity.Property(e => e.TripInvoiceablevolume)
                    .HasColumnName("TRIP_INVOICEABLEVOLUME")
                    .HasColumnType("decimal(18, 8)");

                entity.Property(e => e.TripInvoiceableweight)
                    .HasColumnName("TRIP_INVOICEABLEWEIGHT")
                    .HasColumnType("decimal(18, 8)");

                entity.Property(e => e.TripInvoiceid).HasColumnName("TRIP_INVOICEID");

                entity.Property(e => e.TripIsseparateinvoice).HasColumnName("TRIP_ISSEPARATEINVOICE");

                entity.Property(e => e.TripLastlocation)
                    .HasColumnName("TRIP_LASTLOCATION")
                    .HasMaxLength(2000);

                entity.Property(e => e.TripNotes)
                    .HasColumnName("TRIP_NOTES")
                    .HasMaxLength(1000);

                entity.Property(e => e.TripNotuselimitsincalculation).HasColumnName("TRIP_NOTUSELIMITSINCALCULATION");

                entity.Property(e => e.TripOfficialdocumentno)
                    .HasColumnName("TRIP_OFFICIALDOCUMENTNO")
                    .HasMaxLength(200);

                entity.Property(e => e.TripParenttripid).HasColumnName("TRIP_PARENTTRIPID");

                entity.Property(e => e.TripPlannedarrivaldatetime)
                    .HasColumnName("TRIP_PLANNEDARRIVALDATETIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.TripPlanneddepartdatetime)
                    .HasColumnName("TRIP_PLANNEDDEPARTDATETIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.TripPlanneddriverinfo)
                    .HasColumnName("TRIP_PLANNEDDRIVERINFO")
                    .HasMaxLength(250);

                entity.Property(e => e.TripPlannedvehicleplate)
                    .HasColumnName("TRIP_PLANNEDVEHICLEPLATE")
                    .HasMaxLength(100);

                entity.Property(e => e.TripProjectid).HasColumnName("TRIP_PROJECTID");

                entity.Property(e => e.TripPurchaseamountapproved).HasColumnName("TRIP_PURCHASEAMOUNTAPPROVED");

                entity.Property(e => e.TripPurchaseinvoiced).HasColumnName("TRIP_PURCHASEINVOICED");

                entity.Property(e => e.TripPurchbaseamount)
                    .HasColumnName("TRIP_PURCHBASEAMOUNT")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TripPurchcurrencyamount)
                    .HasColumnName("TRIP_PURCHCURRENCYAMOUNT")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TripPurchcurrencycodeid).HasColumnName("TRIP_PURCHCURRENCYCODEID");

                entity.Property(e => e.TripPurchcurrencyrate)
                    .HasColumnName("TRIP_PURCHCURRENCYRATE")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TripReceivedcomissionamount)
                    .HasColumnName("TRIP_RECEIVEDCOMISSIONAMOUNT")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TripReferanceid).HasColumnName("TRIP_REFERANCEID");

                entity.Property(e => e.TripRelatedtripid).HasColumnName("TRIP_RELATEDTRIPID");

                entity.Property(e => e.TripRequestdatetime)
                    .HasColumnName("TRIP_REQUESTDATETIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.TripRouteid).HasColumnName("TRIP_ROUTEID");

                entity.Property(e => e.TripSalesbaseamount)
                    .HasColumnName("TRIP_SALESBASEAMOUNT")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TripSalescurrencyamount)
                    .HasColumnName("TRIP_SALESCURRENCYAMOUNT")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TripSalescurrencycodeid).HasColumnName("TRIP_SALESCURRENCYCODEID");

                entity.Property(e => e.TripSalescurrencyrate)
                    .HasColumnName("TRIP_SALESCURRENCYRATE")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TripSecondtrailerid).HasColumnName("TRIP_SECONDTRAILERID");

                entity.Property(e => e.TripSecondvehicledriverid).HasColumnName("TRIP_SECONDVEHICLEDRIVERID");

                entity.Property(e => e.TripSendersdocumentno)
                    .HasColumnName("TRIP_SENDERSDOCUMENTNO")
                    .HasMaxLength(200);

                entity.Property(e => e.TripSubcontractorrefno)
                    .HasColumnName("TRIP_SUBCONTRACTORREFNO")
                    .HasMaxLength(80);

                entity.Property(e => e.TripSubcontrtripcode)
                    .HasColumnName("TRIP_SUBCONTRTRIPCODE")
                    .HasMaxLength(80);

                entity.Property(e => e.TripTircarnetno)
                    .HasColumnName("TRIP_TIRCARNETNO")
                    .HasMaxLength(200);

                entity.Property(e => e.TripToaddress)
                    .HasColumnName("TRIP_TOADDRESS")
                    .HasMaxLength(500);

                entity.Property(e => e.TripToaddressid).HasColumnName("TRIP_TOADDRESSID");

                entity.Property(e => e.TripTocityid).HasColumnName("TRIP_TOCITYID");

                entity.Property(e => e.TripTogeoareaid).HasColumnName("TRIP_TOGEOAREAID");

                entity.Property(e => e.TripTopostalcodeid).HasColumnName("TRIP_TOPOSTALCODEID");

                entity.Property(e => e.TripTranspcontrcomissionamount)
                    .HasColumnName("TRIP_TRANSPCONTRCOMISSIONAMOUNT")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TripTransportationcontractid).HasColumnName("TRIP_TRANSPORTATIONCONTRACTID");

                entity.Property(e => e.TripTransportationtypeid).HasColumnName("TRIP_TRANSPORTATIONTYPEID");

                entity.Property(e => e.TripTransportationwayid).HasColumnName("TRIP_TRANSPORTATIONWAYID");

                entity.Property(e => e.TripTransportscheduleid).HasColumnName("TRIP_TRANSPORTSCHEDULEID");

                entity.Property(e => e.TripTransportsubcontrorid).HasColumnName("TRIP_TRANSPORTSUBCONTRORID");

                entity.Property(e => e.TripTransportsupervisorid).HasColumnName("TRIP_TRANSPORTSUPERVISORID");

                entity.Property(e => e.TripTripdistancekm).HasColumnName("TRIP_TRIPDISTANCEKM");

                entity.Property(e => e.TripTripinvoicetypeid).HasColumnName("TRIP_TRIPINVOICETYPEID");

                entity.Property(e => e.TripTripreasonid).HasColumnName("TRIP_TRIPREASONID");

                entity.Property(e => e.TripTriprelationtypeid).HasColumnName("TRIP_TRIPRELATIONTYPEID");

                entity.Property(e => e.TripTripstatusid).HasColumnName("TRIP_TRIPSTATUSID");

                entity.Property(e => e.TripTriptypeid).HasColumnName("TRIP_TRIPTYPEID");

                entity.Property(e => e.TripUpdatedby)
                    .HasColumnName("TRIP_UPDATEDBY")
                    .HasMaxLength(200);

                entity.Property(e => e.TripUpdateddatetime)
                    .HasColumnName("TRIP_UPDATEDDATETIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.TripVatrate)
                    .HasColumnName("TRIP_VATRATE")
                    .HasColumnType("decimal(24, 8)");

                entity.Property(e => e.TripVehiclebeginkm).HasColumnName("TRIP_VEHICLEBEGINKM");

                entity.Property(e => e.TripVehicledriverid).HasColumnName("TRIP_VEHICLEDRIVERID");

                entity.Property(e => e.TripVehicleendkm).HasColumnName("TRIP_VEHICLEENDKM");

                entity.Property(e => e.TripVehicleid).HasColumnName("TRIP_VEHICLEID");

                entity.Property(e => e.TripVehiclesupplydate)
                    .HasColumnName("TRIP_VEHICLESUPPLYDATE")
                    .HasColumnType("datetime");

                entity.Property(e => e.TripWaybillprintdate)
                    .HasColumnName("TRIP_WAYBILLPRINTDATE")
                    .HasColumnType("datetime");

                entity.Property(e => e.UetdsNotificationRefNo).HasMaxLength(50);

                entity.Property(e => e.UetdsNotificationStatus).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<LtraTripdetail>(entity =>
            {
                entity.HasKey(e => e.TrdtId)
                    .HasName("LTRA_TRIPDETAIL_PK");

                entity.ToTable("LTRA_TRIPDETAIL");

                entity.HasIndex(e => e.TrdtTransportationorderid)
                    .HasName("_dta_index_LTRA_TRIPDETAIL_5_1587601540__K11");

                entity.HasIndex(e => new { e.TrdtTransportationorderid, e.TrdtTripid })
                    .HasName("_dta_index_LTRA_TRIPDETAIL_5_1587601540__K12_11");

                entity.HasIndex(e => new { e.TrdtId, e.TrdtTripid, e.TrdtTransportationorderid, e.TrdtTransporttaskid, e.TrdtRouteorder, e.TrdtTripdetailstatusid })
                    .HasName("_dta_index_LTRA_TRIPDETAIL_5_1587601540__K12_K11_K9_K34_K28_10");

                entity.Property(e => e.TrdtId)
                    .HasColumnName("TRDT_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastModifiedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Sddatetime)
                    .HasColumnName("SDDatetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.Sddelivery).HasColumnName("SDDelivery");

                entity.Property(e => e.Sdintegration).HasColumnName("SDIntegration");

                entity.Property(e => e.TrdtActualdeliverydate)
                    .HasColumnName("TRDT_ACTUALDELIVERYDATE")
                    .HasColumnType("datetime");

                entity.Property(e => e.TrdtActualloaddatetime)
                    .HasColumnName("TRDT_ACTUALLOADDATETIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.TrdtActualquantity)
                    .HasColumnName("TRDT_ACTUALQUANTITY")
                    .HasColumnType("decimal(24, 8)");

                entity.Property(e => e.TrdtActualvolume)
                    .HasColumnName("TRDT_ACTUALVOLUME")
                    .HasColumnType("decimal(24, 8)");

                entity.Property(e => e.TrdtActualweight)
                    .HasColumnName("TRDT_ACTUALWEIGHT")
                    .HasColumnType("decimal(24, 8)");

                entity.Property(e => e.TrdtCalculatedprice)
                    .HasColumnName("TRDT_CALCULATEDPRICE")
                    .HasColumnType("decimal(24, 8)");

                entity.Property(e => e.TrdtCustomerid).HasColumnName("TRDT_CUSTOMERID");

                entity.Property(e => e.TrdtDeclaredvalforcarriage)
                    .HasColumnName("TRDT_DECLAREDVALFORCARRIAGE")
                    .HasColumnType("decimal(24, 8)");

                entity.Property(e => e.TrdtDeclaredvalforcustoms)
                    .HasColumnName("TRDT_DECLAREDVALFORCUSTOMS")
                    .HasColumnType("decimal(24, 8)");

                entity.Property(e => e.TrdtDescription)
                    .HasColumnName("TRDT_DESCRIPTION")
                    .HasMaxLength(400);

                entity.Property(e => e.TrdtDrivergetspayment).HasColumnName("TRDT_DRIVERGETSPAYMENT");

                entity.Property(e => e.TrdtDriverpenalty)
                    .HasColumnName("TRDT_DRIVERPENALTY")
                    .HasColumnType("decimal(24, 8)");

                entity.Property(e => e.TrdtEnteredby)
                    .HasColumnName("TRDT_ENTEREDBY")
                    .HasMaxLength(100);

                entity.Property(e => e.TrdtEntrydatetime)
                    .HasColumnName("TRDT_ENTRYDATETIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.TrdtExpecteddeliverydate)
                    .HasColumnName("TRDT_EXPECTEDDELIVERYDATE")
                    .HasColumnType("datetime");

                entity.Property(e => e.TrdtFrompartyaddressid).HasColumnName("TRDT_FROMPARTYADDRESSID");

                entity.Property(e => e.TrdtGrossweight)
                    .HasColumnName("TRDT_GROSSWEIGHT")
                    .HasColumnType("decimal(24, 8)");

                entity.Property(e => e.TrdtHawbno)
                    .HasColumnName("TRDT_HAWBNO")
                    .HasMaxLength(160);

                entity.Property(e => e.TrdtIsweightvalprepaid).HasColumnName("TRDT_ISWEIGHTVALPREPAID");

                entity.Property(e => e.TrdtNotes)
                    .HasColumnName("TRDT_NOTES")
                    .HasMaxLength(1000);

                entity.Property(e => e.TrdtOtherprepaid).HasColumnName("TRDT_OTHERPREPAID");

                entity.Property(e => e.TrdtPalletqty)
                    .HasColumnName("TRDT_PALLETQTY")
                    .HasColumnType("decimal(24, 8)");

                entity.Property(e => e.TrdtPlannedcustomerarrival)
                    .HasColumnName("TRDT_PLANNEDCUSTOMERARRIVAL")
                    .HasColumnType("datetime");

                entity.Property(e => e.TrdtPlannedcustomerdepart)
                    .HasColumnName("TRDT_PLANNEDCUSTOMERDEPART")
                    .HasColumnType("datetime");

                entity.Property(e => e.TrdtPlannedcustomerload)
                    .HasColumnName("TRDT_PLANNEDCUSTOMERLOAD")
                    .HasColumnType("datetime");

                entity.Property(e => e.TrdtPlannedkm).HasColumnName("TRDT_PLANNEDKM");

                entity.Property(e => e.TrdtPlannedsenderarrival)
                    .HasColumnName("TRDT_PLANNEDSENDERARRIVAL")
                    .HasColumnType("datetime");

                entity.Property(e => e.TrdtPlannedsenderdepart)
                    .HasColumnName("TRDT_PLANNEDSENDERDEPART")
                    .HasColumnType("datetime");

                entity.Property(e => e.TrdtPlannedsenderload)
                    .HasColumnName("TRDT_PLANNEDSENDERLOAD")
                    .HasColumnType("datetime");

                entity.Property(e => e.TrdtPurchasebaseprice)
                    .HasColumnName("TRDT_PURCHASEBASEPRICE")
                    .HasColumnType("decimal(24, 8)");

                entity.Property(e => e.TrdtPurchasecurrencycodeid).HasColumnName("TRDT_PURCHASECURRENCYCODEID");

                entity.Property(e => e.TrdtQuantity)
                    .HasColumnName("TRDT_QUANTITY")
                    .HasColumnType("decimal(24, 8)");

                entity.Property(e => e.TrdtRouteorder).HasColumnName("TRDT_ROUTEORDER");

                entity.Property(e => e.TrdtTopartyaddressid).HasColumnName("TRDT_TOPARTYADDRESSID");

                entity.Property(e => e.TrdtTransportationorderid).HasColumnName("TRDT_TRANSPORTATIONORDERID");

                entity.Property(e => e.TrdtTransportationpricetypeid).HasColumnName("TRDT_TRANSPORTATIONPRICETYPEID");

                entity.Property(e => e.TrdtTransportorderdetid).HasColumnName("TRDT_TRANSPORTORDERDETID");

                entity.Property(e => e.TrdtTransportproductgroupid).HasColumnName("TRDT_TRANSPORTPRODUCTGROUPID");

                entity.Property(e => e.TrdtTransporttaskid).HasColumnName("TRDT_TRANSPORTTASKID");

                entity.Property(e => e.TrdtTripdetailstatusid).HasColumnName("TRDT_TRIPDETAILSTATUSID");

                entity.Property(e => e.TrdtTripid).HasColumnName("TRDT_TRIPID");

                entity.Property(e => e.TrdtTriptypeid).HasColumnName("TRDT_TRIPTYPEID");

                entity.Property(e => e.TrdtUpdatedby)
                    .HasColumnName("TRDT_UPDATEDBY")
                    .HasMaxLength(400);

                entity.Property(e => e.TrdtUpdateddatetime)
                    .HasColumnName("TRDT_UPDATEDDATETIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.TrdtVolume)
                    .HasColumnName("TRDT_VOLUME")
                    .HasColumnType("decimal(24, 8)");

                entity.Property(e => e.TrdtVolumeunitid).HasColumnName("TRDT_VOLUMEUNITID");

                entity.Property(e => e.TrdtWaybillprintdate)
                    .HasColumnName("TRDT_WAYBILLPRINTDATE")
                    .HasColumnType("datetime");

                entity.Property(e => e.TrdtWeight)
                    .HasColumnName("TRDT_WEIGHT")
                    .HasColumnType("decimal(24, 8)");

                entity.Property(e => e.TrdtWeightunitid).HasColumnName("TRDT_WEIGHTUNITID");

                entity.HasOne(d => d.TrdtTransportationorder)
                    .WithMany(p => p.LtraTripdetail)
                    .HasForeignKey(d => d.TrdtTransportationorderid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("TRIPDETAIL_FK01");

                entity.HasOne(d => d.TrdtTrip)
                    .WithMany(p => p.LtraTripdetail)
                    .HasForeignKey(d => d.TrdtTripid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("TRIPDETAIL_FK15");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}

﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.LaModels
{
    public partial class LtraTrip
    {
        public LtraTrip()
        {
            LtraTransportorddeliveryinf = new HashSet<LtraTransportorddeliveryinf>();
            LtraTripdetail = new HashSet<LtraTripdetail>();
        }

        public long TripId { get; set; }
        public string TripCode { get; set; }
        public string TripDescription { get; set; }
        public string TripNotes { get; set; }
        public long? TripVehicleid { get; set; }
        public long? TripVehicledriverid { get; set; }
        public DateTime? TripPlanneddepartdatetime { get; set; }
        public DateTime? TripPlannedarrivaldatetime { get; set; }
        public long TripTripstatusid { get; set; }
        public long? TripTripinvoicetypeid { get; set; }
        public bool? TripDocumentarrived { get; set; }
        public DateTime? TripDocumentarrivaldate { get; set; }
        public DateTime? TripActualdepartdatetime { get; set; }
        public string TripSendersdocumentno { get; set; }
        public string TripDepositorsdocumentno { get; set; }
        public long? TripExtravehicleid { get; set; }
        public DateTime? TripEntrydatetime { get; set; }
        public long? TripProjectid { get; set; }
        public long? TripCompanyid { get; set; }
        public string TripDriverinvoicenumber { get; set; }
        public DateTime? TripDriverinvoicedatetime { get; set; }
        public long? TripVehiclebeginkm { get; set; }
        public long? TripVehicleendkm { get; set; }
        public long? TripTripdistancekm { get; set; }
        public string TripContainerno { get; set; }
        public long? TripContainertypeid { get; set; }
        public string TripOfficialdocumentno { get; set; }
        public long? TripTransportationtypeid { get; set; }
        public long? TripParenttripid { get; set; }
        public string TripTircarnetno { get; set; }
        public string TripAubkno { get; set; }
        public bool? TripIncludeintransportreport { get; set; }
        public long? TripTriptypeid { get; set; }
        public long? TripFromcityid { get; set; }
        public long? TripFromgeoareaid { get; set; }
        public string TripFromaddress { get; set; }
        public long? TripTocityid { get; set; }
        public long? TripTogeoareaid { get; set; }
        public string TripToaddress { get; set; }
        public long? TripFrompostalcodeid { get; set; }
        public long? TripTopostalcodeid { get; set; }
        public long? TripRouteid { get; set; }
        public long? TripTransportationcontractid { get; set; }
        public long? TripReferanceid { get; set; }
        public long TripInventorysiteid { get; set; }
        public long? TripSecondvehicledriverid { get; set; }
        public bool? TripPurchaseinvoiced { get; set; }
        public bool? TripPurchaseamountapproved { get; set; }
        public string TripEnteredby { get; set; }
        public long? TripTransportsubcontrorid { get; set; }
        public bool? TripIsseparateinvoice { get; set; }
        public string TripUpdatedby { get; set; }
        public DateTime? TripUpdateddatetime { get; set; }
        public long? TripRelatedtripid { get; set; }
        public long? TripTriprelationtypeid { get; set; }
        public DateTime? TripWaybillprintdate { get; set; }
        public long? TripFromaddressid { get; set; }
        public long? TripToaddressid { get; set; }
        public long? TripSecondtrailerid { get; set; }
        public decimal? TripSalescurrencyamount { get; set; }
        public decimal? TripSalesbaseamount { get; set; }
        public long? TripPurchcurrencycodeid { get; set; }
        public decimal? TripPurchcurrencyamount { get; set; }
        public decimal? TripPurchbaseamount { get; set; }
        public decimal? TripPurchcurrencyrate { get; set; }
        public long? TripSalescurrencycodeid { get; set; }
        public decimal? TripSalescurrencyrate { get; set; }
        public decimal? TripEndinglitre { get; set; }
        public decimal? TripBeginninglitre { get; set; }
        public string TripBookingnumber { get; set; }
        public long? TripTransportationwayid { get; set; }
        public string TripSubcontractorrefno { get; set; }
        public string TripSubcontrtripcode { get; set; }
        public decimal? TripVatrate { get; set; }
        public long? TripTransportscheduleid { get; set; }
        public DateTime? TripRequestdatetime { get; set; }
        public decimal? TripInsuaranceamount { get; set; }
        public DateTime? TripVehiclesupplydate { get; set; }
        public long? TripTransportsupervisorid { get; set; }
        public long? TripAppointedbyid { get; set; }
        public DateTime? TripActualarrivaldatetime { get; set; }
        public DateTime? TripEmptycontreturndate { get; set; }
        public decimal? TripReceivedcomissionamount { get; set; }
        public decimal? TripTranspcontrcomissionamount { get; set; }
        public string TripPlannedvehicleplate { get; set; }
        public string TripPlanneddriverinfo { get; set; }
        public decimal? TripInvoiceablevolume { get; set; }
        public decimal? TripInvoiceabletripquantity { get; set; }
        public decimal? TripInvoiceabledistancekm { get; set; }
        public decimal? TripInvoiceableweight { get; set; }
        public decimal? TripInvoiceabletripdistancekm { get; set; }
        public bool? TripNotuselimitsincalculation { get; set; }
        public long? TripTripreasonid { get; set; }
        public decimal? TripInvoiceablevalue { get; set; }
        public string TripArrivalcustoms { get; set; }
        public string TripDeparturecustoms { get; set; }
        public string TripLastlocation { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public long? TripInvoiceid { get; set; }
        public bool? UetdsNotificationStatus { get; set; }
        public bool? IsUpdate { get; set; }
        public string UetdsNotificationRefNo { get; set; }

        public virtual ICollection<LtraTransportorddeliveryinf> LtraTransportorddeliveryinf { get; set; }
        public virtual ICollection<LtraTripdetail> LtraTripdetail { get; set; }
    }
}

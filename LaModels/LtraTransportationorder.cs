﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.LaModels
{
    public partial class LtraTransportationorder
    {
        public LtraTransportationorder()
        {
            LtraTransportorddeliveryinf = new HashSet<LtraTransportorddeliveryinf>();
            LtraTripdetail = new HashSet<LtraTripdetail>();
        }

        public long? TrorPurchcurrencycodeid { get; set; }
        public decimal? TrorPurchcurrencyamount { get; set; }
        public decimal? TrorPurchbaseamount { get; set; }
        public decimal? TrorPurchcurrencyrate { get; set; }
        public decimal? TrorWeight { get; set; }
        public decimal? TrorVolume { get; set; }
        public long? TrorQuantity { get; set; }
        public long? TrorReturnoforderid { get; set; }
        public long? TrorTranspprojectscheduleid { get; set; }
        public long? TrorTodistictid { get; set; }
        public long? TrorFromdistictid { get; set; }
        public long? TrorPaymenttermid { get; set; }
        public decimal? TrorDensity { get; set; }
        public bool? TrorIsweightvalprepaid { get; set; }
        public bool? TrorOtherprepaid { get; set; }
        public decimal? TrorDeclaredvalforcarriage { get; set; }
        public decimal? TrorDeclaredvalforcustoms { get; set; }
        public long? TrorTripinvoicetypeid { get; set; }
        public long? TrorTransitwarehousesid { get; set; }
        public long? TrorInvoicetoaddressid { get; set; }
        public long? TrorPaymentwayid { get; set; }
        public decimal? TrorVatrate { get; set; }
        public long? TrorFromcountryid { get; set; }
        public long? TrorMilestonetemplateid { get; set; }
        public long? TrorTransportscheduleid { get; set; }
        public bool? TrorIselevator { get; set; }
        public decimal? TrorNumberoffloor { get; set; }
        public decimal? TrorInsuaranceamount { get; set; }
        public decimal? TrorOrdervalue { get; set; }
        public long? TrorMilestoneid { get; set; }
        public long? TrorWarehouseorderid { get; set; }
        public long? TrorWarehousereceiptid { get; set; }
        public DateTime? TrorReportdate1 { get; set; }
        public DateTime? TrorReportdate2 { get; set; }
        public DateTime? TrorReportdate3 { get; set; }
        public long TrorId { get; set; }
        public string TrorEnteredby { get; set; }
        public DateTime? TrorEntrydatetime { get; set; }
        public string TrorNotes { get; set; }
        public string TrorCode { get; set; }
        public DateTime? TrorPlannedshipmentdatetime { get; set; }
        public DateTime? TrorPlanneddeliverydatetime { get; set; }
        public long TrorDepositorid { get; set; }
        public long? TrorCustomerid { get; set; }
        public long? TrorFromcityid { get; set; }
        public long? TrorFrompostalcodeid { get; set; }
        public long? TrorFromgeoareaid { get; set; }
        public long? TrorTocityid { get; set; }
        public long? TrorTopostalcodeid { get; set; }
        public long? TrorTogeoareaid { get; set; }
        public long? TrorTransportsenderid { get; set; }
        public long? TrorTocountryid { get; set; }
        public string TrorFromadress { get; set; }
        public string TrorToadress { get; set; }
        public long? TrorTransportationtypeid { get; set; }
        public string TrorDeliverynote { get; set; }
        public string TrorDepositorsdocument { get; set; }
        public long? TrorKm { get; set; }
        public long TrorTransportorderstatusid { get; set; }
        public string TrorOrderedby { get; set; }
        public DateTime TrorOrderdate { get; set; }
        public long? TrorDeliveryleadtime { get; set; }
        public long? TrorProjectid { get; set; }
        public long? TrorCustomerpartyaddressid { get; set; }
        public long? TrorSenderpartyaddressid { get; set; }
        public string TrorOfficialdocumentno { get; set; }
        public long? TrorCompanyid { get; set; }
        public long? TrorPaymentpartyid { get; set; }
        public long? TrorRouteid { get; set; }
        public long? TrorTransportationwayid { get; set; }
        public long? TrorTransportationpriorityid { get; set; }
        public long? TrorSalescurrencycodeid { get; set; }
        public long? TrorTransportationcontractid { get; set; }
        public long? TrorTransportationordertypeid { get; set; }
        public long? TrorTransportationpricetypeid { get; set; }
        public long? TrorTransportationpayeeid { get; set; }
        public bool? TrorIsinternational { get; set; }
        public long? TrorManufacturerid { get; set; }
        public long? TrorAgencyid { get; set; }
        public long? TrorCustomsid { get; set; }
        public string TrorDeparturecustoms { get; set; }
        public string TrorArrivalcustoms { get; set; }
        public string TrorCmr { get; set; }
        public string TrorFcr { get; set; }
        public long? TrorDeliverytermsid { get; set; }
        public DateTime? TrorExpectedreadydatetime { get; set; }
        public string TrorCustomsdeclarationnumber { get; set; }
        public string TrorCustomsdeclarationdate { get; set; }
        public string TrorLetterofcreditnumber { get; set; }
        public string TrorCancelreason { get; set; }
        public long? TrorReferanceid { get; set; }
        public long? TrorTransportservicelevelid { get; set; }
        public long? TrorActualkm { get; set; }
        public long? TrorInventorysiteid { get; set; }
        public long? TrorSalesinvoicablekm { get; set; }
        public long? TrorPurchaseinvoicablekm { get; set; }
        public decimal? TrorSalescurrencyrate { get; set; }
        public DateTime? TrorActualdeliverydatetime { get; set; }
        public DateTime? TrorActualloaddatetime { get; set; }
        public long? TrorShipmentpacknumber { get; set; }
        public long? TrorTozoneid { get; set; }
        public bool? TrorPayincash { get; set; }
        public string TrorCustomerordernumber { get; set; }
        public DateTime? TrorApprovaldate { get; set; }
        public long? TrorTransportorderentrytypeid { get; set; }
        public long? TrorFromzoneid { get; set; }
        public long? TrorTotownid { get; set; }
        public long? TrorFromtownid { get; set; }
        public bool? TrorSalesinvoiced { get; set; }
        public bool? TrorSalesamountapproved { get; set; }
        public bool? TrorIsseparateinvoice { get; set; }
        public long? TrorOrdervatpercent { get; set; }
        public string TrorUpdatedby { get; set; }
        public DateTime? TrorUpdateddatetime { get; set; }
        public string TrorAddressdirections { get; set; }
        public long? TrorTransordcancelreasonid { get; set; }
        public string TrorReferancekey { get; set; }
        public string TrorReferancecode { get; set; }
        public decimal? TrorSalescurrencyamount { get; set; }
        public decimal? TrorSalesbaseamount { get; set; }
        public long? TrorVehicleid { get; set; }
        public DateTime? TrorRequesteddeliverydate { get; set; }
        public DateTime? TrorRequestedassemblydate { get; set; }
        public long? TrorTransportpurchcontractid { get; set; }
        public long? TrorReplicationid { get; set; }
        public long? TrorInvoiceid { get; set; }
        public int? SpintegrationStatus { get; set; }
        public DateTime? SpintegrationDate { get; set; }
        public string RefCode { get; set; }
        public string TypeCode { get; set; }
        public string PricingTypeCode { get; set; }
        public decimal? Price { get; set; }
        public string Currency { get; set; }
        public long RefSeq { get; set; }
        public bool? IsLoadInsurance { get; set; }
        public long? TrorRegionid { get; set; }

        public virtual ICollection<LtraTransportorddeliveryinf> LtraTransportorddeliveryinf { get; set; }
        public virtual ICollection<LtraTripdetail> LtraTripdetail { get; set; }
    }
}

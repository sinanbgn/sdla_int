﻿using System;
using System.Collections.Generic;

namespace SdLa_Int.LaModels
{
    public partial class LtraTransportorddeliveryinf
    {
        public long TodiId { get; set; }
        public DateTime? TodiArrivaldatetime { get; set; }
        public DateTime TodiDeliverydatetime { get; set; }
        public string TodiRecievedby { get; set; }
        public long? TodiIdentitytypeid { get; set; }
        public string TodiIdentityno { get; set; }
        public string TodiDeliveryplace { get; set; }
        public DateTime? TodiDeparturedatetime { get; set; }
        public long? TodiTransportreturnreasonid { get; set; }
        public string TodiNotes { get; set; }
        public long? TodiTransportdelayreasonid { get; set; }
        public long? TodiTripid { get; set; }
        public long TodiTransportationorderid { get; set; }
        public string TodiEnteredby { get; set; }
        public DateTime? TodiEntrydatetime { get; set; }
        public string TodiUpdatedby { get; set; }
        public DateTime? TodiUpdateddatetime { get; set; }
        public long? TodiTransordadrchangreasonid { get; set; }
        public long? TodiChangedpartyaddressid { get; set; }
        public string TodiReceiverphoto { get; set; }
        public long? TodiTransportorderdetid { get; set; }
        public string TodiPoddocumentno { get; set; }
        public decimal? TodiKm { get; set; }
        public long? TodiAsnquantity { get; set; }
        public string TodiProductdescription { get; set; }
        public long? TodiActionstatusid { get; set; }
        public string TodiShipmentbarcode { get; set; }
        public string TodiCode { get; set; }
        public string TodiDescription { get; set; }
        public bool? TodiIsapproved { get; set; }
        public byte[] TodiImage { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }

        public virtual LtraTransportationorder TodiTransportationorder { get; set; }
        public virtual LtraTrip TodiTrip { get; set; }
    }
}
